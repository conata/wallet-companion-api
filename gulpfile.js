/**
 * @fileOverview ./gulpfile.js
 * @description
 * Gulpfile for Telegraf Money Server Suite, Digital Ecologies, whatever, etc.:
 *
 * 1. Microservices
 * 2. RPCs, Message Queues, Sockets, etc.
 * 3. Database Managers (DO YOU REALLY NEED A BLOCKCHAIN DATABASE FOR EVERY
 *    PORT OR ENDPOINT SERVICE WORD? Do all your transactional schemas need
 *    probabilistic validation or con-fi-den-tiality conditions basically hard-
 *    coded to the ledger? -- probably you do not.)
 * 4. Confidentendential Environment Configurations (yes, I did just
 *    pormanteauize "Confidence" and "Tendential"); such configurations
 *    (around nomological constands like the names or patterns of
 *    "authentication" like non-identity based forms of authentication such as
 *    Ninja, Hypermedial Adaptive TFA (HA2FA), social authentications, etc.)
 *    whereof the "rate of digitality tendentially fals" is not necessarily true,
 *    and must be accounted for (laruelle). "Confidence" is a library for facilitating
 *    such buzzquackery talk that is being discussed here, and such talk is
 *    not "metagaming," but actual specification of implementation details for
 *    how this very codebase is going to implement authentcation patterns
 *    beyond Passport or mere Biometric "Identity".
 * 5. Restful Endpoints
 * 6. Distributed Commands (CQRS) for Scheduling Crons and programmatic
 *    temporal conditions
 * 7. Software-defined Networks, Meshnets, Geometric Routers, etc.
 */

var gulp = require('gulp')
var server = require('gulp-server-livereload')
var prettify = require('gulp-jsbeautifier')
var jshint = require('gulp-jshint')
var nodemon = require('gulp-nodemon')
var apidoc = require('gulp-apidoc')
var stylish = require('jshint-stylish')

/**
 * TASKS
 */

gulp.task('server', server)
gulp.task('doc', doc)
gulp.task('lint', lint)
gulp.task('prettify', prettify)
gulp.task('default', ['server'])

/////////////

/**
 * @name server
 */
function server() {
  nodemon({
    script: 'app/server.js',
    ext: 'js'
  })
}


/**
 * @name prettify
 */
function prettify() {
  gulp.src('./app/**/*.js')
      .pipe(prettify({
      config: './.jsbeautify.json'
    }))
    .pipe(gulp.dest('./app/'))
}


/**
 * @name lint
 */
function lint() {
  return gulp.src('./app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
}


/**
 * @name doc
 */
function doc(done){
  apidoc({
    src: 'app/',
    dest: 'app/public/docs/'
  },done)
}

