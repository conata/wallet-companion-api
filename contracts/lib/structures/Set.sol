pragma solidity ^0.4.11;

contract Set {

  bytes32 public members;

  struct Member {
    bytes32 next;
    uint number;
    string name;
  }

  mapping(bytes32 => Member) public members;

  /**
   * @name Set
   */
  function Set() public {
  }

  function getMembers() view public returns (bytes32) {
    return members;
  }

  /**
   * @function has
   */
  function has() public {
    // stub
  }

  /**
   * @function values
   */
  function values() public {
    // stub
  }

}
