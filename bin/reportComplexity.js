/**
 * @fileOverview ./bin/reportComplexity.js
 * @description
 * A basic implementation of Plato (https://github.com/es-analysis/plato) to
 * facilitate generation of a complexity analysis report on the Messenger
 * Wallet project front end codebase.
 */

var plato = require('plato');
var log = console.log;

var codebaseSrc = [
  './app/actions/**',
  './app/artifacts/**',
  './app/authorities/**',
  './app/classes/**',
  './app/composers/**',
  './app/config/**',
  './app/contracts/**',
  './app/events/**',
  './app/interfaces/**',
  './app/lib/**',
  './app/models/**',
  './app/modules/**',
  './app/objects/**',
  './app/operations/**',
  './app/views/**',
  './app/actions.js',
  './app/constants.js',
  './app/errors.js',
  './app/server.js',
  './app/utils.js'
];

var outputDir = './app/public/reports/complexity';
var config = {
  title: 'Wallet Companion API'
};

var postGenerate = postGenerate;

function postGenerate(report) {
  log('Generated complexity report!');
}

plato.inspect(codebaseSrc, outputDir, config, postGenerate);
