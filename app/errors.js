/**
 * @name tgm.error:ERRORS
 * @rfc https://tools.ietf.org/html/rfc7231
 * @description
 * Implements semantics for HTTP's Response Status Codes.
 */
const _ = require('lodash');

const ERRORS = {};

/**
 * General Errors
 */

ERRORS.MAINTENANCE_MODE = {
  status: 404,
  type: 'MaintenanceModeError',
  message: 'Application is currently undergoing maintenance. Please try again later.'
};

ERRORS.INVALID_PARAMETERS = {
  status: 400,
  type: 'InvalidParametersError',
  message: 'The request parameters are not valid.'
};

ERRORS.REQUEST_FAILED = {
  status: 400,
  type: 'RequestFailedError',
  message: 'The request failed.'
};

ERRORS.RESOURCE_NOT_FOUND = {
  status: 404,
  type: 'ResourceNotFoundError',
  message: 'The requested resource does not exist.'
};

ERRORS.RESOURCE_NOT_IMPLEMENTED = {
  status: 404,
  type: 'ResourceNotImplementedError',
  message: 'The requested resource has not been implemented.'
};

ERRORS.CONFLICT = {
  status: 409,
  type: 'InconsistentParametersConditionError',
  message: 'The request parameters are in conflict.'
};

ERRORS.IM_A_TEAPOT = {
  status: 418,
  type: 'InvalidParametersConditionError',
  message: 'The request parameters do not satisfy strict conditions.'
};

ERRORS.UNPROCESSABLE_ENTITY = {
  status: 422,
  type: 'IncoherentParametersError',
  message: 'The request parameters are incoherent.'
};

ERRORS.SERVER_FAULT = {
  status: 500,
  type: 'ServerFaultError',
  message: 'An internal error has prevented this request from being processed.'
};

/**
 * Auth Errors
 */
ERRORS.INVALD_LOGIN = {
  status: 401,
  type: 'InvalidLoginError',
  message: 'Login credentials are not valid.'
};

ERRORS.AUTH_REQUIRED = {
  status: 401,
  type: 'AuthRequiredError',
  message: 'You must be logged in to access this resource.'
};

ERRORS.ROLE_PERMISSION = {
  status: 401,
  type: 'RolePermissionError',
  message: 'You do not have permission to access this resource.'
};

/**
 * User Errors
 */
ERRORS.USER_EXISTS = {
  status: 400,
  type: 'UserExistsError',
  message: 'A user with this email address already exists.'
};

ERRORS.USER_NOT_FOUND = {
  status: 400,
  type: 'UserNotFoundError',
  message: 'A user with this email address does not exist.'
};

ERRORS.INVALID_PASSWORD_RESET_TOKEN = {
  status: 400,
  type: 'InvalidPasswordResetTokenError',
  message: 'The password reset token is either invalid or has expired.'
};

ERRORS.INVALID_VERIFICATION_TOKEN = {
  status: 400,
  type: 'InvalidAccountVerificationTokenError',
  messsage: 'The account verification token is either invalid or has expired.'
};

ERRORS.USER_VERIFICATION_PENDING = {
  status: 400,
  type: 'AccountPendingUserVerificationError',
  message: 'Your account is still pending email verification.'
};

ERRORS.ADMIN_APPROVAL_PENDING = {
  status: 400,
  type: 'AccountPendingAdminApprovalError',
  message: 'Access to this account is pending administrator approval.'
};

/**
 * Asset Errors
 */


ERRORS.IMAGE_DIMENSIONS_OUT_OF_RANGE = {
  status: 400,
  type: 'ImageDimensionsOutOfRangeError',
  message: 'Image width must be greater than 260px and less than 1024px'
};

ERRORS.INVALD_IMAGE_FILE_DATA_ERROR = {
  status: 400,
  type: 'InvalidImageFileDataError',
  message: 'The requested file cannot be processed for upload.'
};

/**
 * Offer Errors
 */
ERRORS.OFFER_BUYER_PERMISSION = {
  status: 401,
  type: 'OfferBuyerPermissionError',
  message: 'Only the buyer of this offer can perform this action.'
};

/**
 * Contract Errors
 */
ERRORS.CONTRACT_BUYER_PERMISSION = {
  status: 401,
  type: 'ContractBuyerPermissionError',
  message: 'Only the buyer for this contract can perform this action.'
};

ERRORS.CONTRACT_DATE_NOT_AVAILABLE = {
  status: 400,
  type: 'ContractDateNotAvailableError',
  message: 'Coin is not available for the requested dates'
};

ERRORS.CONTRACT_INVALID_DATES = {
  status: 400,
  type: 'ContractInvalidDatesError',
  message: 'The requested dates for this contract are not valid.'
};

ERRORS.CONTRACT_INVALID_QUANTITY = {
  status: 400,
  type: 'ContractQuantityExceedsAvailabilityError',
  message: 'Number of coins requested exceeds quantity available.'
};

module.exports = {
  ERRORS: ERRORS
};
