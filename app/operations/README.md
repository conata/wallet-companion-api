# Distributed Operations

## Infrastructural States (CQRS)

### Available 

Available Operations are: Disabled - Loaded - Active

### Installed 

Installed Operations are: Available + Loaded + Disabled

### Loaded 

Installed Operations are: Installed + Available + Disabled

### Disabled

Disabled Operations are: Available + Loaded - Active

### Active 

Disabled Operations are: Installed + Loaded - Disabled

## Application Resource States (ROCA-REST; HATEOAS)

### Original Operations

### TimeGate Operations

### TimeMap Operations

### Memento Operations

