const Seneca = require('seneca');

/**
 * __acceptor__
 *
 * @returns {undefined}
 */
function __acceptor__(req, res, next) {

  Seneca()
    .client({
      port: 8260,
      pin: 'cmd:run'
    })
    .use(local)
    .act('cmd:run', handler);
}

/**
 * acceptor
 *
 * @param req
 * @param res
 * @param next
 * @returns {undefined}
 */
function acceptor() {
  this.add('cmd:run', (msg, done) => {
    return done(null, {
      tag: 'acceptor'
    });
  });
}

/**
 * __approver__
 *
 * @returns {undefined}
 */
function __approver__(req, res, next) {

  Seneca()
    .client({
      port: 8261,
      pin: 'cmd:run'
    })
    .use(local)
    .act('cmd:run', handler(res));
}

/**
 * approver
 *
 * @returns {undefined}
 */
function approver() {

  this.add('cmd:run', (msg, done) => {
    return done(null, {
      tag: 'approver'
    });
  });
}

/**
 * __rejector__
 *
 * @returns {undefined}
 */
function __rejector__(req, res, next) {

  Seneca()
    .client({
      port: 8262,
      pin: 'cmd:run'
    })
    .use(local)
    .act('cmd:run', handler(res));
}

/**
 * rejector
 *
 * @returns {undefined}
 */
function rejector() {

  this.add('cmd:run', (msg, done) => {
    return done(null, {
      tag: 'rejector'
    });
  });
}

/**
 * __leader__
 *
 * @returns {undefined}
 */
function __leader__(req, res, next) {


  Seneca()
    .client({
      port: 8263,
      pin: 'cmd:run'
    })
    .use(local)
    .act('cmd:run', handler(res));

}

/**
 * leader
 *
 * @returns {undefined}
 */
function leader() {
  this.add('cmd:run', (msg, done) => {
    return done(null, {
      tag: 'leader'
    });
  });
}

/**
 * __replica__
 *
 * @returns {undefined}
 */
function __replica__(req, res, next) {
  Seneca()
    .client({
      port: 8264,
      pin: 'cmd:run'
    })
    .use(local)
    .act('cmd:run', handler(res));
}

/**
 * replica
 *
 * @returns {undefined}
 */
function replica() {
  this.leaders = [];
  this.initial_state = null;
  this.add('cmd:run', (msg, done) => {
    return done(null, {
      tag: 'replica'
    });
  });
}

/**
 * handler
 *
 * @returns {undefined}
 */
function handler(res) {
  return function(err, reply) {
    console.log(err, reply);
    res
      .json(reply)
      .done();
  };
}

/**
 * local
 *
 * @returns {undefined}
 */
function local() {
  this.add('cmd:run', function (msg, done) {
    this.prior(msg, (err, reply) => {
      return done(null, {
        tag: reply ? reply.tag : 'local'
      });
    });
  });
}

module.exports  = {
  __approver__ : __approver__,
  __acceptor__ : __acceptor__,
  __rejector__ : __rejector__,
  __leader__   : __leader__,
  __replica__  : __replica__,
  approver     : approver,
  acceptor     : acceptor,
  rejector     : rejector,
  leader       : leader,
  replica      : replica
};

