/* jshint asi:true */

// const BedquiltClient = require('bedquilt').BedquiltClient;
const massive = require('massive');
const { Pool, Client } = require('pg')
const QUILT = process.env['WALLETCOMPANION_QUILT'];
const throwError = require('./handlers').throwError;
const sendResponse = require('./handlers').sendResponse;

var client;
var cursor;

/**
 * @name call
 * @returns {object:Promise} A Promised PostgreSQL client adapter.
 */
function call() {
  var loaded = false;
  return new Promise(function(resolve, reject) {
    client = new Client({
      connectionString: QUILT,
      ssl: true
    });
    client.connect((err) => {
      let createTableQuery = `CREATE TABLE IF NOT EXISTS tickets(cipher varchar(255) NOT NULL)`;
      client.query(createTableQuery, err => {
        resolve({ connected: true });
      });
    });
  });
}

// ping
// call()
//   .then(function(client) {
//     console.log('Loadeded ', { $client: client })
//   }, function(err) {
//     console.log({ error: err });
//   })
//   .catch(function(err) {
//     console.log({ error: err });
//   });

exports.call = call;

/**
 * getTickets
 *
 * @param request
 * @param response
 * @returns {undefined}
 */
exports.listTickets = function(request, response) {
  call().then(function() {
    var q = 'SELECT * FROM tickets';
    client.query(q, (err, res) => {
      if (err) {
        return throwError('RequestFailedError')(response);
      }

      return sendResponse({ collection: res })(response);
    });
  });
};

/**
 * createTicket
 *
 * @param request
 * @param response
 * @returns {undefined}
 */
exports.createTicket = function(request, response) {
  call().then(function() {
    var q = 'INSERT INTO tickets(cipher) VALUES($1) RETURNING *';
    var p = [request._cipher];
    client.query(q, p, (err, res) => {
      if (err) {
        console.log({ error: err });
        return throwError('RequestFailedError')(response);
      }

      return sendResponse({
        contract : request._contract,
        cipher   : request._cipher,
        ticket   : res
      })(response);
    });
  });
};

/**
 * findTicket
 *
 * @param request
 * @param response
 * @returns {undefined}
 */
exports.findTicket = function(request, response) {
  call().then(function() {
    var q = 'SELECT * FROM tickets WHERE id = $1';
    var p = [request.params.id];
    client.query(q, p, (err, res) => {
      if (err) {
        return throwError('RequestFailedError')(response);
      }

      return sendResponse({ collection: res })(response);
    });
  });
};


/**
 * findStreamTicket
 *
 * @param request
 * @param response
 * @returns {undefined}
 */
function findStreamTicket(request, response) {
  call().then(function() {
    var q = 'SELECT * FROM tickets WHERE id = $1';
    var p = [request.params.id];
    client.query(q, p, (err, res) => {
      if (err) {
        return throwError('RequestFailedError')(response);
      }

      return sendResponse({ collection: res })(response);
    });
  });
}

exports.findStreamTicket = findStreamTicket;
exports.cursor = cursor;

