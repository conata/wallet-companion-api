const _ = require('lodash');
const s3 = require('s3');
const im = require('imagemagick');
const aws = require('aws-sdk');
const uuidv4 = require('uuid/v4');
const readChunk = require('read-chunk');
const fileType = require('file-type');

const bucket = process.env['WALLETCOMPANION_STORAGE_S3_BUCKET_PREFIX'];
const sender = process.env['WALLETCOMPANION_DEFAULT_EMAIL_SENDER'];

/**
 * Storage
 */

const s3client = s3.createClient({
  s3Client: new aws.S3({
    apiVersion: '2006-03-01',
    region: 'us-east-1'
  }),
  s3RetryCount: 4,
  s3RetryDelay: 2000,
});

/**
 * uploadFileToBucket
 *
 * @param remotePath
 * @param localPath
 * @returns {undefined}
 */
function uploadFileToBucket(remotePath, localPath) {
  return new Promise(function(resolve, reject) {

    console.log('uploadFileToBucket()');

    var params = {
      localFile: localPath,
      s3Params: {
        Bucket: bucket,
        Key: remotePath
      }
    };

    var objectURL = 'https://' + _.join( [ 's3', 'amazonaws', 'com'], '.') + '/' + _.join([bucket,remotePath], '/');

    var uploader = s3client.uploadFile(params);

    uploader.on('error', function(err) {
      console.log(err);
      console.log('Failed to upload: ', err.stack);
      reject(err);
    });

    /*
     * uploader.on('progress', function() {
     * console.log('Upload progress:',
     * uploader.progressMd5Amount, uploader.progressAmount, uploader.progressTotal);
    });
    */

    uploader.on('end', function() {
      console.log('Upload finished: ' + objectURL);
      resolve(objectURL);
    });
  });
}

/**
 * extractRemoteStorageURL
 *
 * @param url
 * @returns {undefined}
 */
function extractRemoteStorageURL(url) {
  var components = _.split( _.split(url, '://')[1] , '/' );
  var objectParams = {
    bucket: components[1],
    key: _.join( [ components[2], components[3] ], '/' )
  };
  return objectParams;
}

/**
 * removeFileFromBucket
 *
 * @param url
 * @returns {undefined}
 */
function removeFileFromBucket(url) {
  return new Promise(function(resolve, reject) {
    console.log('removeFileFromBucket()');

    var s3object = extractRemoteStorageURL(url);

    if ( _.isNil(s3object.bucket) || _.isNil(s3object.key) ) {
      reject(new Error('Object does not have a valid asset URL'));
    }

    var s3Params = {
      Bucket: s3object.bucket,
      Delete: {
        Objects: [{Key: s3object.key}]
      }
    };

    var deleteEmitter = s3client.deleteObjects(s3Params);

    deleteEmitter.on('error', function(err) {
      console.log(err);
      console.log('Failed to delete object: ', err.stack);
      reject(err);
    });

    deleteEmitter.on('end', function() {
      resolve(url);
    });

  });
}

/**
 * saveLocalFile
 *
 * @param asset
 * @param args
 * @returns {undefined}
 */
function saveLocalFile(asset, args) {
  return new Promise(function(resolve, reject) {
    console.log('saveLocalFile()');

    var file;

    if ( _.has(asset, 'file') ){
      file = asset.file;
    } else {
      file = asset;
    }

    if (args.noop === true) {
      resolve({ path: file, uuid: args.uuid, extension: args.extension });
    }

    if ( ! file ) {
      reject('no file to save');
    }

    var fileData = fileType(file.data);
    var fileExtension = '.notype.txt';

    if ( _.has(fileData, 'ext' ) && ! _.isNil(fileData.ext) ) {
      fileExtension = fileData.ext;
    } else {
      console.log(fileData);
      reject(new Error('InvalidImageFileDataError'));
    }

    var fileID = uuidv4();
    var localPath  = '/tmp/' + fileID + '.' + fileExtension;

    file.mv(localPath, function(err) {
      if ( err ) {
        console.log(err);
        reject(err);
      }
    });

    var result = { path: localPath, uuid: fileID, extension: fileExtension };

    resolve(result);
  });
}

/**
 * extractImageData
 *
 * @param file
 * @returns {undefined}
 */
function extractImageData(file) {
  return new Promise(function(resolve, reject) {
    console.log('extractImageData()');

    if ( file.match('^https://') ) {
      file = file.replace('https://','http://');
    }

    console.log(file);
    im.identify(file, function(err, features) {
      if (err) {
        console.log(err);
        reject(new Error('InvalidImageFileDataError'));
      }
      resolve(features);
    });
  });
}

/**
 * resizeImage
 *
 * @param asset
 * @param params
 * @returns {undefined}
 */
function resizeImage(asset, params) {
  return new Promise(function(resolve, reject) {
    console.log('resizeImage()');

    const offsetX = params.width || 0;
    const offsetY = params.height || 0;

    var srcFile = asset.url;

    if ( srcFile.match('^https://') ) {
      srcFile = srcFile.replace('https://','http://');
    }

    return extractImageData(srcFile)
      .then(function(imageData) {
        const fileID = uuidv4();
        const fileExtension = _.lowerCase(imageData.format);
        const dstFile = '/tmp/' + fileID + '.' + fileExtension;
        const resizeWidth = imageData.width + offsetX;
        const resizeHeight = imageData.height + offsetY;
        var params = {
          srcPath: srcFile,
          dstPath: dstFile,
          width: resizeWidth,
          height: resizeHeight
        };
        im.resize(params, function(err, stdout, stderr) {
          if (err) {
            console.log(err);
            reject(new Error('InvalidImageFileDataError'));
          }
          resolve({path: dstFile, uuid: fileID, extension: fileExtension});
        });
      })
      .catch(function(err) {
        console.log(err);
        reject(new Error('InvalidImageFileDataError'));
      });
  });
}

/**
 * cropImage
 *
 * @param asset
 * @param params
 * @returns {undefined}
 */
function cropImage(asset, params) {
  return new Promise(function(resolve, reject) {
    console.log('cropImage()');

    const offsetX = params.width || 0;
    const offsetY = params.height || 0;
    const gravity = params.gravity || 'Center';

    var srcFile = asset.url;

    if ( srcFile.match('^https://') ) {
      srcFile = srcFile.replace('https://','http://');
    }

    return extractImageData(srcFile)
      .then(function(imageData) {
        const fileID = uuidv4();
        const fileExtension = _.lowerCase(imageData.format);
        const dstFile = '/tmp/' + fileID + '.' + fileExtension;
        const resizeWidth = imageData.width + offsetX;
        const resizeHeight = imageData.height + offsetY;
        var params = {
          srcPath: srcFile,
          dstPath: dstFile,
          width: resizeWidth,
          height: resizeHeight,
          gravity: gravity
        };
        im.crop(params, function(err, stdout, stderr) {
          if (err) {
            console.log(err);
            reject(new Error('InvalidImageFileDataError'));
          }
          resolve({path: dstFile, uuid: fileID, extension: fileExtension});
        });
      })
      .catch(function(err) {
        console.log(err);
        reject(new Error('InvalidImageFileDataError'));
      });
  });
}

/**
 * checkImageSize
 *
 * @param file
 * @returns {undefined}
 */
function checkImageSize(file) {
  console.log('checkImageSize()');
  return new Promise(function(resolve, reject) {
    extractImageData(file)
      .then(function(features) {
        var width = features.width;

        if ( width < 320 || width > 3000 ) {
          console.log('image height:' + features.height);
          console.log('image width:' + width);
          //          reject(new Error('ImageDimensionsOutOfRangeError'));
          resolve(features);
        } else {
          resolve(features);
        }
      })
      .catch(function(err) {
        console.log(err);
        reject(new Error('InvalidImageFileDataError'));
      });
  });
}

/**
 * uploadImage
 *
 * @param asset
 * @param remoteDirectory
 * @param isUpdate
 * @returns {undefined}
 */
function uploadImage(asset, remoteDirectory, isUpdate) {
  return new Promise(function(resolve, reject) {
    console.log('uploadImage()');

    var file = asset.file;
    var saveLocalFileArgs = {};
    var oldURL = null;

    if ( isUpdate ) {
      oldURL = asset.url;
      saveLocalFileArgs.noop = true;
      saveLocalFileArgs.uuid = asset.uuid;
      saveLocalFileArgs.extension = asset.extension;
    } else {
      saveLocalFileArgs.noop = false;
      file = asset;
    }

    saveLocalFile(file, saveLocalFileArgs)
      .then(function(localFile) {
        return checkImageSize(localFile.path)
          .then(function(fileData) {
            const fileName = _.join([localFile.uuid, localFile.extension], '.');
            const remotePath = _.join([remoteDirectory, fileName], '/');
            return uploadFileToBucket(remotePath, localFile.path)
              .then(function(objectURL) {

                if ( isUpdate ) {
                  return removeFileFromBucket(oldURL)
                    .then(function(oldURL) {
                      resolve(objectURL);
                    })
                    .catch(function(err) {
                      console.log(err);
                      reject(err);
                    });
                } else {
                  resolve(objectURL);
                }
              })
              .catch(function(err) {
                console.log(err);
                reject(err);
              });
          })
          .catch(function(err) {
            console.log(err);
            reject(err);
          });
      })
      .catch(function(err) {
        console.log(err);
        reject(err);
      });
  });
}

/**
 * uploadAsset
 *
 * @param asset
 * @param isUpdate
 * @returns {undefined}
 */
function uploadAsset(asset, isUpdate) {
  return new Promise(function(resolve, reject) {
    console.log('uploadAsset()');

    const bucketPathForAssetType = { offerImage: 'offers', userImage: 'users' };

    if ( ! _.has(bucketPathForAssetType, asset.type) ) {
      reject(asset);
    }

    const bucketPath = bucketPathForAssetType[asset.type];

    uploadImage(asset, bucketPath, isUpdate)
      .then(function(objectURL) {
        asset.url = objectURL;
        resolve(asset);
      })
      .catch(function(err) {
        console.log(err);
        reject(err);
      });
  });
}

module.exports = {
  extractImageData: extractImageData,
  resizeImage: resizeImage,
  cropImage: cropImage,
  uploadAsset: uploadAsset,
};

