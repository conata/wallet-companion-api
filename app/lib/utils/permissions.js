/**
 * Permission Checks
 */

function createAdminPermissionCheck(user) {
  if (user.isAdmin()) {
    return true;
  }

  return false;
}

function modifyUserPermissionCheck(user, target) {

  if (user.isAdmin() || user._id.toString() === target._id.toString()) {
    return true;
  }

  return false;
}

function deleteUserPermissionCheck(user, target) {

  if (user.isAdmin()) {
    return true;
  }

  return false;
}

function modifyAssetPermissionCheck(user, asset) {

  if (user.isAdmin() || user._id.toString() === asset.owner._id.toString()) {
    return true;
  }

  return false;
}

function deleteAssetPermissionCheck(user, asset) {

  if (user.isAdmin() || user._id.toString() === asset.owner._id.toString()) {
    return true;
  }

  return false;
}

function createPolicyPermissionCheck(user) {

  if (user.isOfferBuyer() || user.isAdmin()) {
    return true;
  }

  return false;
}

function modifyPolicyPermissionCheck(user, policy) {

  if (user.isAdmin()) {
    return true;
  }
  else if ( user.isOfferBuyer() && user._id.toString() === policy.owner._id.toString() ) {
    return true;
  }

  return false;
}

function deletePolicyPermissionCheck(user, policy) {

  if (user.isAdmin()) {
    return true;
  }
  else if ( user.isOfferBuyer() && user._id.toString() === policy.owner._id.toString() ) {
    return true;
  }

  return false;
}

function createOfferPermissionCheck(user) {

  if (user.isOfferBuyer()) {
    return true;
  }

  return false;
}

function modifyOfferPermissionCheck(user, offer) {

  console.log(offer);

  if (user.isAdmin()) {
    return true;
  } else if ( user.isOfferBuyer() && user._id.toString() === offer.owner._id.toString() ) {
    return true;
  } else {
    return false;
  }
}

function deleteOfferPermissionCheck(user, offer) {

  if (user.isAdmin()) {
    return true;
  }

  return false;
}

function createContractPermissionCheck(user) {

  if (user.isBuyer()) {
    return true;
  }

  return false;
}

function viewContractPermissionCheck(user, contract, owner) {

  const buyer = contract.buyer;

  if (user.isAdmin()) {
    return true;
  }
  else if (user.isBuyer() && user._id.toString() === buyer._id.toString()) {
    return true;
  }
  else if (user.isOfferBuyer() && user._id.toString() === owner._id.toString()) {
    return true;
  }

  return false;
}

function modifyContractPermissionCheck(user, contract, owner) {

  const buyer = contract.buyer;

  if (user.isAdmin()) {
    return true;
  }
  else if (user.isBuyer() && user._id.toString() === buyer._id.toString()) {
    return true;
  }
  else if (user.isOfferBuyer() && user._id.toString() === owner._id.toString()) {
    return true;
  }

  return false;
}

function deleteContractPermissionCheck(user, contract) {

  if (user.isAdmin()) {
    return true;
  }

  return false;
}

module.exports = {
  createAdminPermissionCheck    : createAdminPermissionCheck,
  modifyUserPermissionCheck     : modifyUserPermissionCheck,
  deleteUserPermissionCheck     : deleteUserPermissionCheck,
  modifyAssetPermissionCheck    : modifyAssetPermissionCheck,
  deleteAssetPermissionCheck    : deleteAssetPermissionCheck,
  createPolicyPermissionCheck   : createPolicyPermissionCheck,
  modifyPolicyPermissionCheck   : modifyPolicyPermissionCheck,
  deletePolicyPermissionCheck   : deletePolicyPermissionCheck,
  createOfferPermissionCheck    : createOfferPermissionCheck,
  modifyOfferPermissionCheck    : modifyOfferPermissionCheck,
  deleteOfferPermissionCheck    : deleteOfferPermissionCheck,
  createContractPermissionCheck : createContractPermissionCheck,
  viewContractPermissionCheck   : viewContractPermissionCheck,
  modifyContractPermissionCheck : modifyContractPermissionCheck,
  deleteContractPermissionCheck : deleteContractPermissionCheck
};
