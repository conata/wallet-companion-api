const _ = require('lodash');
const moment = require('moment');

/**
 * Query Builders
 */

/**
 * extractFilterParams
 *
 * @param searchParams
 * @returns {undefined}
 */
function extractFilterParams(searchParams) {

  return function(queryKey, validKeys) {

    var extractedFilters = {};

    _.forEach(_.keys(searchParams[queryKey]), function(filterKey) {
      if ( _.includes(validKeys), filterKey ) {
        extractedFilters[filterKey] = searchParams[queryKey][filterKey];
      } else {
        console.log('Cannot filter for ' + filterKey);
      }
    });
    return extractedFilters;
  };
}


/**
 * appendToQuery
 *
 * @param filterKey
 * @param queryKey
 * @returns {undefined}
 */
function appendToQuery(filterKey, queryKey) {

  if ( ! queryKey ) {
    queryKey = filterKey;
  }

  return function(query, filter) {

    return function(queryParams) {
      if ( filter[filterKey] ) {
        query[queryKey] = queryParams;
      }
      return query;
    };
  };
}


/**
 * filterCoins
 *
 * @param filterParams
 * @returns {undefined}
 */
function filterCoins(filterParams) {

  var coinQuery = {};

  coinQuery = appendToQuery('roomType')(coinQuery, filterParams)({
    $in: filterParams.roomType
  });

  coinQuery = appendToQuery('offer')(coinQuery, filterParams)({
    $eq: filterParams.offer
  });

  if (filterParams.price) {
    var minPrice = filterParams.price.min || 0;
    var maxPrice = filterParams.price.max || 999999;

    coinQuery = appendToQuery('price')(coinQuery, filterParams)({
      $gte: minPrice,
      $lte: maxPrice
    });
  }

  if (filterParams.availability) {
    var startDate = moment().startOf('day');
    var endDate = moment(startDate).add(7, 'days');

    if (filterParams.availability.from) {
      startDate = moment(new Date(filterParams.availability.from)).startOf('day');
      endDate = moment(startDate).add(1, 'months');
    }

    coinQuery = appendToQuery('from', 'availability.from')(coinQuery, filterParams.availability)({
      $lte: startDate.toDate()
    });

    if (filterParams.availability.to) {
      endDate = moment(new Date(filterParams.availability.to)).startOf('day');
    }

    coinQuery = appendToQuery('to', 'availability.to')(coinQuery, filterParams.availability)({
      $gte: endDate.toDate()
    });
  }

  return coinQuery;
}


/**
 * filterOffers
 *
 * @param filterParams
 * @returns {undefined}
 */
function filterOffers(filterParams) {

  var offerQuery = {};

  offerQuery = appendToQuery('buyer')(offerQuery, filterParams)({
    $eq: filterParams.buyer
  });

  offerQuery = appendToQuery('assets')(offerQuery, filterParams)({
    $all: filterParams.assets
  });

  offerQuery = appendToQuery('offerType', 'profile.offerType')(offerQuery, filterParams)({
    $in: filterParams.offerType
  });

  if( filterParams.location ) {

    var minDistance = filterParams.location.minDistance || 0;
    var maxDistance = filterParams.location.maxDistance || 10000000;

    var coordinates = filterParams.location.coordinates || [ 0, 0 ];

    _.reverse(coordinates);

    offerQuery = appendToQuery('location', 'geometry.loc')(offerQuery, filterParams)({
      $near: {
        $geometry: {
          type: 'Point',
          coordinates: coordinates
        },
        $maxDistance:  maxDistance,
        $minDistance:  minDistance
      }
    });
  }

  return offerQuery;
}

/**
 * filterReservations
 *
 * @param filterParams
 * @returns {undefined}
 */
function filterReservations(filterParams) {

  var reservationQuery = {};

  reservationQuery = appendToQuery('status')(reservationQuery, filterParams)({
    $in: filterParams.status
  });

  reservationQuery = appendToQuery('buyer')(reservationQuery, filterParams)({
    $eq: filterParams.buyer
  });

  reservationQuery = appendToQuery('coins', 'coins.token')(reservationQuery, filterParams)({
    $in: filterParams.coins
  });

  if (filterParams.dates) {
    var startDate = moment().startOf('day');
    var endDate = moment(startDate).add(7, 'days');

    if (filterParams.dates.from) {
      startDate = moment(new Date(filterParams.dates.from)).startOf('day');
      endDate = moment(startDate).add(1, 'months');
    }

    if (filterParams.dates.to) {
      endDate = moment(new Date(filterParams.dates.to)).startOf('day');
    }

    reservationQuery = appendToQuery('from', 'dates.from')(reservationQuery, filterParams.dates)({
      $gte: startDate.toDate()
    });

    reservationQuery = appendToQuery('to', 'dates.to')(reservationQuery, filterParams.dates)({
      $lte: endDate.toDate()
    });
  }


  return reservationQuery;
}

module.exports = {
  extractFilterParams: extractFilterParams,
  filterCoins: filterCoins,
  filterOffers: filterOffers,
  filterReservations: filterReservations
};
