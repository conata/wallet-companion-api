/* jshint asi: true */

/**
 * INIT
 */

var Random = require("random-js");
var niceware = require('niceware');
var _ = require('lodash')
var log = console.log

/**
 * IMPLEMENTS
 */

/**
 * Vigènere
 *
 * @returns {undefined}
 */
class Vigenere {

  /**
   * alphabetLen_
   *
   * @returns {undefined}
   */
  static alphabetLen_() {
    return Vigenere.ALPHABET.length;
  }

  /**
   * encrypt
   *
   * @returns {undefined}
   */
  static encrypt(text, key) {
    let result = '';
    if ( ! _.isFunction(text.replace) ) {
      return result;
    }
    const preparedText = text.replace(new RegExp('\\s', 'g'), '').toLowerCase();
    const preparedKey = key.toLowerCase();
    const alphabetLen = Vigenere.alphabetLen_();
    for(let i = 0; i < preparedText.length; i++) {
      const char = preparedText[i];
      var l = preparedKey.length
      var id = i % l
      const keyChar = preparedKey[id];
      const shift =
          Vigenere.ALPHABET.indexOf(keyChar) % alphabetLen;
      const indexInAlphabet = Vigenere.ALPHABET.indexOf(char);
      result += Vigenere.ALPHABET[(indexInAlphabet + shift) % alphabetLen];
    }
    return result;
  }

  /**
   * decrypt
   *
   * @returns {undefined}
   */
  static decrypt(encrypted, key) {
    let result = '';
    const preparedKey = key.toLowerCase();
    const alphabetLen = Vigenere.alphabetLen_();
    for(let i = 0; i < encrypted.length; i++) {
      const char = encrypted[i];
      const keyChar = preparedKey[i % preparedKey.length];
      let shift = (
        Vigenere.ALPHABET.indexOf(char) - Vigenere.ALPHABET.indexOf(keyChar)
      );
      shift = (shift >= 0) ? shift : (alphabetLen + shift);
      result += Vigenere.ALPHABET[shift];
    }
    return result;
  }

}

// Vigenere.ALPHABET = 'abcdefghijklmnopqrstuvwxyz';
// Vigenere.ALPHABET = 'the cat is on the mat';


var logOfPassphrase;
var defaultSize;

try {

  logOfPassphrase = (process.argv[2] || []);
} catch(e) {

  console.log(e);
}

var _l = '--length=';
var m = 4;

if (logOfPassphrase.length) {
  try {

    var _s = logOfPassphrase.includes(_l);
    var size = _s ? logOfPassphrase : [_l, m].join('');
    var __l = size.split(_l);
    var ident = __l[1];
    var ___l = _.isNaN(ident);
  } catch(e) {

    console.log(e);
  }

  defaultSize = !___l ? ident : m;
} else {
  defaultSize = 16;
}

var _defaultSize = parseInt(defaultSize, 10);
var pp = niceware.generatePassphrase(_defaultSize);

// passphrase by unique char
Vigenere.ALPHABET = pp
  .join('')
  .split('')
  .filter(function(item, i, ar) {
    return ar.indexOf(item) === i;
  }).join('');

var legend = Vigenere.ALPHABET

/**
 * initLegend
 *
 * @returns {undefined}
 */
function initLegend() {

  var s = 16;
  var ds = parseInt(s, 10);
  var pass = niceware.generatePassphrase(ds);

  // passphrase by unique char
  var ll = pass
    .join('')
    .split('')
    .filter(function(item, i, ar) {
      return ar.indexOf(item) === i;
    }).join('');

  return ll;
}

var random = new Random(Random.engines.mt19937().autoSeed());

/**
 * gen
 *
 * @param arr
 * @param limit
 * @returns {undefined}
 */
function gen(arr, limit) {
  var a = null
  var id = null
  var _z = []
  var _a = []
  for(var i = 0; i < limit; ++i) {
    id = random.integer(1, arr.length)
    a = arr[id]
    _a.push(a)
    _z.push(id+'')
  }
  return {
    k: _a.join(''),
    j: _z.join('')
  }
}

// why generate possible redundancy?
const k1 = gen(legend, legend.length)
const k2 = gen(legend, legend.length)
const k3 = gen(legend, legend.length)
const k4 = gen(legend, legend.length)
const text = 'Lorem ipsum dolor sit amet'
const encrypted = Vigenere.encrypt(text, k1.k)
const decrypted = Vigenere.decrypt(encrypted, k1.k)

var patient = _.extend({}, {
  $id         : k1.j,
  passphrase : pp,
  legend     : legend,
  key        : [
    k1.k,
    k2.k,
    k3.k,
    k4.k
  ],
  text       : text,
  encrypted  : encrypted,
  decrypted  : decrypted
})

// var diagnostic = JSON.stringify(patient, null, 2)

// log(diagnostic)

/**
 * EXPORTS
 */

module.exports = {
  init    : initLegend,
  LEGEND  : legend,
  gen     : gen,
  encrypt : Vigenere.encrypt,
  decrypt : Vigenere.decrypt
};

