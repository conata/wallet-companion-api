const winston = require('winston');

const transports = {
  console: new winston.transports.Console({ level: 'warn' }),
  file: new winston.transports.File({ filename: 'combined.log', level: 'error' })
};

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    transports.console,
    transports.file
  ]
});

exports.logger = logger;
