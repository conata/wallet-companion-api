const _ = require('lodash');

const ActionTable = require('../../actions');

const ERRORS = require('../../errors').ERRORS;

/**
 * Response wrapper
 */

/**
 * sendResponse
 *
 * @param data
 * @returns {undefined}
 */
function sendResponse(data) {
  return function(res) {
    if ( data ) {
      return res.status(200).json(data);
    }
    return res.status(204).end();
  };
}

/**
 * Error Handling
 */

/**
 * lookupErrorByType
 *
 * @param type
 * @returns {undefined}
 */
function lookupErrorByType(type) {
  var error = _.find(ERRORS, function(e) {
    return e.type === type;
  });
  return error;
}

/**
 * throwError
 *
 * @param errorKey
 * @returns {undefined}
 */
function throwError(errorKey) {
  var error = lookupErrorByType(errorKey) || ERRORS.SERVER_FAULT;

  return function(res) {
    return res.status(error.status).json(
      {
        error: {
          type: error.type,
          message: error.message
        }
      }
    );
  };
}

/**
 * Action Dispatching
 */

/**
 * getActionHandler
 *
 * @param groupKey
 * @param actionKey
 * @returns {undefined}
 */
function getActionHandler(groupKey, actionKey) {

  const handler = ActionTable[groupKey][actionKey];

  const actionHandler = function(object) {

    return function(params) {

      console.log('Dispatching action: ' + groupKey + '::' + actionKey);

      if (handler) {

        return function(res) {

          var response;

          response = handler(object, params);

          if ( ! response ) {
            console.log('ActionHandler recieved an empty response');
            return throwError('RequestFailedError')(res);
          }
          else if ( response.error ) {
            console.log(response.error.msg);
            return throwError('RequestFailedError')(res);
          }
          else if ( _.isFunction(response) ) {

            const closureResponseCallback = function(err, data) {
              if (err) {
                console.log(err);
                return throwError('RequestFailedError')(res);
              }

              if ( ! _.isNil(data.error) ) {
                console.log('Error: ' + data.error.msg);
                return throwError('RequestFailedError')(res);
              }

              if ( data === 'noop' ) {
                return sendResponse()(res);
              }

              return sendResponse(data)(res);
            };

            return response(closureResponseCallback);
          }
          else if ( response.then && typeof(response.then) === 'function' ) {
            response
              .then(function(data) {
                return sendResponse(data)(res);
              })
              .catch(function(err) {
                console.log('promise rejected:', err);
                if ( lookupErrorByType(err.name) ) {
                  return throwError(err.name)(res);
                } else {
                  return throwError('RequestFailedError')(res);
                }
              });
          }
          else {
            return sendResponse(response)(res);
          }
        };
      }
      else {
        return function(res) {
          return throwError('InvalidParametersError')(res);
        };
      }
    };
  };

  return actionHandler;
}

/**
 * dispatchAction
 *
 * @param handler
 * @param params
 * @returns {undefined}
 */
function dispatchAction(handler, params) {
  return handler(params);
}

module.exports = {
  throwError: throwError,
  sendResponse: sendResponse,
  getActionHandler: getActionHandler,
  dispatchAction: dispatchAction
};
