const _ = require('lodash');
const aws = require('aws-sdk');
const nodemailer = require('nodemailer');

const bucket = process.env['WALLETCOMPANION_STORAGE_S3_BUCKET_PREFIX'];
const sender = process.env['WALLETCOMPANION_DEFAULT_EMAIL_SENDER'];

/**
 * Email
 */

const transporter = nodemailer.createTransport({
  SES: new aws.SES({
    apiVersion: '2010-12-01',
    region: 'us-east-1'
  })
});

/**
 * sendEmail
 *
 * @param params
 * @returns {undefined}
 */
function sendEmail(params) {

  var messageConfig = {
    from: sender,
    to: params.sendTo,
    subject: params.subject,
    text: params.body
  };

  if ( params.replyTo ) {
    messageConfig.replyTo = params.replyTo;
  }

  if ( process.env['NODE_ENV'] !== 'test' ) {

    transporter.sendMail(messageConfig, (err, info) => {
      if (err) {
        console.log(err);
      }
      console.log(info);
    });
  }
}

module.exports = {
  sendEmail: sendEmail,
};
