/* jshint asi:true */

const _ = require('lodash');
const moment = require('moment');

const Coin = require('../../models/Coin');
const User = require('../../models/User');
const Asset = require('../../models/Asset');
const Offer = require('../../models/Offer');
const Policy = require('../../models/Policy');
const Transaction = require('../../models/Transaction');
const Contract = require('../../models/Contract');

/**
 * Queries
 */

/**
 * lookupOfferForContract
 *
 * @param contract
 * @returns {undefined}
 */
function lookupOfferForContract(contract) {
  return function(errorHandler, successHandler) {
    _.forEach(contract.coins, function(contractCoin) {

      var coinRef = contractCoin.coin;

      Coin.populate(coinRef, {
        path: 'offer',
        model: 'Offer'
      }, function(err, coin) {
        if ( err ) {
          return errorHandler();
        }

        Offer.populate(coin.offer, {
          path: 'buyer',
          model: 'User'
        }, function(err, offer) {
          if ( err ) {
            return errorHandler();
          }

          return successHandler(offer);
        });
      });
    });
  };
}

/**
 * lookupContractsForUser
 *
 * @param user
 * @returns {undefined}
 */
function lookupContractsForUser(user) {
  if ( user.isBuyer() ) {
    return lookupContractsForBuyer(user);
  }
  else if ( user.isOfferBuyer() ) {
    return lookupContractsForBuyer(user);
  }
  else if (user.isAdmin() ) {
    return lookupContractsForAdmin(user);
  }
  else {
    return null;
  }
}

/**
 * lookupContractsForAdmin
 *
 * @param user
 * @returns {undefined}
 */
function lookupContractsForAdmin(user) {
  return function(errorHandler, successHandler) {
    Contract
      .find({ })
      .populate('buyer')
      .populate('coins.token')
      .exec(function(err, contracts) {

        if (err) {
          return errorHandler();
        }

        return successHandler(contracts);
      });
  };
}

/**
 * lookupContractsForBuyer
 *
 * @param user
 * @returns {undefined}
 */
function lookupContractsForBuyer(user) {
  return function(errorHandler, successHandler) {
    Contract
      .find({ buyer: user._id })
      .populate('buyer')
      .populate('coins.token')
      .exec(function(err, contracts) {

        if (err) {
          return errorHandler();
        }

        return successHandler(contracts);
      });
  };
}

/**
 * lookupContractsForBuyer
 *
 * @param user
 * @returns {undefined}
 */
function lookupContractsForBuyer(user) {
  return function(errorHandler, successHandler) {
    Offer
      .find({ buyer: user._id })
      .exec(function(err, offers) {

        if (err) {
          return errorHandler();
        }

        var offerIDs = _.map(offers, function(offer) {
          return offer._id;
        });

        Coin
          .find({ offer: { $in: offerIDs } })
          .exec(function(err, coins) {

            if (err) {
              return errorHandler();
            }

            var coinIDs = _.map(coins, function(coin) {
              return coin._id;
            });

            Contract
              .find({ 'coins.token': { $in : coinIDs } } )
              .populate('buyer')
              .populate('coins.token')
              .exec(function(err, contracts) {

                if (err) {
                  return errorHandler();
                }

                return successHandler(contracts);
              });
          });
      });
  };
}

/**
 * lookupContractsForOffer
 *
 * @param offer
 * @returns {undefined}
 */
function lookupContractsForOffer(offer) {
  return function(errorHandler, successHandler) {
    Contract
      .find({})
      .populate('buyer')
      .populate('coins.token')
      .exec(function(err, contracts) {

        if (err) {
          return errorHandler();
        }

        return successHandler(contracts);
      });
  };
}

/**
 * updateContract
 *
 * @param contract
 * @param updateParams
 * @returns {undefined}
 */
function updateContract(contract, updateParams) {

  const now = moment();

  var startDate = null;
  var endDate = null;

  // change contract dates
  if ( _.has(updateParams, 'dates.from')) {
    startDate = moment(new Date(updateParams.dates.from));
  } else {
    startDate = moment(new Date(contract.dates.from));
  }

  if ( _.has(updateParams, 'dates.to') ) {
    endDate = moment(new Date(updateParams.dates.to));
  } else {
    endDate = moment(new Date(contract.dates.to));
  }

  _.forEach(contract.coins, function(contractCoin) {
    var coinstartDate = moment(new Date(contractCoin.coin.availability.from));
    var coinEndDate = moment(new Date(contractCoin.coin.availability.to));

    if ( startDate.isBefore(coinstartDate, 'day') || endDate.isAfter(coinEndDate, 'day')) {
      // Note: Should be an error
      return contract;
    }
  });

  if ( endDate.isBefore(startDate, 'days') || startDate.isBefore(now, 'days')) {
    // Note: Should be an error
    return contract;
  } else {
    contract.dates.from = startDate;
    contract.dates.to = endDate;
  }

  // add coins or update quantity of desired coins to contract
  if ( updateParams.coins ) {

    _.forEach(updateParams.coins, function(updateCoin) {

      var existingCoin = _.find(contract.coins, function(coinRef) {
        return coinRef.coin._id === updateCoin.coin;
      });

      if ( existingCoin ) {
        existingCoin.quantity = updateCoin.quantity;
      } else {
        contract.coins.push(updateParams.coin);
      }
    });
  }

  return contract;
}

/**
 * calculateCostForContract
 *
 * @param contract
 * @param cb
 * @returns {undefined}
 */
function calculateCostForContract(contract, cb) {

  const now = moment();

  var startDate = moment(contract.dates.from);
  var endDate = moment(contract.dates.to);
  var duration = moment.duration(endDate.diff(startDate)).asDays();

  var totalCost = 0.0;
  var subtotal = 0.0;
  var fees = {};

  var policyFeeKeys = [
    'serviceFee',
    'salesTax',
    'occupancyTax',
    'securityDeposit',
    'cleaningFee'
  ];

  Offer.findOfferByContract(contract, function(err, offer) {

    if ( ! _.isNil(offer.policy) ) {
      var rates = getFeesForPolicy(offer.policy);

      _.forEach(policyFeeKeys, function(policyKey) {
        if ( ! _.isNil(rates[policyKey]) ) {
          fees[policyKey] = {
            rate: rates[policyKey],
            charge: 0.0
          };
        } else {
          fees[policyKey] = {
            rate: 0.0,
            charge: 0.0
          };
        }
      });
    }

    _.forEach(contract.coins, function(coinRef) {
      var coinCostPerDay = coinRef.coin.price;
      var totalCostPerDay = coinCostPerDay * coinRef.quantity;
      var totalCostForCoin = totalCostPerDay * duration;
      subtotal += totalCostForCoin;

      // Apply the cleaning fee on each coin reserved
      if ( ! _.isNil(fees.cleaningFee) ) {
        fees.cleaningFee.charge += fees.cleaningFee.rate * coinRef.quantity;
        totalCost += fees.cleaningFee.charge;
      }
    });

    // Apply the service fee
    if ( ! _.isNil(fees.serviceFee) ) {
      fees.serviceFee.charge = fees.serviceFee.rate * subtotal;
      totalCost += fees.serviceFee.charge;
    }

    // Apply the sales tax rate
    if ( ! _.isNil(fees.salesTax) ) {
      fees.salesTax.charge = fees.salesTax.rate * subtotal;
      totalCost += fees.salesTax.charge;
    }

    // Apply the occupancy sales tax rate
    if ( ! _.isNil(fees.occupancyTax) ) {
      fees.occupancyTax.charge = fees.occupancyTax.rate * subtotal;
      totalCost += fees.salesTax.charge;
    }

    // Apply the security deposit amount
    // NOTE: Security deposits are disabled, since there is no mechanism for refunding them yet
    // if ( ! _.isNil(fees.securityDeposit) ) {
    //   fees.securityDeposit.charge = fees.securityDeposit.rate;
    //   totalCost += fees.securityDeposit.charge;
    // }

    totalCost += subtotal;

    return cb(contract, {
      subtotal: subtotal,
      fees: fees,
      total: totalCost
    });
  });
}

/**
 * createTransaction
 *
 * @param contract
 * @param charges
 * @returns {undefined}
 */
function createTransaction(contract, charges) {

  var transactionData = {
    contract: contract._id,
    subtotal: charges.subtotal,
    amountDue: charges.total
  };

  _.forEach([
    'serviceFee',
    'salesTax',
    'occupancyTax',
    'cleaningFee',
    'securityDeposit'
  ], function(feeKey) {
    if ( ! _.isNil(charges.fees[feeKey]) ) {
      transactionData[feeKey] = charges.fees[feeKey].charge;
    }
  });

  const transaction = new Transaction(transactionData);

  transaction.save((err) => {
    if (err) {
      console.log(err);
      return { error: { msg: err } };
    }

    return contract;
  });
}

/**
 * updateTransaction
 *
 * @param contract
 * @param charges
 * @returns {undefined}
 */
function updateTransaction(contract, charges) {
  contract.save((err, contract) => {
    if (err) {
      console.log(err);
      return { error: { msg: err } };
    }

    Transaction
      .findTransactionForContract(contract._id, (err, transaction) => {

        if (err) {
          console.log(err);
          return { error: { msg: err } };
        }

        transaction.setSubtotal(charges.subtotal);
        transaction.setServiceFee(charges.fees.serviceFee.charge);
        transaction.setSalesTax(charges.fees.salesTax.charge);
        transaction.setOccupancyTax(charges.fees.occupancyTax.charge);
        transaction.setCleaningFee(charges.fees.cleaningFee.charge);
        // NOTE: Security deposits are disabled, since there is no mechanism for refunding them yet
        //transaction.setSecurityDeposit(charges.fees.securityDeposit.charge);
        transaction.setAmountDue(charges.total);

        transaction.save((err) => {
          if (err) {
            console.log(err);
            return { error: { msg: err } };
          }

          return contract;
        });
      });
  });
}

/**
 * getFeesForPolicy
 *
 * @param policy
 * @returns {undefined}
 */
function getFeesForPolicy(policy) {

  var rates = {};

  // NOTE: Security deposits are disabled, since there is no mechanism for refunding them yet
  // if ( policy.hasSecurityDeposit() ) {
  //  rates.securityDepsoit = policy.getSecurityDeposit();
  // }

  if ( policy.hasCleaningFee() ) {
    rates.cleaningFee = policy.getCleaningFee();
  }

  if ( policy.hasServiceFee() ) {
    rates.serviceFee = policy.getServiceFee();
  }

  if ( policy.hasSalesTax() ) {
    rates.salesTax = policy.getSalesTaxRate();
  }

  if ( policy.hasOccupancyTax() ) {
    rates.occupancyTax = policy.getOccupancyTaxRate();
  }

  return rates;
}

module.exports = {
  lookupOfferForContract: lookupOfferForContract,
  lookupContractsForUser: lookupContractsForUser,
  calculateCostForContract: calculateCostForContract,
  updateContract: updateContract,
  createTransaction: createTransaction,
  updateTransaction: updateTransaction
};
