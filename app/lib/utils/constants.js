const countrynames = require('countrynames');

const CONSTANTS = require('../../constants').EXPORT_CONSTANTS; // All constants

/**
 * Constants
 */

/**
 * getConstant
 *
 * @param constantKey
 * @returns {undefined}
 */
var getConstant = function(constantKey) {
  if ( ! constantKey ) {
    return CONSTANTS;
  }

  return CONSTANTS[constantKey];
};

/**
 * getCountryCode
 *
 * @param countryName
 * @returns {undefined}
 */
var getCountryCode = function(countryName) {
  return countrynames.getCode(countryName);
};

/**
 * getCountryCodes
 *
 * @returns {undefined}
 */
var getCountryCodes = function() {
  return CONSTANTS['COUNTRY_CODES'];
};

/**
 * getCountryName
 *
 * @param countryCode
 * @returns {undefined}
 */
var getCountryName = function(countryCode) {
  return countrynames.getName(countryCode);
};

module.exports = {
  getConstant: getConstant,
  getCountryCode: getCountryCode,
  getCountryCodes: getCountryCodes,
  getCountryName: getCountryName
};
