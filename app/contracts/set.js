const Web3 = require('web3')
var _httpProvider = 'http://localhost:8545'
var httpProvider = 'https://api.myetherapi.com/eth'
const web3 = new Web3(new Web3.providers.HttpProvider(_httpProvider))
const SetData = fs.readFileSync('../../contracts/lib/Set.sol').toString()
const solc = require('solc')

/**
 * GET /api/v1/contracts
 *
 * @description
 */
exports.has = (req, res, next) => {
  res.json({
    '_links': { 'self': { 'href': 'http://example.com' } },
    'id': '',
    '$value': true
  })
}

/**
 * GET /api/v1/contracts
 *
 * @description
 * A (presumably) restful microservice for solidity contract parameterization
 * over data structure over data structures; 'restful distributed data
 * structures', etc. This way, HTTP OPTIONS 'parameters' can consist of a list
 * of data structures used to compose the contract.
 */
exports.values = (req, res, next) => {

  const __set__ = solc.compile(SetData)
  const abiDefinition = JSON.parse(__set__.contracts[':Set'].interface)
  const SetContract = web3.eth.contract(abiDefinition)
  const SetByteCode = __set__.contracts[':Set'].bytecode

  deploy().then(function(instance) {

    instance.getMembers.call()

    res.json({
      '_links': {
        'self': {
          'href': 'http://example.com'
        }
      },
      'id': '',
      '_embedded': {
        'values': instance.values({
          from: web3.eth.accounts[0]
        })
      }
    })
  })

  ////////////////

  function deploy() {
    const deployedSet = SetContract.new({
      data : SetByteCode,
      from : web3.eth.accounts[0],
      gas  : 6900000
    })

    return new Promise(function(resolve, reject) {
      let contractInstance
      try {
        contractInstance = SetContract.at(deployedSet.address)
        resolve(contractInstance)
      } catch(err) {
        reject({ error: err })
      } finally {
        console.log(contractInstance)
      }
    })
  }


}
