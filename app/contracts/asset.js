const _ = require('lodash');

const Asset = require('../models/Asset');

const utils = require('../lib/utils');

const throwError = utils.handlers.throwError;
const sendResponse = utils.handlers.sendResponse;
const getActionHandler = utils.handlers.getActionHandler;
const dispatchAction = utils.handlers.dispatchAction;

const uploadAsset = utils.storage.uploadAsset;

const modifyAssetPermissionCheck = utils.permissions.modifyAssetPermissionCheck;
const deleteAssetPermissionCheck = utils.permissions.deleteAssetPermissionCheck;

/**
 * GET /api/v1/assets
 */
exports.listAssets = (req, res, next ) => {

  const user = res.locals.user;

  var page = req.query.page || 1;

  if ( ! _.isInteger(page) ) {
    page = _.toInteger(page);
  }

  var limit = req.query.limit || 10;

  if ( ! _.isInteger(limit) ) {
    limit = _.toInteger(limit);
  }

  var query = {};

  if ( ! user.isAdmin() ) {
    query = { buyer: user._id };
  }

  Asset
    .paginate(query, { page: page, limit: limit })
    .then(function(result) {
      return sendResponse({assets: result.docs})(res);
    })
    .catch(function(err) {
      if ( err ) {
        return throwError('RequestFailedError')(res);
      }
    });
};


/**
 * POST /api/v1/assets
 */

exports.createAsset = (req, res, next ) => {

  const assetBuyer = res.locals.user._id;
  const assetType  = req.body.type;

  var assetList = [];

  if ( req.files ) {

    const assetFile  = req.files.photo;

    uploadAsset({buyer: assetBuyer, type: assetType, file: assetFile })
      .then(function(assetData) {
        const asset = new Asset(assetData);
        asset.save(function(err) {
          if ( err ) {
            return throwError('RequestFailedError')(res);
          }
          return sendResponse({ asset: asset })(res);
        });
      })
      .catch(function(err) {
        if ( err && err.message) {
          console.log(err);
          return throwError(err.message)(res);
        } else {
          return throwError('RequestFailedError')(res);
        }
      });
  } else {
    return throwError('InvalidParametersError')(res);
  }
};

/**
 * GET /api/v1/assets/:id
 */
exports.getAsset = (req, res, next ) => {
  Asset
    .findById(req.params.id)
    .exec(function(err, asset) {
      if ( err ) {
        return throwError('RequestFailedError')(res);
      }

      return sendResponse({asset: asset})(res);
    });
};

/**
 * POST /api/v1/assets/:id/action
 */

exports.dispatchAssetAction = (req, res, next) => {

  const actionKey = req.body.action.key;
  const actionParams = req.body.action.params || {};

  const assetActions = getActionHandler('asset', actionKey);

  Asset
    .findById(req.params.id)
    .populate('buyer')
    .exec((err, asset) => {
      if (err) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      if ( ! modifyAssetPermissionCheck(res.locals.user, asset) ) {
        return throwError('RolePermissionError')(res);
      }

      const actionHandler = assetActions(asset);

      return dispatchAction(actionHandler, actionParams)(res);
    });
};

/**
 * DELETE /api/v1/assets/:id
 */
exports.deleteAsset = (req, res, next ) => {
  Asset
    .findById(req.params.id)
    .exec(function(err, asset) {
      if ( err ) {
        return throwError('RequestFailedError')(res);
      }

      asset.remove((err) => {
        if (err) {
          return throwError('RequestFailedError')(res);
        }
        return sendResponse()(res);
      });
    });
};
