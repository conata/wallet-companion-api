/* jshint asi:true */
'use strict';

const _ = require('lodash');
const bluebird = require('bluebird');
const crypto = bluebird.promisifyAll(require('crypto'));
const passport = require('passport');
const niceware = require('niceware');
const nodemailer = require('nodemailer');
const moment = require('moment');

const User = require('../models/User');

const utils = require('../lib/utils');

const throwError = utils.handlers.throwError;
const sendResponse = utils.handlers.sendResponse;
const getActionHandler = utils.handlers.getActionHandler;
const dispatchAction = utils.handlers.dispatchAction;

const ROLE_MEMBER = utils.constants.getConstant('ROLE_MEMBER');
const ROLE_ADMIN =  utils.constants.getConstant('ROLE_ADMIN');

const sendEmail = utils.email.sendEmail;

const createAdminPermissionCheck = utils.permissions.createAdminPermissionCheck;
const modifyUserPermissionCheck = utils.permissions.modifyUserPermissionCheck;
const deleteUserPermissionCheck = utils.permissions.deleteReservationPermissionCheck;

const environment = process.env['NODE_ENV'];

/**
 * @apiDefine UserObjectResponse
 *
 * @apiSuccess {Object} user User
 * @apiSuccess {String} user.id User ID
 * @apiSuccess {String} user.email Email Address
 * @apiSuccess {String} user.role User Role
 * @apiSuccess {Object} user.profile User Profile
 * @apiSuccess {Object} user.profile.name Name details
 * @apiSuccess {String} user.profile.name.first First name
 * @apiSuccess {String} user.profile.name.last Last name
 * @apiSuccess {String} user.profile.gender Gender
 * @apiSuccess {Object} user.profile.location Location
 * @apiSuccess {String} profile.location.address Address
 * @apiSuccess {String} profile.location.city City
 * @apiSuccess {String} profile.location.state State/Province
 * @apiSuccess {String} profile.location.country Country Code
 * @apiSuccess {Object} user.verification Verification Details
 * @apiSuccess {Boolean} user.verification.isVerified True if dentity been verified
 * @apiSuccess {Boolean} user.verification.isComplete True if profile setup is completed
 */


/**
 * Handle OPTIONS Request for CORS Pre-Flight headers
 */

function authOptions(req, res, next) {
  return res.status(200).end();
}

/**
 * @api {post} /api/v1/auth Authenticate User
 * @apiName auth
 * @apiGroup Authentication
 *
 * @apiParam {String} email Email for user
 * @apiParam {String} password Password
 *
 * @apiUse UserObjectResponse
 * @apiError error Error object
 */

function authenticate(req, res, next) {

  req.assert('email', 'Email cannot be empty').notEmpty();
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('password', 'Password cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  const errors = req.validationErrors();

  if (errors) {
    return throwError('InvalidParametersError')(res);
  }

  passport.authenticate('local', (err, user, info) => {
    if ( err || ! user ) {
      console.log(err);
      return throwError('InvalidLoginError')(res);
    }

    // WARN: Hackish
    if ( ! user.isVerified() ) {
      if ( environment === 'production' ) {
        return throwError('AccountPendingUserVerificationError')(res)
      } else {
        console.log('[DEBUG] User account is not verified')
      }
    }

    // WARN: Hackish
    if ( ! user.isApproved() ) {
      if ( environment === 'production' ) {
        return throwError('AccountPendingAdminApprovalError')(res)
      } else {
        console.log('[DEBUG] User account is not admin approved')
      }
    }

    req.logIn(user, (err) => {
      if (err) {
        console.log(err)
          return throwError('RequestFailedError')(res)
        }

      User
        .findById(user._id, (err, fullUser) => {
          if (err) {
            console.log(err)
            return throwError('RequestFailedError')(res)
          }
          return sendResponse({user: fullUser})(res);
        });
    });
  })(req, res, next);
}

/**
 * @api {get} /api/v1/users List Users
 * @apiName listUsers
 * @apiGroup Users
 *
 * @apiSuccess {Object[]} users Array of User objects
 *
 * @apiError error Error object
 */
exports.listUsers = (req, res, next) => {

  const user = res.locals.user

  if ( ! user.isAdmin() ) {
    return throwError('RolePermissionError')(res)
  }

  var page = req.query.page || 1

  if ( ! _.isInteger(page) ) {
    page = _.toInteger(page)
  }

  var limit = req.query.limit || 10

  if ( ! _.isInteger(limit) ) {
    limit = _.toInteger(limit)
  }

  User
    .paginate({}, { page: page, limit: limit, populate: 'profile.avatar' })
    .then(function(result) {
      return sendResponse({users: result.docs})(res)
    })
    .catch(function(err) {
      if (err) {
        console.log(err)
        return throwError('RequestFailedError')(res)
      }
    })
}

/**
 * @api {get} /api/v1/users/:id Get User
 * @apiName getUser
 * @apiGroup Users
 *
 * @apiParam {ObjectId} id User ID
 *
 * @apiUse UserObjectResponse
 * @apiError error Error object
 */
exports.getUser = (req, res, next) => {
  User
    .findById(req.params.id)
    .populate('profile.avatar')
    .exec((err, user) => {
      if (err) {
        console.log(err)
        return throwError('RequestFailedError')(res)
      }
      return sendResponse({user: user})(res)
    })
};

/**
 * @api {post} /api/v1/users Create User
 * @apiName createUser
 * @apiGroup Users
 *
 * @apiParam {String} email Email for new user
 * @apiParam {String} password Password for the new user
 * @apiParam {String} confirmPassword Confirmation of the password
 * @apiParam {Enum} role=ROLE_MEMBER The role/type of account
 * @apiParam {Object} profile User details
 * @apiParam {Object} profile.name Name details
 * @apiParam {String} profile.name.first First name
 * @apiParam {String} profile.name.last Last name
 * @apiParam {String} profile.gender Gender
 * @apiParam {Object} profile.location Location
 * @apiParam {String} profile.location.address Address
 * @apiParam {String} profile.location.city City
 * @apiParam {String} profile.location.state State/Province
 * @apiParam {String} profile.location.country Country Code
 *
 * @apiUse UserObjectResponse
 * @apiError error Error object
 *
 */
exports.createUser = (req, res, next) => {

  req.assert('email', 'Email cannot be empty').notEmpty()
  req.assert('email', 'Email is not valid').isEmail()
  req.assert('password', 'Password must be at least 4 characters long').len(4)
  req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password)
  req.sanitize('email').normalizeEmail({ remove_dots: false })

  if (req.validationErrors()) {
    return throwError('InvalidParametersError')(res)
  }

  if ( req.body.role === ROLE_ADMIN && ! createAdminPermissionCheck(res.locals.user) ) {
    return throwError('RolePermissionError')(res)
  }

  var errorOut

  const createRandomToken = crypto
        .randomBytesAsync(16)
        .then(buf => buf.toString('hex'))

  var userData = {
    email: req.body.email,
    password: req.body.password,
    profile: req.body.profile,
    role: req.body.role,
  }

  const stripe = req.app.locals.stripe

  const now = moment().unix()

  User
    .findOne({ email: userData.email }, (err, existingUser) => {
      if (err) {
        console.log(err)
        return throwError('RequestFailedError')(res)
      }

      if (existingUser) {
        return throwError('UserExistsError')(res)
      }

      // Create a closure around the userData for sending the account creation email notification
      const sendConfirmationEmail = function(user) {
        const token = user.verificationToken

        const baseUrl = process.env['WALLETCOMPANION_WEBAPP_URL']

        const resetLink = _.join([baseUrl, 'verify', token], '/')

        const accountCreationText = [
          'Congratulations, your BackPacker College account has been created!',
          'Please click here to verify your account:',
          resetLink,
          'Once you have verified your account, you can login right away.'
        ].join('\n\n')

        const emailParams = {
          sendTo: user.email,
          subject: 'Your BackPacker College Account',
          body: accountCreationText
        }

        return sendEmail(emailParams)
      }

      // Create a closure to stay DRY in handling the stripe user creation race condition
      const createUserFn = function() {

        try {

          stripe.customers.create({email: userData.email})
            .then(function(customer) {
              userData.stripe = { customerId: customer.id }

              buildUser(userData)
            })
            .catch(function(err) {
              console.log(err)
              return throwError('RequestFailedError')(res)
            })
        } catch(err) {
          console.log(err)
          buildUser(userData)
        }

        function buildUser(userData) {
          createRandomToken
            .then(function(token) {

              userData.verificationToken = token
              userData.verificationExpires = Date.now() + ( 24 * 7 ) * 3600000

              const user = new User(userData)

              // WARN: Hackish
              //if ( environment === 'develop' || environment === 'test' ) {
                user.setProfileVerified()
              //}

              user.save(function(err) {
                if (err) {
                  return throwError('RequestFailedError')(res)
                }
                sendConfirmationEmail(user)
                return sendResponse({user: user})(res)
              })
            })
            .catch(function(err) {
              console.log(err)
              return throwError('RequestFailedError')(res)
            })
        }
      }

      try {

        // Check for stripe customers created in the last 5 minutes,
        // so we do not create duplicate stripe customers
        stripe.customers.list({
          created: {
            gte: now - (60 * 5),
            lte: now
          }
        })
          .then(function(customers) {

            var existingCustomer = _.find(customers.data, function(customer) {
              return customer.email === userData.email
            })

            if ( ! _.isUndefined(existingCustomer) ) {
              console.log('Stripe customer already exists - deleting old customer:', existingCustomer)
              stripe.customers.del(existingCustomer.id)
                .then(function(response) {
                  if ( ! response.deleted ) {
                    return throwError('RequestFailedError')(res)
                  }
                  createUserFn()
                })
                .catch(function(err) {
                  console.log(err)
                  return throwError('RequestFailedError')(res)
                })
            } else {
              createUserFn()
            }
          })
          .catch(function(err) {
            console.log(err)
            return throwError('RequestFailedError')(res)
          })

      } catch(err) {
        errorOut = err
        createUserFn()
      } finally {
        console.log(errorOut)
      }
    })
};

/**
 * @api {put} /api/v1/users/:id Update User
 * @apiName updateUser
 * @apiGroup Users
 *
 * @apiParam {Object} profile User details
 * @apiParam {Object} profile.name Name details
 * @apiParam {String} profile.name.first First name
 * @apiParam {String} profile.name.last Last name
 * @apiParam {String} profile.gender Gender
 * @apiParam {String} profile.location Location
 *
 * @apiError error Error object
 */
exports.updateUser = (req, res, next) => {

  const updateKeys = [ 'name', 'gender', 'location', 'phone', 'avatar' ]

  var updateQuery = {}

  _.forEach(updateKeys, function(key) {
    if ( _.has(req.body, key) ) {
      if ( _.isPlainObject(req.body[key]) ) {
        _.forEach(_.keys(req.body[key]), function(k) {
          var attributePath = _.join([ 'profile', key, k ], '.')
          updateQuery[attributePath] = req.body[key][k]
        })
      } else {
        var attributePath = _.join([ 'profile', key ], '.')
        updateQuery[attributePath] = req.body[key]
      }
    }
  })

  User.findById(req.params.id, (err, user) => {
    if (err) {
      return throwError('RequestFailedError')(res)
    }

    if (! modifyUserPermissionCheck(res.locals.user, user)) {
      return throwError('RolePermissionError')(res)
    }

    User
      .findByIdAndUpdate(req.params.id, {
        $set: updateQuery
      }, {
        new: true
      }, (err, updatedUser) => {
        if (err) {
          console.log(err)
          return throwError('RequestFailedError')(res)
        }

        return sendResponse({user: updatedUser})(res)
      })
  })
};

/**
 * @api {post} /api/v1/users/:id/action Dispatch Action
 * @apiName dispatchUserAction
 * @apiGroup Users
 *
 * @apiParam {String} id User id
 * @apiParam {Object} action User Action
 * @apiParam {String} action.key Action Key
 * @apiParam {Object} action.params Action Parameters
 *
 * @apiDescription Documentation for specific actions will
 * assume it is wrapped in an action object with the
 * appropriate key, only values for action.params will be
 * documented for a given action.
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
exports.dispatchUserAction = (req, res, next) => {
  const actionKey = req.body.action.key
  const actionParams = req.body.action.params || {}
  const userActions = getActionHandler('user', actionKey)

  var stripeActions = [
    'addPaymentMethod',
    'getPaymentMethods',
    'editPaymentMethod',
    'removePaymentMethod',
  ]

  if ( process.env['NODE_ENV'] !== 'production' ) {
    stripeActions.push('addDummyPaymentMethod')
  }

  const adminActions = [
    'setUserAdmin',
    'setProfileApproved'
  ]

  if ( _.includes(stripeActions, actionKey) ) {
    actionParams.stripe = req.app.locals.stripe
  }

  if ( _.includes(adminActions, actionKey) && ! res.locals.user.isAdmin() ) {
    return throwError('RolePermissionError')
  }

  User
    .findById(req.params.id)
    .select('stripe')
    .populate('profile.avatar')
    .exec((err, user) => {
      if (err) {
        console.log(err)
        return throwError('RequestFailedError')(res)
      }

      if ( ! modifyUserPermissionCheck(res.locals.user, user) ) {
        return throwError('RolePermissionError')(res)
      }

      const actionHandler = userActions(user)

      return dispatchAction(actionHandler, actionParams)(res)
    })
};

/**
 * @api {delete} /api/v1/users/:id Delete User
 * @apiName deleteUser
 * @apiGroup Users
 *
 * @apiParam {ObjectId} id User id
 *
 * @apiError error Error object
 */
exports.deleteUser = (req, res, next) => {

  const stripe = req.app.locals.stripe

  User.findById(req.params.id, (err, user) => {
    if (err) {
      console.log(err)
      return throwError('RequestFailedError')(res)
    }

    if (! deleteUserPermissionCheck(res.locals.user, user)) {
      return throwError('RolePermissionError')(res)
    }

    var stripeCustomerID = user.stripe.customerId

    if ( stripeCustomerID ) {
      stripe.customers.del(stripeCustomerID)
        .then(function(response) {
          if ( ! response.deleted ) {
            return throwError('RequestFailedError')(res)
          }
        })
        .catch(function(err) {
          console.log(err)
          return throwError('RequestFailedError')(res)
        })
    }
  })

  User
    .remove({ _id: req.user.id }, (err) => {
      if (err) {
        console.log(err)
        return throwError('RequestFailedError')(res)
      }
      return sendResponse()(res)
    })
};

/**
 * GET /account
 */
exports.getAccount = (req, res, next) => {
  User.findById(res.locals.user._id)
    .populate('profile.avatar')
    .exec((err, user) => {
      if (err) {
        console.log(err)
        return throwError('RequestFailedError')(res)
      }
      return sendResponse({user: user})(res)
    })
};

/**
 * DELETE /account
 */
exports.deleteAccount = (req, res, next) => {
  User.remove({ _id: res.locals.user._id}, (err) => {
    if (err) {
      console.log(err)
      return throwError('RequestFailedError')(res)
    }
    return sendResponse()(res)
  })
};

/**
 * POST /account/password
 */
exports.updatePassword = (req, res, next) => {
  req.assert('password', 'Password must be at least 4 characters long').len(4)
  req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password)

  if (req.validationErrors()) {
    return throwError('InvalidParametersError')(res)
  }

  User
    .findById(req.user.id)
    .select('+password')
    .exec((err, user) => {
      if (err) {
        console.log(err)
        return throwError('RequestFailedError')(res)
      }
      user.password = req.body.password
      user.save((err) => {
        if (err) {
          console.log(err)
          return throwError('RequestFailedError')(res)
        }
        return sendResponse()(res)
      })
    })
};

/**
 * GET /passphrase
 *
 * @description For passphrase suggestions.
 * @example
 *   {
 *     passphrase: 'brownstone suburbed laxer debug'
 *   }
 */
exports.getPassphrase = (req, res, next) => {
  var passphrase, activityConstruct, outcome
  try {
    passphrase = niceware.generatePassphrase(8)
    activityConstruct = {
      passphrase: passphrase
    }
    outcome = sendResponse(activityConstruct)(res)
  } catch (err) {
    console.log(err)
    outcome = throwError('RequestFailedError')(res)
  } finally {
    return outcome
  }
};

/**
 * GET /verify/:token
 */

exports.getVerification = (req, res, next) => {
  User
    .findOne({ verificationToken: req.params.token })
    .where('verificationExpires').gt(Date.now())
    .select('+verificationToken')
    .select('+verificationExpires')
    .exec((err, user) => {
      if (err) { return next(err) }
      if (!user) {
        return throwError('InvalidAccountVerificationTokenError')(res)
      }
      return sendResponse({ user: user })(res)
    })
};

/**
 * POST /verify/:token
 */

exports.postVerification = (req, res, next) => {
  User
    .findOne({ verificationToken: req.params.token })
    .select('+verificationToken')
    .select('+verificationExpires')
    .where('verificationExpires').gt(Date.now())
    .exec((err, user) => {
      if (err) { return next(err) }
      if (!user) {
        return throwError('InvalidAccountVerificiationTokenError')(res)
      }

      user.setProfileVerified()

      user.save((err) => {
        if (err) {
          console.log(err)
          return throwError('RequestFailedError')(res)
        }
        return sendResponse()(res)
      })
    })
};

/**
 * GET /reset/:token
 */
exports.getReset = (req, res, next) => {
  User
    .findOne({ passwordResetToken: req.params.token })
    .where('passwordResetExpires').gt(Date.now())
    .select('+passwordResetToken')
    .select('+passwordResetExpires')
    .exec((err, user) => {
      if (err) { return next(err) }
      if (!user) {
        return throwError('InvalidPasswordResetTokenError')(res)
      }
      return sendResponse({ user: user })(res)
    })
};

/**
 * POST /reset/:token
 */
exports.postReset = (req, res, next) => {
  req.assert('password', 'Password must be at least 4 characters long').len(4)
  req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password)

  if (req.validationErrors()) {
    return throwError('InvalidParametersError')(res)
  }

    User
    .findOne({ passwordResetToken: req.params.token })
    .where('passwordResetExpires').gt(Date.now())
    .select('+password')
    .exec((err, user) => {
      if (err) { return next(err) }
      if (!user) {
        return throwError('InvalidPasswordResetTokenError')(res)
      }

      user.password = req.body.password

      user.save((err) => {
        if (err) {
          console.log(err)
          return throwError('RequestFailedError')(res)
        }
        return sendResponse()(res)
      })
    })
};

/**
 * GET /forgot
 */

function getForgot(req, res) {
  res.status(200).json({msg: 'Not implemented'})
}

/**
 * POST /forgot
 */

function postForgot(req, res, next) {

  req.assert('email', 'Please enter a valid email address.').isEmail()
  req.sanitize('email').normalizeEmail({ remove_dots: false })

  const errors = req.validationErrors()

  if (errors) {
    return throwError('InvalidParametersError')(res)
  }

  const createRandomToken = crypto
        .randomBytesAsync(16)
        .then(buf => buf.toString('hex'))

  const setRandomToken = token =>
    User
    .findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        return throwError('UserNotFoundError')(res)
      } else {
        user.passwordResetToken = token
        user.passwordResetExpires = Date.now() + 3600000 // 1 hour
        user = user.save()
      }
      return user
    });

  const sendForgotPasswordEmail = user => {

    if (!user) {
      console.log('No user')
      return
    }

    const token = user.passwordResetToken

    const baseUrl = process.env['WALLETCOMPANION_WEBAPP_URL']

    const resetLink = _.join([baseUrl, 'reset-password', token], '/')

    const resetPasswordText = 'You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          resetLink + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'

    const emailParams = {
      sendTo: user.email,
      subject: 'Reset your password for BackPacker College',
      body: resetPasswordText
    }

    return sendEmail(emailParams)
  };

  createRandomToken
    .then(setRandomToken)
    .then(sendForgotPasswordEmail)
    .then(() => { return sendResponse()(res) })
    .catch(next);
}


/**
 * POST /getPaymentMethods (temporary)
 */

function getPaymentMethods(req, res, next) {

  const stripe = req.app.locals.stripe
  const customerId = req.body.stripe.customerId
  stripe.customers.listCards(customerId).then(function(cards) {
    res.status(200).json({methods: cards})
  })
}

exports.authOptions = authOptions;
exports.authenticate = authenticate;
exports.getPaymentMethods = getPaymentMethods;
exports.postForgot = postForgot;
exports.getForgot = getForgot;

