/* jshint asi: true */
// @fileOverview ./app/actions/asset.js
// @description
// A file regarding the assets stored on AWS or whatever other cloud storage, or
// distributed cloud storage system.
const moment = require('moment');
const _  = require('lodash');

// @utils
const actionLib = require('./common');

// @move models
const Asset = require('../models/Asset');

/**
 * getMetadata
 * @param asset
 * @returns {undefined}
 */
function getMetadata(asset) {
  return new Promise(function(resolve, reject) {
    console.log(asset);
    require('../utils').extractImageData(asset.url)
      .then(function(data) {
        resolve(data);
      })
      .catch(function(err) {
        reject(err);
      })
  })
}

/**
 * @name resize
 * @param asset
 * @param params
 * @returns {undefined}
 */
function resize(asset, params) {
  return new Promise(function(resolve, reject) {
    require('../utils')
      .resizeImage(asset, params)
      .then(function(localFile) {
        return _updateAsset(asset, localFile)
          .then(function(updateAsset) {
            resolve(updateAsset);
          })
          .catch(function(err) {
            reject(err);
          });
      })
      .catch(function(err) {
        reject(err);
      });
  });
}

/**
 * @name crop
 * @param asset
 * @param params
 * @returns {undefined}
 */
function crop(asset, params) {
  return new Promise(function(resolve, reject) {
    require('../utils').cropImage(asset, params)
      .then(function(localFile) {
        return _updateAsset(asset, localFile)
          .then(function(updateAsset) {
            resolve(updateAsset)
        })
        .catch(function(err) {
          reject(err)
        })
    })
    .catch(function(err) {
      reject(err)
    })
  })
}

/**
 * @name _updateAsset
 * @description Private Functions
 * @param asset
 * @param localFile
 * @returns {undefined}
 */
function _updateAsset(asset, localFile) {
  return new Promise(function(resolve, reject) {
    asset.file = localFile.path;
    asset.uuid = localFile.uuid;
    asset.extension = localFile.extension;
    require('../utils')
      .uploadAsset(asset, true)
      .then(function(updateAsset) {
        asset.save(function(err) {
          if (err) {
            reject(err);
          }
          resolve(updateAsset);
        })
      })
      .catch(function(err) {
        console.log(err);
        reject(err);
      });
  });
}

/**
 * @description Exports
 * @returns {undefined}
 */
module.exports = {
  resize      : resize,
  crop        : crop,
  getMetadata : getMetadata
};

