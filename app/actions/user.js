const _  = require('lodash');
const moment = require('moment');

const actionLib = require('./common');

const Asset = require('../models/Asset');
const User = require('../models/User');

/**
 * @api {post} /api/v1/users/:id/action setAdmin
 * @apiName setAdmin
 * @apiGroup UserActions
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
function setUserAdmin(user, params) {
  return function(cb) {
    user.setAdmin();
    user.save(cb);
  };
}

/**
 * @api {post} /api/v1/users/:id/action setProfilePhoto
 * @apiName setProfilePhoto
 * @apiGroup UserActions
 *
 * @apiParam {String} image Asset ID for the image
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
function setProfilePhoto(user, params) {

  const imageAssetId = params.image;

  const updateQuery = {};

  return function(cb) {

    Asset
      .findById(imageAssetId)
      .exec(function(err, asset) {
        if (err) {
          console.log(err);
          return { error: { msg: 'Asset query failed.' } };
        }

        updateQuery['profile.avatar'] = asset._id;

        User
          .findByIdAndUpdate(user._id, { $set: updateQuery }, {new: true}, cb);
      });
  };
}

/**
 * @api {post} /api/v1/users/:id/action setProfileVerified
 * @apiName setProfileVerified
 * @apiGroup UserActions
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
function setUserProfileVerified(user, params) {

  const updateQuery = {
    'verification.isVerified': true
  };

  return function(cb) {
    User.findByIdAndUpdate(user._id, { $set: updateQuery }, { new: true }, cb);
  };
}

/**
 * @api {post} /api/v1/users/:id/action setProfileComplete
 * @apiName setProfileComplete
 * @apiGroup UserActions
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
function setUserProfileComplete(user, params) {

  const updateQuery = {
    'verification.isComplete': true
  };

  return function(cb) {
    User.findByIdAndUpdate(user._id, { $set: updateQuery }, { new: true }, cb);
  };
}

/**
 * @api {post} /api/v1/users/:id/action setProfileApproved
 * @apiName setProfileApproved
 * @apiGroup UserActions
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
function setUserProfileApproved(user, params) {

  const updateQuery = {
    'verification.isApproved': true
  };

  return function(cb) {
    User.findByIdAndUpdate(user._id, { $set: updateQuery }, { new: true }, cb);
  };
}

/**
 * @api {post} /api/v1/users/:id/action addPaymentMethod
 * @apiName addPaymentMethod
 * @apiGroup UserActions
 *
 * @apiParam {String} token Stripe token
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
function addUserPaymentMethod(user, params) {

  const stripe = params.stripe;

  const customerId = user.stripe.customerId;

  var token = params.token;

  if ( ! token ) {
      return { error: { msg: 'Must provide a token for adding payment method' } };
  }

  return function(cb) {
    stripe.customers.createSource(customerId, { source: params.token }, cb);
  };
}

/**
 * @api {post} /api/v1/users/:id/action addDummyPaymentMethod
 * @apiName addDummyPaymentMethod
 * @apiGroup UserActions
 *
 * @apiDescription Convenient for testing, disabled in production
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
function addDummyPaymentMethod(user, params) {

  const stripe = params.stripe;

  const customerId = user.stripe.customerId;

  return function(cb) {
    stripe.tokens.create({
      card: {
        number: '4242424242424242',
        exp_month: 12,
        exp_year: 2018,
        cvc: '123'
      }
    }).then(function(token) {
      stripe.customers.listCards(customerId)
        .then(function(cards) {
          var fingerPrints = _.map(cards.data, 'fingerprint');
          if ( _.includes(fingerPrints, token.card.fingerprint) ) {
            actionLib.noop(cb);
          } else {
            stripe
              .customers
              .createSource(customerId, {
                source: token.id
              }, cb);
          }
        });
    });
  };
}

/**
 * @api {post} /api/v1/users/:id/action getPaymentMethods
 * @apiName getPaymentMethods
 * @apiGroup UserActions
 *
 * @apiSuccess {Object[]} sources Payment methods
 * @apiError error Error object
 */
function getUserPaymentMethods(user, params) {

  const stripe = params.stripe;
  const customerId = user.stripe.customerId;

  return function(cb) {
    stripe.customers.listCards(customerId, cb);
  };
}

/**
 * @api {post} /api/v1/users/:id/action editPaymentMethod
 * @apiName editPaymentMethod
 * @apiGroup UserActions
 *
 * @apiParam {String} name Full name of cardholder
 * @apiParam {String} email Email address
 * @apiParam {String} phone Phone number of cardholder
 * @apiParam {Object} address Address for the card holder
 * @apiParam {String} address.line1 Street Address
 * @apiParam {String} address.line2 Suite/Unit Number
 * @apiParam {String} address.city City
 * @apiParam {String} address.state State/Province
 * @apiParam {String} address.postal_code Postal Code
 * @apiParam {String} address.country Country
  *
 * @apiSuccess {Object} source Payment method
 * @apiError error Error object
 */
function editUserPaymentMethod(user, params) {

  const stripe = params.stripe;
  const customerId = user.stripe.customerId;
  const sourceId = params.paymentSource;
  const updateParams = params.details;

  if ( _.isUndefined(updateParams) || _.isUndefined(sourceId) ) {
    return { error : { msg : 'Missing parameters to update payment method' } };
  }

  return function(cb) {
    stripe.customers.updateCard(customerId, sourceId, updateParams, cb);
  };
}

/**
 * @api {post} /api/v1/users/:id/action removePaymentMethod
 * @apiName removePaymentMethod
 * @apiGroup UserActions
 *
 * @apiParam {String} paymentSource ID of the payment source to remove
 *
 * @apiSuccess {Object} user User object
 * @apiError error Error object
 */
function removeUserPaymentMethod(user, params) {

  const stripe = params.stripe;
  const customerId = user.stripe.customerId;
  const sourceId = params.paymentSource;

  if ( _.isUndefined(sourceId) ) {
    return { error : { msg : 'Missing parameters to delete payment method' } };
  }

  return function (cb) {
    stripe.customers.deleteCard(customerId, sourceId, cb);
  };
}

/**
 * Exports
 */

module.exports = {
  setAdmin: setUserAdmin,
  setProfilePhoto: setProfilePhoto,
  setProfileVerified: setUserProfileVerified,
  setProfileApproved: setUserProfileApproved,
  setProfileComplete: setUserProfileComplete,
  addPaymentMethod: addUserPaymentMethod,
  addDummyPaymentMethod: addDummyPaymentMethod,
  getPaymentMethods: getUserPaymentMethods,
  editPaymentMethod: editUserPaymentMethod,
  removePaymentMethod: removeUserPaymentMethod
};
