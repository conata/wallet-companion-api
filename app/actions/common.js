const _ = require('lodash');
const moment = require('moment');

function noop(cb) {
  const err = null;
  const data = 'noop';
  return cb(err, data);
}

/**
 * Exports
 */

module.exports = {
  noop: noop
};
