const _  = require('lodash');
const moment = require('moment');

const actionLib = require('./common');

/**
 * @api {post} /api/v1/organizations/:id/action addBuyer
 * @apiName addBuyer
 * @apiGroup OrganizationActions
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
function addOrganizationBuyer(organization, params) {

  const user = params.user;

  return function(callback) {
    organization.buyers.push(user);
    organization.save(callback);
  };
}

var org = Object.create({}, {})

// @note
  // org
  //   .pipe(addOrganizationBuyer)
  //   .compose([
  //   ])
  //   .map(function(abt) {
  //     console.log(abt)
  //   })
  //   .pipe(function(() => {
  //       return function() {
  //       }
  //     })

/**
 * @api {post} /api/v1/organizations/:id/action addBuyer
 * @apiName addBuyer
 * @apiGroup OrganizationActions
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
function addOrganizationBuyer(organization, params) {

  const user = params.user;

  return function(callback) {
    organization.sellers.push(user);
    organization.save(callback);
  };
}

/**
 * @api {post} /api/v1/organizations/:id/action removeBuyer
 * @apiName RemoveBuyer
 * @apiGroup OrganizationActions
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
function removeOrganizationBuyer(organization, params) {

  const userID = params.user;

  return function(callback) {
    organization.buyers.id(userID).remove();
    organization.save(callback);
  };
}

/**
 * @api {post} /api/v1/organizations/:id/action removeBuyer
 * @apiName removeBuyer
 * @apiGroup OrganizationActions
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
function removeOrganizationBuyer(organization, params) {

  const userID = params.user;

  return function(callback) {
    organization.sellers.id(userID).remove();
    organization.save(callback);
  };
}

/**
 * @api {post} /api/v1/organizations/:id/action addOffer
 * @apiName addOffer
 * @apiGroup OrganizationActions
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
function addOrganizationOffer(organization, params) {

  const offer = params.offer;

  return function(callback) {
    organization.offers.push(offer);
    organization.save(callback);
  };
}

/**
 * @api {post} /api/v1/organizations/:id/action removeOffer
 * @apiName removeOffer
 * @apiGroup OrganizationActions
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
function removeOrganizationOffer(organization, params) {

  const offerID = params.offer;

  return function(callback) {
    organization.offers.id(offerID).remove();
    organization.save(callback);
  };
}

/**
 * Exports
 */

module.exports = {
  addBuyer    : addOrganizationBuyer,
  addBuyer    : addOrganizationBuyer,
  removeBuyer : removeOrganizationBuyer,
  removeBuyer : removeOrganizationBuyer,
  addOffer     : addOrganizationOffer,
  removeOffer  : removeOrganizationOffer
};
