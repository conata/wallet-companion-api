/**
 *
 */
const _  = require('lodash');
const moment = require('moment');

const actionLib = require('./common');

const Asset = require('../models/Asset');
const Offer = require('../models/Offer');
const Coin = require('../models/Coin');

const sendEmail = require('../lib/utils/email').sendEmail;

/**
 * @api {post} /api/v1/offers/:id/action addAsset
 * @apiName addAsset
 * @apiGroup OfferActions
 *
 * @apiParam {String} asset Key for the asset
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
function addAssetToOffer(offer, params) {

  const asset = params.asset;

  return function(cb) {
    offer.assets.push(asset);
    offer.save(cb);
  };
}

/**
 * @api {post} /api/v1/offers/:id/action addPhoto
 * @apiName addPhoto
 * @apiGroup OfferActions
 *
 * @apiParam {String[]} photos Array of asset IDs for the photos
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
function addPhotoForOffer(offer, params) {

  const images = params.photos;

  return function(cb) {
    _.forEach(images, function(imageAssetId) {
      Offer.findByIdAndUpdate(
        offer._id,
        { $push: { 'profile.photos': { image: imageAssetId, isDefault: false } } },
        { safe: true, upsert: true, new: true }, cb );
    });
  };
}

/**
 * @api {post} /api/v1/offers/:id/action setPolicy
 * @apiName setPolicy
 * @apiGroup OfferActions
 *
 * @apiParam {String} policy PolicyID
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
function setPolicyForOffer(offer, params) {

  const policyId = params.policy;

  return function(cb) {
    Offer.findByIdAndUpdate(
      offer._id,
      { $set: { 'policy': policyId } },
      { safe: true,  new: true }, cb );
  };
}

/**
 * @api {post} /api/v1/offers/:id/action setPublished
 * @apiName setPublished
 * @apiGroup OfferActions
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
function setPublished(offer, params) {

  return function(cb) {
    offer.setPublished();
    offer.save(cb);
  };
}

/**
 * @api {post} /api/v1/offers/:id/action setHidden
 * @apiName setHidden
 * @apiGroup OfferActions
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
function setHidden(offer, params) {

  return function(cb) {
    offer.setHidden();
    offer.save(cb);
  };
}

/**
 * @api {post} /api/v1/offers/:id/action requestInformation
 * @apiName requestInformation
 * @apiGroup OfferActions
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
function requestInformation(offer, params) {

  const user = params.user;
  const purchaseDates = params.dates || { from : 'N/A', to: 'N/A' };
  const quantity = params.quantity || 1;
  const tokenType = params.tokenType || 'Any';

  const notificationEmailText = 'Your request for additional information has been received. \n\n' +
        'We will respond to your inquiry within 24 hours. \n\n';

  const enquiryEmailText = 'A user has requested information about ' + offer.profile.title + '. ' +
        'Their requested purchase dates are: \n\n' +
        'Check-In: ' + purchaseDates.from + '\n' +
        'Check-Out: ' + purchaseDates.to +  '\n' +
        'Number of coins required: ' + quantity + '\n' +
        'Token type requested: ' + tokenType + '\n\n' +
        '---------------------------------------\n' +
        '         Buyer information         \n' +
        '---------------------------------------\n\n' +
        'Name: ' + user.profile.name.first + ' ' + user.profile.name.last + '\n\n' +
        'Email: ' + user.email + '\n\n' +
        'Phone: '  + user.profile.phone + '\n\n' +
        'Stripe Customer ID: ' + user.stripe.customerId + '\n\n' +
        'View Account: ' + process.env['TGM_WEBAPP_URL'] + '/users/' + user._id + '\n\n';

  const notificationEmailParams = {
    sendTo: user.email,
    subject: 'Your purchase enquiry for ' + offer.profile.title,
    body: notificationEmailText
  };

  const enquiryEmailParams = {
    sendTo: process.env['TGM_CONTRACTS_EMAIL_SENDTO'],
    subject: 'Purchase enquiry for ' + offer.profile.title,
    body: enquiryEmailText
  };

  sendEmail(notificationEmailParams);
  sendEmail(enquiryEmailParams);

  return offer;
}

/*
 * Exports
 **/

module.exports = {
  addPhoto           : addPhotoForOffer,
  addAsset           : addAssetToOffer,
  setPolicy          : setPolicyForOffer,
  setPublished       : setPublished,
  setHidden          : setHidden,
  requestInformation : requestInformation
};
