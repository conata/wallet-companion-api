const userActions = require('./user');
const assetActions = require('./asset');
const organizationActions = require('./organization');
const offerActions = require('./offer');
const contractActions = require('./contract');

function getActionTable() {
  return {
    user         : userActions,
    asset        : assetActions,
    organization : organizationActions,
    offer        : offerActions,
    contract     : contractActions
  };
}

module.exports = {
  getActionTable: getActionTable
};
