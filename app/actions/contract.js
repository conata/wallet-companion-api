const _  = require('lodash');
const moment = require('moment');

const actionLib = require('./common');

const Coin = require('../models/Coin');
const Transaction = require('../models/Transaction');

const queries = require('../lib/utils/queries');

const calculateCostForContract = queries.calculateCostForContract;
const updateContract = queries.updateContract;
const updateTransaction = queries.updateTransaction;

/**
 * @api {post} /api/v1/contracts/:id/action addCoin
 * @apiName addCoin
 * @apiGroup ContractActions
 *
 * @apiParam {Object[]} coins List of coins to add
 * @apiParam {String} coins.token ID of the token to add
 * @apiParam {Number} coins.quantity number of coins to add
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
function addCoinToContract(contract, params) {

  if ( ! contract.isStatusPending() ) {
    return { error: { msg: 'Cannot add coins to this contract' } };
  }

  return _updateContract(contract, params);
}

/**
 * @api {post} /api/v1/contracts/:id/action changeDates
 * @apiName changeDates
 * @apiGroup ContractActions
 *
 * @apiParam {Object} dates Dates for the contract
 * @apiParam {String} dates.from Beginning date string for the contract
 * @apiParam {String} dates.to Ending date string for the contract
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
function changeDatesForContract(contract, params) {

  if ( ! contract.isStatusPending() ) {
    return { error: { msg: 'Dates cannot be changed for this contract' } };
  }

  return _updateContract(contract, params);
}

/**
 * @api {post} /api/v1/contracts/:id/action getInvoice
 * @apiName getInvoice
 * @apiGroup ContractActions
 *
 * @apiSuccess {Object} transaction Transaction object
 * @apiError error Error object
 */
function getTransactionDetailsForContract(contract, params) {
  return function(cb) {
    Transaction
      .findTransactionForContract(contract._id, cb);
    };
}

/**
 * @api {post} /api/v1/contracts/:id/action cancel
 * @apiName cancel
 * @apiGroup ContractActions
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
function cancelContract(contract, params) {

  const stripe = params.stripe;

  return function(cb) {
    Transaction
      .findTransactionForContract(contract._id, (err, transaction) => {

        var stripeCharge = transaction.getStripeCharge();

        //console.log(stripeCharge);

        // Check if we need to issue refund
        if (  stripeCharge ) {
          stripe.refunds.create({ charge: stripeCharge })
            .then(function(refund) {
              //console.log(refund);
              transaction.setAmountRefunded(transaction.getAmountPaid());
              transaction.setStripeRefund(refund.id);
              transaction.setStatusRefunded();
              transaction.save();
              contract.setStatusCancelled();
              contract.save(cb);
            });
        } else {
          transaction.setAmountPending(0.00);
          transaction.setStatusCancelled();
          transaction.save();
          contract.setStatusCancelled();
          contract.save(cb);
        }
      });
  };
}

/**
 * @api {post} /api/v1/contracts/:id/action submitPayment
 * @apiName submitPayment
 * @apiGroup ContractActions
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
function submitPaymentForContract(contract, params) {

  const stripe = params.stripe;

  const stripeCustomerId = contract.buyer.stripe.customerId;

  return function(cb) {
    Transaction
      .findTransactionForContract(contract._id, (err, transaction) => {

        stripe.customers.retrieve(stripeCustomerId)
          .then(function(customer) {
            var paymentSource = customer['default_source'];
            //console.log(paymentSource);
            transaction.setPaymentSource(paymentSource);

            transaction.setAmountPending(transaction.getAmountDue());

            transaction.setStatusProcessing();

            transaction.save();

            contract.setStatusApproved();

            contract.save(cb);
          });
      });
  };
}

/**
 * @api {post} /api/v1/contracts/:id/action completePayment
 * @apiName completePayment
 * @apiGroup ContractActions
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
function completePaymentForContract(contract, params) {

  const stripe = params.stripe;

  const stripeCustomerId = contract.buyer.stripe.customerId;

  return function(cb) {
    Transaction
      .findTransactionForContract(contract._id, (err, transaction) => {
        // Process the stripe transaction

        stripe.charges.create({
          amount: transaction.getAmountPending() * 100,
          currency: 'cad',
          customer: stripeCustomerId,
          source: transaction.getPaymentSource()
        })
          .then(function(charge) {
            //console.log(charge);

            transaction.setStripeCharge(charge.id);

            transaction.setAmountPaid(transaction.getAmountPending());

            transaction.setAmountPending(0.00);

            transaction.setStatusCompleted();

            transaction.save();

            contract.setStatusActive();

            contract.save(cb);
          });
      });
  };
}

/**
 * @api {post} /api/v1/contracts/:id/action confirmCheckout
 * @apiName confirmCheckout
 * @apiGroup ContractActions
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
function confirmCheckoutForContract(contract, params) {

  // Make sure today's date is after the end of the contract date
  return function(cb) {
    contract.setStatusComplete();
    contract.save(cb);
  };
}

/**
 * Private Functions
 */

function _updateContract(contract, updateParams) {
  return calculateCostForContract(updateContract(contract, updateParams), updateTransaction);
}

/**
 * Exports
 */

module.exports = {
  cancel          : cancelContract,
  addCoin         : addCoinToContract,
  changeDates     : changeDatesForContract,
  getInvoice      : getTransactionDetailsForContract,
  submitPayment   : submitPaymentForContract,
  completePayment : completePaymentForContract,
  confirmCheckout : confirmCheckoutForContract
};

