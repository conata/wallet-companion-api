/**
 * @fileOverview ./app/utils.js
 * @description
 * A collection of utilities for custodial microservice work.
 */
const utils = require ('./lib/utils');

module.exports = {
  throwError                    : utils.handlers.throwError,
  sendResponse                  : utils.handlers.sendResponse,
  sendEmail                     : utils.email.sendEmail,
  extractImageData              : utils.storage.extractImageData,
  resizeImage                   : utils.storage.resizeImage,
  cropImage                     : utils.storage.cropImage,
  uploadAsset                   : utils.storage.uploadAsset,
  extractFilterParams           : utils.filters.extractFilterParams,
  filterCoins                   : utils.filters.filterCoins,
  filterOffers                  : utils.filters.filterOffers,
  filterContracts               : utils.filters.filterContracts,
  getConstant                   : utils.constants.getConstant,
  getCountryCode                : utils.constants.getCountryCode,
  getCountryName                : utils.constants.getCountryName,
  createAdminPermissionCheck    : utils.permissions.createAdminPermissionCheck,
  modifyUserPermissionCheck     : utils.permissions.modifyUserPermissionCheck,
  deleteUserPermissionCheck     : utils.permissions.deleteUserPermissionCheck,
  modifyAssetPermissionCheck    : utils.permissions.modifyAssetPermissionCheck,
  deleteAssetPermissionCheck    : utils.permissions.deleteAssetPermissionCheck,
  createPolicyPermissionCheck   : utils.permissions.createPolicyPermissionCheck,
  modifyPolicyPermissionCheck   : utils.permissions.modifyPolicyPermissionCheck,
  deletePolicyPermissionCheck   : utils.permissions.deletePolicyPermissionCheck,
  createOfferPermissionCheck    : utils.permissions.createOfferPermissionCheck,
  modifyOfferPermissionCheck    : utils.permissions.modifyOfferPermissionCheck,
  deleteOfferPermissionCheck    : utils.permissions.deleteOfferPermissionCheck,
  createContractPermissionCheck : utils.permissions.createContractPermissionCheck,
  viewContractPermissionCheck   : utils.permissions.viewContractPermissionCheck,
  modifyContractPermissionCheck : utils.permissions.modifyContractPermissionCheck,
  deleteContractPermissionCheck : utils.permissions.deleteContractPermissionCheck,
  getActionHandler              : utils.handlers.getActionHandler,
  dispatchAction                : utils.handlers.dispatchAction,
  lookupOfferForContract        : utils.queries.lookupOfferForContract,
  lookupContractsForUser        : utils.queries.lookupContractsForUser,
  calculateCostForContract      : utils.queries.calculateCostForContract,
  updateContract                : utils.queries.updateContract,
  createTransaction             : utils.queries.createTransaction,
  updateTransaction             : utils.queries.updateTransaction
};

