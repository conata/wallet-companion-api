const actionHandler = require('./actions/index');

/**
 * Exports
 */

const ActionTable = actionHandler.getActionTable();

module.exports = ActionTable;
