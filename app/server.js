/**
 * @fileOverview ./app/server.js
 * @name tgm.server.app
 * @description
 * A Web Application Server implemented in Node.js with ExpressJS framework
 * reference implementation materials to interplay referents within API
 * compositions for mobile reference clients, hypermedia clients,
 * administrative policy control panels, distributed hardware, home automation
 * systems, distributed ledger technologies, dispute resolution centers, data
 * API services (WebAPI), RSS, microformats, PBX interfaces, mesh networking,
 * flatness/fault-tolerance command topologies, radioverses (layered frequency
 * channel representation signals, LFCRS; e.g., HTTP message simulation, error
 * code checks of torusoidal-nomological constants over speaker wave form at
 * detection of smooth amplitudes given polytonal (dualzed polynomial)
 * equalization), microcontrollers, CoREs, etc.
 */
const express = require('express')
const compression = require('compression')
const session = require('express-session')
const bodyParser = require('body-parser')
const errorHandler = require('errorhandler')
const lusca = require('lusca')
const dotenv = require('dotenv')
const MongoStore = require('connect-mongo')(session)
const flash = require('express-flash')
const path = require('path')
const chalk = require('chalk')
const logger = require('morgan')
const mongoose = require('mongoose')
const passport = require('passport')
const stripe = require('stripe')
const expressValidator = require('express-validator')
const expressStatusMonitor = require('express-status-monitor')
const fileUpload = require('express-fileupload')
const fs = require('fs')
const https = require('https')
const _logger = require('./lib/utils/logger');
const winston = require('winston');
const Seneca = require('seneca');
const paxos = require('./lib/utils/paxos');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */

if ( process.env.NODE_ENV === 'test' ) {
  console.log('Loading test environment')
  dotenv.load({ path: './app/.env.test' })
}
else if ( process.env.NODE_ENV === 'production' ) {
  console.log('Loading production environment')
  dotenv.load({ path: '.env.production' })
} else {
  console.log('Loading development environment')
  dotenv.load({ path: '.env.develop' })
}

if (process.env.NODE_ENV !== 'production') {

  // _logger.logger.add(new winston.transports.Console({
  //   format: winston.format.simple()
  // }));

}

const quilt = require('./lib/utils/quilt')

/**
 * Controllers (route handlers).
 */

/**
 * @description
 * Application administration.
 */
const adminController = require('./interfaces/admin')

/**
 * @description
 * Application interface and configuration defaults.
 * @usage
 * interfaceController::{operation://event}
 * @see https://schema.org
 */
const appController = require('./interfaces/app')

/**
 * @name app.ecosystem
 * @description
 * Missives and Messages Microservice Model Representations (MOVER; "timeless movers")
 *   =def (model operation-view-event-representations);
 *
 * Relational Schema (non-decisional events)
 *   [x] Simple Labor Values (fig. 1) (Value Layer)
 *   [ ] Prices of Production (capitalistic)
 *   [ ] Equalization of the Rate of Profit across all Capitals (planned; "bourgeois "prices")
 *
 *   fig. 1:
 *
 *     (1 + r)(p^m A + ma) = p^m
 *
 *   A is the matrix of produced means of production                  (Means of Production; Productivization) :: model (parameters of being; meaning of the end)         addresses (digital entities)
 *   a is row-vector showing the level of employment in each industry (Employment)                            :: messages (scope of being; meaning of the time)          relations (digital time; statuses)
 *   r is a uniform rate of profit                                    (Continuous Capital, Dianomic Capital,     [ontological commitment]
 *                                      Generic Capital, Differential Capital) :: microservice (epithets of being; meaning of the trace)  actions   (virtual activity; transactions, trx volume)
 *   p^m is a row-vector of money prices                              (Price; Rent; Tax; Profit)              :: missives; and (content of being; meaning of the given)  decisions (virtual time; statuses)
 *   m is the money wage rate                                         (Wage)                                  :: tendential fall in the rate of digitality               events    (transaction artifacts created per dollar per month^2 for each address; anomalous time)
 *                                                          (impossibility of intent)
 *
 * Perks of "planned prices"; i.e., non-capitalistic pricing systems:
 *
 * 1. Serious errors will be caught by the system of material balances.
 * 2. In an economy with a stable population the errors are small and can be
 *    removed by using historic labor costs.
 * 3. These “errors” are with respect to a standard that itself produces some
 *    anomalous results in evaluating long-term proposals, and they are likely
 *    to be ecologically benign.
 * 4. The errors are far less than those induced by the exploitation of cheap
 *    labor in a capitalist pricing system.
 * "Economic planning, computers and labor values".  W. Paul Cockshott and Allin Cottrell∗ January, 1999.
 */

    /**
     * @name actor.schemas
     * @description
     * Public Key Segmented-Infrastructure Representation Language with WoT Open Keys + REST Confidence
     * @see https://github.com/palmerabollo/rest-confidence-client#usage
     * @see https://github.com/openpgpjs/openpgpjs/#encrypt-with-compression
     * @see https://docs.openchain.org/en/latest/api/ledger.html#acc-record
     * @see http://xmlns.com/wot/0.1/
     * @see https://www.vaultproject.io/docs/secrets/pki/index.html (for distributed secrets-manager)
     * @see https://www.w3.org/TR/activitypub/#actor-objects
     * We pass around
     * convergent, anomalous, clinamen, etc. keys for asynchronous, degradable,
     * and modular (AMD) HATEOAS over Hybrid PWA-ROCA MOVER with Discrete Task
     * Information Observer Models (DTIOM ODRL; open digital rights language)
     * inhering {entity-authority-actor-object} Relational Key Groups (EAAORKG)
     * (blockchain-agnostic permissionless autonomous Markov Chain Model) w/r/t
     * Object-Oriented or Functional layered STOMPing FSMs with Reconfigurable
     * ACID-complient REST-ful STEMS).
     * @see http://duncan-cragg.org/blog/post/distributed-observer-pattern-rest-dialogues/
     * @see https://blogs.vmware.com/vfabric/2013/02/choosing-your-messaging-protocol-amqp-mqtt-or-stomp.html
     * @see https://github.com/benjaminws/stomp-js/blob/master/examples/stomp-producer-txn.js
     * @see Stoppable Paxos (not MultiPaxos; also a multi-party "cluster"/compute); L. Lamport et al.
     */

    // @TODO change controllers->actors since "actor" is natural kind of controller
    // @description
    // Distributed Entities Markov Model
    // @see http://raganwald.com/2018/02/23/forde.html
    const userController = require('./entities/user')

    // Distributed Authorities Markov Model
    // @description Risk modeling on INVEST model using Generalized Pareto Distributions (GPD)
    // @TODO change controllers->authorities since "authority" is a natural kind of controller
    // @see https://quant.stackexchange.com/questions/7673/value-at-risk-monte-carlo-using-generalized-pareto-distributiongpd
    const organizationController = require('./authorities/organization')

    /**
     * @name object.schemas
     * @description
     * Distributed Contracts, Artifacts, Classes, and Objects with Monte Carlo
     * Pareto-based, etc. statistical mechanics
     * Public Domain Record Ontology under PKI
     * @see https://docs.openchain.org/en/latest/api/ledger.html#data-record
     * @see https://www.w3.org/TR/2017/WD-wot-architecture-20170914/
     * @see https://www.vaultproject.io/docs/secrets/pki/index.html (for distributed secrets-manager)
     * @see https://w3c.github.io/webrtc-pc/archives/20151123/webrtc.html#bib-ICE
     * @see https://www.w3.org/TR/2018/REC-odrl-model-20180215/#x2-odrl-information-model
     * @see https://www.w3.org/TR/activitystreams-core/#model
     * @see https://w3c-ccg.github.io/did-spec/
     */

    // @TODO change controllers->entities since "assets" and "affiliates" are a
    // natural kind of

    // "extrinsic" (differential; alethic possibility) controllers
    //   (see external-body messages; a posteriori necessity; communicative rings
    //   as local finite galois fields) and
    //const affiliateController = require('./objects/affiliate')

    // "intrinsic" (dialectical; dianomic; aneuploidicity) controllers
    //   assets as "soft contracts", soft objects; "knot data" that is possibly
    //   non-communicative (watch out when combined with noetherian rings on L-
    //   and R-endianness
    //   (a priori necessity)
    const assetController = require('./contracts/asset')

    // @TODO change controllers->objects since "objects", hard objects, are
    // natural kinds of

    // "continuous" (anisotropic necessity) controllers
    //   which discern some areal artifact: "artinian" ring-data, which can be
    //   communicative
    //   (contingent a posteriori)
    const offerController = require('./artifacts/offer')

    // "generic" (non-autopositionality) controllers
    //   which classify some areal artifact (areal artifacts are metatransactional
    //   (metanotational) transaction artifacts that owns
    //   Actors and Authorities as data, pre-in-formationally; they own data
    //   that provably does not exist yet; "classic data" or noetherian rings
    //   non-communicative status determines necessity of endianness necessarily
    //   (contingent a priori)
    //   e.g. "intrinsic generic reservations" would be non-communicative
    //   non-descript noetherian rings
    const contractController = require('./classes/contract')

/**
 * @name discoverable.media.modules
 * @description
 * Modular Media Models over microservice platforms using CQRS. Modular Tokens.
 * @see http://microservices.io/patterns/data/cqrs.html
 * @see https://github.com/ethereum/EIPs#finalized-eips-standards-that-have-been-adopted
 *
 * distribution <> locales
 * discussion <> packages
 * description <> generics
 * debate <> components
 */

  /**
   * @name continuous.platforming
   * @description
   * Transaction Log Tailing with Log-Structured Merge Trees as packed
   * rb-trees. For distributed event locales (stores).
   * @see https://github.com/palmerabollo/rest-confidence-client#usage (again)
   * @see https://blog.eventuate.io/2016/10/06/eventuate-local-event-sourcing-and-cqrs-with-spring-boot-apache-kafka-and-mysql/
   * @see http://microservices.io/patterns/data/database-triggers.html
   * @features {policy,title,domain)
   */
  const localeMiddleware = require('./modules/locale')

  /**
   * @name generic.platforming
   * @description
   * For bulletproof distributed-generator managers.
   * Database Triggers and range proof events (distributed proof generators).
   * @see Bulletproofs: Short Proofs for Confidential Transactions and More.
   *   https://eprint.iacr.org/2017/1066.pdf
   * @see http://microservices.io/patterns/data/transaction-log-tailing.html
   * @features {satellite,domain,policy)
   */
  const genericMiddleware = require('./modules/generic')

  /**
   * @name differential.platforming
   * @description
   * Event Sourcing.
   * For distributed hypermedia packages (packaged pools).
   * @see https://github.com/coopernurse/node-pool
   * @see http://eventuate.io/exampleapps.html
   * @see http://microservices.io/patterns/data/event-sourcing.html
   * @features {title,policy,satellite)
   */
  const packageMiddleware = require('./modules/package')

  /**
   * @name dialectical.platforming
   * @description
   * Hybrid PWA Component with HTTP 302 EventTargets using Application
   * Events. For distributed web components. e.g. P2P Pears and Idioms
   * @see http://microservices.io/patterns/data/application-events.html
   * @see https://html.spec.whatwg.org/multipage/comms.html#server-sent-events
   * @see https://dzone.com/articles/html5-server-sent-events
   * @see https://github.com/trquoccuong/Server-Sent-Events-Nodejs
   * @see http://dsheiko.github.io/h5i/
   * @see http://archive.is/wHW6f
   * @features {domain,satellite,title)
   */
  const componentMiddleware = require('./modules/component')

/**
 * @name decomposable.media.factors
 * @description
 * Degradable Media Models for Degradable Contacts (full name: degradable
 * organic contracts; as social ecology ) as opposed to "social smart contracts"
 * (eco-socialism; https://github.com/DemocracyEarth/paper#32-proof-of-identity).
 * Periodic table of information provides for "proof of periodicity" based on
 * Free Abelian Signatures with (Inter)Temporal Proof-theoretic Representations
 * (FASTR Proofs; "epipathic proofs"; "elastic convergency zone proofs", etc.);
 *
 * DIRAC BG (background)
 * [Proof of Distributivity]         (distributed domains; clinamen theorems/signatures; distribution)
 * [Proof of Involution]                 (distributed discussion; antinomy threoms/signatures; discursivity)
 * [Proof of Role]                         (distributed observer/différance; dualyzation correlations of "inconsistent
 *                        multiples" over the the count-as-one; anisotropic theorems/signatures
 *                           http://duncan-cragg.org/blog/post/distributed-observer-pattern-rest-dialogues/)
 * [Proof of Absolutivity]                 (distributed description; EDW theorems; description)
 * [Proof of Collapse]                (distributed deconstruction/annihilator; 'pataphysical groups; "gossip already syzygies"; metaphor without metaphor)
 * [Proof of Bags (of Words)]              (distributed anomalies; word vector content analysis)
 * [Proof of Gossip]                  (distributed debate; or "proof of syzygies")
 * 1. DIRAC Groups: PoD, PoI, PoG, PoEDW, PoC, PoBoW, PoR
 * 2. PoD: Exposes SDRT (segmented discourse representation theorems) to
 *    discover Systems of communicative praxis (isomorphic to noncommunicative rings).
 *
 * deconstruction <> decomposition/discovery <> factor decomposition <> content dispositional analysis
 */

  /**
   * @name domain.groups
   * @description
   * Distributed domain<>document-managers basing DualApi domains for dynamic,
   * configurable epiprotocological discoverable routing using common consensus
   * protocol semantics.
   * @see https://github.com/plediii/dual-navigator/blob/master/example/routes.js
   * @see http://microservices.io/patterns/decomposition/decompose-by-subdomain.html
   * @composes control
   */
  const domainController = require('./composers/domain')

  /**
   * @name satellite.cuids
   * @description
   * Distributed Satellite-managers over sempaphore with collision-resistent ids
   * optimized for horizontal scaling.
   * @see "Markov Chain Monte Carlo Method and Its Application".
   * @see "Applegate, D., Kannan, R. and Polson, N. G. (1990) Random polynomial
   * time algorithms for sampling from joint distributions."
   * Technical Report 500. Carnegie Mellon University, Pittsburgh.
   * @see https://azuqua.github.io/clusterluck/Clusterluck.DSMServer.html
   * @composes narration
   */
  const satelliteController = require('./composers/.satelliteExample')

  /**
   * @name title.documents
   * @description
   * Distributed TitleOrRepresentations-manager bitemporal valuations basing distributed credit
   * vector-managers (reward/penalty paragraph-vector with softmax document
   * parameters using test representation linear regression network-content
   * analysis). Computing coefficients for primal words in paragraph vectors of
   * large-document stores (event locales; see above). Narratological
   * constructs (GRIOTs) for the Link Layer of PTI. (Story)Bookproofs generators with
   * bloomfilter, MinHash, DivSelect+CNNLM distributed compute of sentence
   * selection and sentence similarity. Also see: fwp. free keys map
   * automorphism for transitivity and regularity.
   * @see https://en.wikipedia.org/wiki/Graph_automorphism#Graph_families_defined_by_their_automorphisms
   * @see http://ctheory.net/ctheory_wp/algebra-of-identity/
   * @see https://studylib.net/doc/13904759/optimizing-sentence-modeling-and-selection-for-document-s...
   * @see http://meta-guide.com/dialog-systems/qa-systems/skipgrams-deep-learning-question-answering-2017
   * @see http://movieqa.cs.toronto.edu/home/
   * @see https://github.com/Planeshifter/node-word2vec
   * @see https://github.com/NaturalNode/natural#pos-tagger
   * @see https://github.com/Maratyszcza/NNPACK
   * @see http://stars.library.ucf.edu/cgi/viewcontent.cgi?article=1391&context=rtd
   * @see https://gist.github.com/Piyush3dB/8ebd08a7b7f962f2cfe4e0c6a83d9965
   * @see https://github.com/StevenLOL/aicyber_semeval_2016_ivector
   * @see Line of Self (anomalous similarity to line of credit) from
   *   "Engaging the Anomalous Self of Individuals with Autism through Digital
   *   Doppelgangers: Transcendence from Self to i-Self"
   *   http://www.ijrdet.com/files/Volume4Issue7/IJRDET_0715_01.pdf
   * @see http://docs.oasis-open.org/xdi/xdi-core/v1.0/csd01/xdi-core-v1.0-csd01.xml
   * @composes value
   */
  const titleController = require('./composers/.titleExample')

  /**
   * @name policy.rings
   * @description
   * Distributed lock-manager for consistent hash rings with coefficients over a
   * ring of polynomials basing quantum metropolis lines of credit over ideal
   * rings given Markov Chain States isomorphic to domains without Efficient
   * Market Hypothesis.
   * @see "Notes on Abstract Algebra". 7 Quotient Rings and Ideals. Charles Boyd.
   * @see https://azuqua.github.io/clusterluck/Clusterluck.DLMServer.html
   * @see http://docs.oasis-open.org/xdi/xdi-core/v1.0/csd01/xdi-core-v1.0-csd01.xml#xdi-link-contract-1.0
   * @alias ./controllers/polis
   * @composes attribution
   */
  const policyController = require('./composers/policy')

/**
 * API keys and Passport configuration.
 */
const passportConfig = require('./config/passport')

/**
 * Create Express server.
 */
const app = express()

app.locals.stripe = stripe(process.env.STRIPE_SECRET)
app.locals.environment = process.env['NODE_ENV']

/**
 * Connect to MongoDB.
 */
mongoose.Promise = global.Promise
mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI)
mongoose.connection.on('error', (err) => {
  console.error(err)
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'))
  process.exit()
})

/**
 * Express configuration.
 */
// app.set('_logger', _logger);
app.set('port', process.env.PORT || 3000)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.use(expressStatusMonitor())
app.use(fileUpload())
app.use(logger('dev'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(compression())
app.use(expressValidator())
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: {
    httpOnly: false
  },
  store: new MongoStore({
    url: process.env.MONGODB_URI || process.env.MONGOLAB_URI,
    autoReconnect: true,
    clear_interval: 3600
  })
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

app.use(lusca.xframe('SAMEORIGIN'))
app.use(lusca.xssProtection(true))
app.disable('x-powered-by')

app.use((req, res, next) => {

  res.header('Access-Control-Allow-Origin', req.headers.origin)
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept')

  res.locals.user = req.user
  next()
})

app.use('/docs', express.static(path.join(__dirname, 'public/docs')))
app.use('/reports/complexity', express.static(path.join(__dirname, 'public/reports/complexity')))
app.use('/reports/client-complexity', express.static(path.join(__dirname, 'public/reports/client-complexity')))

app.get('/stripe/key', appController.getStripeKey)

//app.get('/admin/setup', adminController.setup)

app.get('/account', passportConfig.isAuthenticated, userController.getAccount)
app.delete('/account', passportConfig.isAuthenticated, userController.deleteAccount)
app.post('/account/password', passportConfig.isAuthenticated, userController.updatePassword)

app.get('/passphrase', userController.getPassphrase)
app.get('/forgot', userController.getForgot)
app.post('/forgot', userController.postForgot)
app.get('/reset/:token', userController.getReset)
app.post('/reset/:token', userController.postReset)

app.get('/verify/:token', userController.getVerification)
app.post('/verify/:token', userController.postVerification)

app.get('/app/config/stripe', appController.getCountryCodes)

app.get('/app/constants/countries', appController.getCountryCodes)
app.get('/app/constants/countries/:countryCode', appController.getCountryInfo)
app.get('/app/constants/countries/:countryCode/states', appController.getStatesForCountry)
app.get('/app/constants/countries/:countryCode/provinces', appController.getStatesForCountry)

app.options('/api/v1/auth', userController.authOptions)

app.post('/api/v1/auth', userController.authenticate)
//app.post('/api/v1/auth', appController.maintenanceMode)


app.post('/app/support', passportConfig.isAuthenticated, appController.createSupportRequest)


app.get('/api/v1/users', passportConfig.isAuthenticated, userController.listUsers)
app.post('/api/v1/users', userController.createUser)
//app.post('/api/v1/users', appController.maintenanceMode)
app.get('/api/v1/users/:id', passportConfig.isAuthenticated, userController.getUser)
app.put('/api/v1/users/:id', passportConfig.isAuthenticated, userController.updateUser)
app.post('/api/v1/users/:id/action', passportConfig.isAuthenticated, userController.dispatchUserAction)
app.delete('/api/v1/users/:id', passportConfig.isAuthenticated, userController.deleteUser)
app.post('/api/v1/users/:id/payment-methods', passportConfig.isAuthenticated, userController.getPaymentMethods)

app.get('/api/v1/organizations', passportConfig.isAuthenticated, organizationController.listOrganizations)
app.post('/api/v1/organizations', passportConfig.isAuthenticated, organizationController.createOrganization)
app.get('/api/v1/organizations/:id', passportConfig.isAuthenticated, organizationController.getOrganization)
app.put('/api/v1/organizations/:id', passportConfig.isAuthenticated, organizationController.updateOrganization)
app.post('/api/v1/organizations/:id/action', passportConfig.isAuthenticated, organizationController.dispatchOrganizationAction)
app.delete('/api/v1/organizations/:id', passportConfig.isAuthenticated, organizationController.deleteOrganization)


app.get('/api/v1/domains', passportConfig.isAuthenticated, domainController.list)
app.post('/api/v1/domains', passportConfig.isAuthenticated, domainController.create)
app.get('/api/v1/domains/:id', passportConfig.isAuthenticated, domainController.get)
app.put('/api/v1/domains/:id', passportConfig.isAuthenticated, domainController.update)
app.post('/api/v1/domains/:id/action', passportConfig.isAuthenticated, domainController.action)
app.delete('/api/v1/domains/:id', passportConfig.isAuthenticated, domainController.delete)

app.get('/api/v1/titles', passportConfig.isAuthenticated, titleController.list)
app.post('/api/v1/titles', passportConfig.isAuthenticated, titleController.create)
app.get('/api/v1/titles/:id', passportConfig.isAuthenticated, titleController.get)
app.put('/api/v1/titles/:id', passportConfig.isAuthenticated, titleController.update)
app.post('/api/v1/titles/:id/action', passportConfig.isAuthenticated, titleController.action)
app.delete('/api/v1/titles/:id', passportConfig.isAuthenticated, titleController.delete)

app.get('/api/v1/satellites', passportConfig.isAuthenticated, satelliteController.list)
app.post('/api/v1/satellites', passportConfig.isAuthenticated, satelliteController.create)
app.get('/api/v1/satellites/:id', passportConfig.isAuthenticated, satelliteController.get)
app.put('/api/v1/satellites/:id', passportConfig.isAuthenticated, satelliteController.update)
app.post('/api/v1/satellites/:id/action', passportConfig.isAuthenticated, satelliteController.action)
app.delete('/api/v1/satellites/:id', passportConfig.isAuthenticated, satelliteController.delete)

app.get('/api/v1/policies', passportConfig.isAuthenticated, policyController.listPolicies)
app.post('/api/v1/policies', passportConfig.isAuthenticated, policyController.createPolicy)
app.get('/api/v1/policies/:id', passportConfig.isAuthenticated, policyController.getPolicy)
app.put('/api/v1/policies/:id', passportConfig.isAuthenticated, policyController.updatePolicy)
app.delete('/api/v1/policies/:id', passportConfig.isAuthenticated, policyController.deletePolicy)


/**
 * DISTRIBUTED OBJECTS
 */
app.get('/api/v1/assets', passportConfig.isAuthenticated, assetController.listAssets)
app.post('/api/v1/assets', passportConfig.isAuthenticated, assetController.createAsset)
app.get('/api/v1/assets/:id', passportConfig.isAuthenticated, assetController.getAsset)
app.post('/api/v1/assets/:id/action', passportConfig.isAuthenticated, assetController.dispatchAssetAction)
app.delete('/api/v1/assets/:id', passportConfig.isAuthenticated, assetController.deleteAsset)

// app.get('/api/v1/hotels', affiliateController.searchHotels)


/**
 * DISTRIBUTED OBJECTS
 */
app.get('/api/v1/offers', offerController.listOffers) // NOTE: unauthenticated endpoint
app.post('/api/v1/offers', passportConfig.isAuthenticated, offerController.createOffer)
app.post('/api/v1/offers/search', offerController.searchOffers) // NOTE: unauthenticated endpoint
app.get('/api/v1/offers/:id', offerController.getOffer) // NOTE: unauthenticated endpoint
app.put('/api/v1/offers/:id', passportConfig.isAuthenticated, offerController.updateOffer)
app.post('/api/v1/offers/:id/action', passportConfig.isAuthenticated, offerController.dispatchOfferAction)
app.delete('/api/v1/offers/:id', passportConfig.isAuthenticated, offerController.deleteOffer)

app.get('/api/v1/offers/:id/coins', passportConfig.isAuthenticated, offerController.listCoinsForOffer)
app.post('/api/v1/offers/:id/coins', passportConfig.isAuthenticated, offerController.createCoinForOffer)
app.get('/api/v1/offers/:offerID/coins/:coinID', passportConfig.isAuthenticated, offerController.getCoinForOffer)
app.put('/api/v1/offers/:offerID/coins/:coinID', passportConfig.isAuthenticated, offerController.updateCoinForOffer)
app.delete('/api/v1/offers/:id/coins/:coinID', passportConfig.isAuthenticated, offerController.deleteCoinForOffer)

app.get('/api/v1/contracts', passportConfig.isAuthenticated, contractController.listContracts)
app.post('/api/v1/contracts', passportConfig.isAuthenticated, contractController.createContract)
app.post('/api/v1/contracts/search', passportConfig.isAuthenticated, contractController.searchContracts)
app.get('/api/v1/contracts/:id', passportConfig.isAuthenticated, contractController.getContract)
app.put('/api/v1/contracts/:id', passportConfig.isAuthenticated, contractController.updateContract)
app.post('/api/v1/contracts/:id/action', passportConfig.isAuthenticated, contractController.dispatchContractAction)
app.delete('/api/v1/contracts/:id', passportConfig.isAuthenticated, contractController.deleteContract)

app.get('/api/v1/tickets', passportConfig.isAuthenticated, quilt.listTickets)
app.post('/api/v1/tickets', passportConfig.isAuthenticated, quilt.createTicket)
app.post('/api/v1/tickets/search', passportConfig.isAuthenticated, quilt.findTicket)
app.get('/api/v1/tickets/:id', passportConfig.isAuthenticated, quilt.findStreamTicket)

app.get('/api/v1/replica', passportConfig.isAuthenticated, paxos.__replica__);
app.get('/api/v1/leader', passportConfig.isAuthenticated, paxos.__leader__);
app.get('/api/v1/acceptor', passportConfig.isAuthenticated, paxos.__acceptor__);
app.get('/api/v1/approver', passportConfig.isAuthenticated, paxos.__approver__);
app.get('/api/v1/rejector', passportConfig.isAuthenticated, paxos.__rejector__);

/**
 * Error Handler.
 */
app.use(errorHandler())

/**
 * Start Express server.
 */

//if ( process.env['NODE_ENV'] === 'production' && process.env['ENABLE_SSL'] === true ) {
if ( process.env['NODE_ENV'] !== 'production' ) {

  // Entities are likely to require a Paxos mechanism for voting on dynamic
  // data. Somewheres else abouts too...
  Seneca()
    .use(paxos.acceptor)
    .listen({ type: 'http', port: '8260', pin:  'cmd:*' });

  Seneca()
    .use(paxos.approver)
    .listen({ type: 'http', port: '8261', pin:  'cmd:*' });

  Seneca()
    .use(paxos.rejector)
    .listen({ type: 'http', port: '8262', pin:  'cmd:*' });

  Seneca()
    .use(paxos.leader)
    .listen({ type: 'http', port: '8263', pin:  'cmd:*' });

  Seneca()
    .use(paxos.replica)
    .listen({ type: 'http', port: '8264', pin:  'cmd:*' });

  /**
   * SSL Keys.
   */
  var pkey = fs.readFileSync(path.resolve(__dirname, '..', 'keys', 'server.key'), 'utf8')
  var cert = fs.readFileSync(path.resolve(__dirname, '..', 'keys', 'server.crt'), 'utf8')

  quilt.call().then(function(client) {
    app.set('db', client);
    https.createServer({
      key  : pkey,
      cert : cert
    }, app).listen(app.get('port'), () => {
      console.log('%s App is running at https://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
      console.log('  Press CTRL-C to stop\n');
    });
  });
} else {

  quilt.call().then(function(client) {
    app.set('db', client);
    app.listen(app.get('port'), () => {
      console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env')) ;
      console.log('  Press CTRL-C to stop\n');
    });
  });
}

module.exports = app
