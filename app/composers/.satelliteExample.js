
function getSatellite(req, res, next) {
  return res.status(200).end()
}

function createSatellite(req, res, next) {
  return res.status(200).end()
}

function dispatchAction(req, res, next) {
  return res.status(200).end()
}

function deleteSatellite(req, res, next) {
  return res.status(200).end()
}

function listSatellites(req, res, next) {
  return res.status(200).end()
}

function updateSatellite(req, res, next) {
  return res.status(200).end()
}

module.exports = {
  action : dispatchAction,
  create : createSatellite,
  delete : deleteSatellite,
  get    : getSatellite,
  list   : listSatellites,
  update : updateSatellite
}
