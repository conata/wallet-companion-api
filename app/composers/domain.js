
/**
 * @api {post} /api/v1/auth Authenticate User
 * @apiName auth
 * @apiGroup Authentication
 *
 * @apiParam {String} email Email for user
 * @apiParam {String} password Password
 *
 * @apiUse UserObjectResponse
 * @apiError error Error object
 */
function getDomain() {
  return res.status(200).end()
}

/**
 * @api {post} /api/v1/auth Authenticate User
 * @apiName auth
 * @apiGroup Authentication
 *
 * @apiParam {String} email Email for user
 * @apiParam {String} password Password
 *
 * @apiUse UserObjectResponse
 * @apiError error Error object
 */
function createDomain() {
  return res.status(200).end()
}

/**
 * @api {post} /api/v1/domains/:id/action Dispatch Action
 * @apiName dispatchUserAction
 * @apiGroup Domains
 *
 * @apiParam {String} id Domain id
 * @apiParam {Object} action Domain Action
 * @apiParam {String} action.key Action Key
 * @apiParam {Object} action.params Action Parameters
 *
 * @apiDescription Documentation for specific actions will
 * assume it is wrapped in an action object with the
 * appropriate key, only values for action.params will be
 * documented for a given action.
 *
 * @apiSuccess {Object} domain Domain object
 * @apiError error Error object
 */
function dispatchAction() {
  return res.status(200).end()
}

/**
 * @api {delete} /api/v1/domains Delete a Distributed Domain
 * @apiName deleteDomain
 * @apiGroup Domains
 * @apiError error Error object
 */
function deleteDomain() {
  return res.status(200).end()
}

/**
 * @api {get} /api/v1/domains List View to Distributed Domain
 * @apiName listDomains
 * @apiGroup Domains
 * @apiError error Error object
 */
function listDomains() {
  return res.status(200).end()
}

/**
 * @api {put} /api/v1/domains Update a Distributed Domain
 * @apiName updateDomain
 * @apiGroup Domains
 * @apiError error Error object
 */
function updateDomain() {
  return res.status(200).end()
}

/**
 * EXPORT
 */
module.exports = {
  action : dispatchAction,
  create : createDomain,
  delete : deleteDomain,
  get    : getDomain,
  list   : listDomains,
  update : updateDomain
}

