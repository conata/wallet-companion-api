const _ = require('lodash');

const Policy = require('../models/Policy');

const utils = require('../lib/utils');

const throwError = utils.handlers.throwError;
const sendResponse = utils.handlers.sendResponse;
const getActionHandler = utils.handlers.getActionHandler;
const dispatchAction = utils.handlers.dispatchAction;

const createPolicyPermissionCheck = utils.permissions.createPolicyPermissionCheck;
const modifyPolicyPermissionCheck = utils.permissions.modifyPolicyPermissionCheck;
const deletePolicyPermissionCheck = utils.permissions.deletePolicyPermissionCheck;

exports.listPolicies = (req, res, next) => {

  const buyer = res.locals.user;

  var query = {};

  if ( ! buyer.isAdmin() ) {
    query = { buyer: buyer._id };
  }

  Policy
    .find(query)
    .exec(function(err, policies) {

      if (err) {
        return throwError('RequestFailedError')(res);
      }

      return sendResponse({policies: policies})(res);
    });
};

exports.createPolicy = (req, res, next) => {

  const user = res.locals.user;

  console.log(user);

  if ( ! createPolicyPermissionCheck(user) ) {
    return throwError('RolePermissionError')(res);
  }

  var policyBody = req.body;

  var policyData = {
    buyer: user._id,
  };

  if ( _.has(policyBody, 'tokenPolicy') ) {
    policyData.tokenPolicy = policyBody.tokenPolicy;
  }

  if ( _.has(policyBody, 'feePolicy') ) {
    policyData.feePolicy = policyBody.feePolicy;
  }

  if ( _.has(policyBody, 'name') ) {
    policyData.name = policyBody.name;
  }

  if ( _.has(policyBody, 'description') ) {
    policyData.description = policyBody.description;
  }

  const policy = new Policy(policyData);

  policy
    .save((err) => {

      if (err) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      return sendResponse({policy: policy})(res);
    });
};

exports.getPolicy = (req, res, next) => {
  Policy
    .findById(req.params.id)
    .populate('buyer')
    .exec(function(err, policy) {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      return sendResponse({policy: policy})(res);
    });
};

exports.updatePolicy = (req, res, next) => {

  const updateQuery = {};

  const updateKeys = [ 'name', 'description', 'tokenPolicy', 'feePolicy' ];

  _.forEach(updateKeys, function(key) {
    if ( _.has(req.body, key) ) {
      if ( _.isPlainObject(req.body[key]) ) {
        _.forEach(_.keys(req.body[key]), function(k) {
          var attributePath = _.join([ key, k ], '.');
          updateQuery[attributePath] = req.body[key][k];
        });
      } else {
        var attributePath = key;
        updateQuery[attributePath] = req.body[key];
      }
    }
  });

  console.log(updateQuery);

  Policy
    .findById(req.params.id)
    .populate('buyer')
    .exec(function(err, policy) {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      if ( ! modifyPolicyPermissionCheck(res.locals.user, policy) ) {
        return throwError('RolePermissionError')(res);
      }

      Policy
        .findByIdAndUpdate(req.params.id, { $set: updateQuery }, { new: true }, (err, updatedPolicy) => {
          if (err) {
            console.log(err);
            return throwError('RequestFailedError')(res);
          }

          return sendResponse({policy: updatedPolicy})(res);
        });
    });
};

exports.deletePolicy = (req, res, next) => {
  Policy
    .findById(req.params.id)
    .exec(function(err, policy) {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      policy
        .remove((err) => {
          if (err) {
            return throwError('RequestFailedError')(res);
          }

          if ( ! deletePolicyPermissionCheck(res.locals.user, policy) ) {
            return throwError('RolePermissionError')(res);
          }

          return sendResponse()(res);
        });
    });
};
