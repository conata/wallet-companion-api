
var stub = 'stub'

function getTitle(req, res, next) {
  return res.status(200).end()
}

function createTitle(req, res, next) {
  return res.status(200).end()
}

function dispatchAction(req, res, next) {
  return res.status(200).end()
}

function deleteTitle(req, res, next) {
  return res.status(200).end()
}

function listTitles(req, res, next) {
  return res.status(200).end()
}

function updateTitle(req, res, next) {
  return res.status(200).end()
}

module.exports = {
  action : dispatchAction,
  create : createTitle,
  delete : deleteTitle,
  get    : getTitle,
  list   : listTitles,
  update : updateTitle
}
