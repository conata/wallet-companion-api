const _ = require('lodash');
const moment = require('moment');

const utils = require('../lib/utils');

const throwError = utils.handlers.throwError;
const sendResponse = utils.handlers.sendResponse;

const TravelPayouts = require('kpdi-travelpayouts-api');

var api

try {
  api = new TravelPayouts(process.env['TRAVELPAYOUTS_API_TOKEN'],
                                process.env['TRAVELPAYOUTS_API_MARKER'],
                                { url: 'http://engine.hotellook.com' });
} catch(e) {

  api = {
    message: 'TravelPayouts',
    error: e
  }
}

function errorOp() {
  return function(message) {
    console.log('External-Body Object not loaded: ', message)
  }
}

exports.searchHotels = (req, res, next) => {

  if ( api && api.error && api.message )
    return errorOp(message)

  if ( _.isNil(req.query.lat) || _.isNil(req.query.lon) )
    return throwError('InvalidParametersError')(res);

  const hotelSearchOptions = {
    limit: req.query.limit || 50,
    offset: req.query.offset || 0
  }

  const coordinates = {
    latitude: req.query.lat,
    longitude: req.query.lon
  }

  if ( _.isNil(req.query.from) || _.isNil(req.query.to) )
    return throwError('InvalidParametersError');

  var fromDate = moment(new Date(req.query.from)).format('YYYY-MM-DD');
  var toDate = moment(new Date(req.query.to)).format('YYYY-MM-DD');

  const dates = {
    from: fromDate,
    to: toDate
  };

  api.searchLocations(coordinates.longitude, coordinates.latitude)
    .then(function(locationResults) {

      if ( locationResults.locations.length === 0 ) {
        return sendResponse({ hotels: [] });
      }

      const locationID = locationResults.locations[0].id;

      const hotelSearchParams = {
        location: {
          city: locationID
        },
        dates: dates
      };

      api.searchHotels(hotelSearchParams, hotelSearchOptions)
        .then(function(searchResults) {
          if ( searchResults.status === 'ok' ) {
            return sendResponse({ hotels : searchResults.result })(res);
          } else {
            return throwError('RequestFailedError')(res);
          }
        })
        .catch(function(err) {
          console.log(err);
          return throwError('RequestFailedError')(res);
        });
    })
    .catch(function(err) {
      console.log(err);
      return throwError('RequestFailedError')(res);
    });
};
