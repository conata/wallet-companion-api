# Distributed Objects

Distributed Objects Definitions (DODs) enable decentralization entry points for third-party and vendor actors and actions with a set of provisions for the formal input specification lanaguage with extensionalities that are mapped to action spaces within the distributed domain ontology.

## Available 

Available Objects are: Disabled - Loaded - Active

## Installed 

Installed Objects are: Available + Loaded + Disabled

## Loaded 

Installed Objects are: Installed + Available + Disabled

## Disabled

Disabled Objects are: Available + Loaded - Active

## Active 

Disabled Objects are: Installed + Loaded - Disabled

## Application Resource States (ROCA-REST; HATEOAS)

### Original Objects

### TimeGate Objects

### TimeMap Objects

### Memento Objects

