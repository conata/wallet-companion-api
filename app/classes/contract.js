const _ = require('lodash');
const moment = require('moment');

const Random = require('random-js');

const Coin = require('../models/Coin');
const Offer = require('../models/Offer');
const Transaction = require('../models/Transaction');
const Contract = require('../models/Contract');

const utils = require('../lib/utils');
const quilt = require('../lib/utils/quilt');
const cipher = require('../lib/utils/cipher');

const throwError = utils.handlers.throwError;
const sendResponse = utils.handlers.sendResponse;
const getActionHandler = utils.handlers.getActionHandler;
const dispatchAction = utils.handlers.dispatchAction;

const filterContracts  = utils.filters.filterContracts;
const extractFilterParams = utils.filters.extractFilterParams;

const createContractPermissionCheck = utils.permissions.createContractPermissionCheck;
const viewContractPermissionCheck = utils.permissions.viewContractPermissionCheck;
const modifyContractPermissionCheck = utils.permissions.modifyContractPermissionCheck;
const deleteContractPermissionCheck = utils.permissions.deleteContractPermissionCheck;

const lookupOfferForContract = utils.queries.lookupOfferForContract;
const lookupContractsForUser = utils.queries.lookupContractsForUser;
const calculateCostForContract = utils.queries.calculateCostForContract;
const createTransaction = utils.queries.createTransaction;
const updateTransaction = utils.queries.updateTransaction;
const updateContract = utils.queries.updateContract;

/**
 * @api {get} /api/v1/contracts List Contracts
 * @apiName listContracts
 * @apiGroup Contracts
 *
 * @apiSuccess {Object[]} contracts Array of Contract objects
 * @apiError error Error object
 */
exports.listContracts = (req, res, next) => {
  var errorHandler = function() {
    return throwError('RequestFailedError')(res);
  };

  var successHandler = function(contracts) {
    return sendResponse({contracts: contracts})(res);
  };

  return lookupContractsForUser(res.locals.user)(errorHandler, successHandler);
};

/**
 * @api {post} /api/v1/contracts/search Search Contracts
 * @apiName searchContracts
 * @apiGroup Contracts
 *
 * @apiSuccess {Object[]} contracts Array of Contract objects
 * @apiError error Error object
 */
exports.searchContracts = (req, res, next) => {

  const contractFilterKeys = [ 'status', 'buyer', 'coins', 'dates' ];

  var extractParams = extractFilterParams(req.body);

  var contractQuery = filterContracts(extractParams('contractFilter', contractFilterKeys));

  var errorHandler = function() {
    return throwError('RequestFailedError')(res);
  };

  var successHandler = function(contractsForUser) {

    var allowedContractIDs = _.map(contractsForUser, function(userContract) {
      return userContract._id.toString();
    });

    console.log(allowedContractIDs);

    Contract
      .find(contractQuery)
      .populate('buyer')
      .populate('coins.token')
      .exec(function(err, contracts) {

        var filterResults = _.filter(contracts, function(contractData) {
          return _.includes(allowedContractIDs, contractData._id.toString());
        });

        return sendResponse(filterResults)(res);
      });
  };

  return lookupContractsForUser(res.locals.user)(errorHandler, successHandler);
};

/**
 * @api {get} /api/v1/contracts/:id Get Contract
 * @apiName getContract
 * @apiGroup Contracts
 *
 * @apiParam {String} id Contract id
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
exports.getContract = (req, res, next) => {
  Contract
    .findById(req.params.id)
    .populate('buyer')
    .populate('coins.token')
    .exec(function(err, contract) {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      if ( contract === null ) {
        console.log('Got empty contract');
        return throwError('RequestFailedError')(res);
      }

      return sendResponse({contract: contract})(res);
    });
};

/**
 * @api {post} /api/v1/contracts Create Contract
 * @apiName createContract
 * @apiGroup Contracts
 *
 * @apiParam {Object} stub Stub for documentation
 *
 * @apiSuccess contract Contract object
 * @apiError error Error object
 */
exports.createContract = (req, res, next) => {

  var contractBody = req.body;

  if (!createContractPermissionCheck(res.locals.user)) {
    return throwError('RolePermissionError')(res);
  }

  var contractData = { dates: {} };

  const now = moment();
  const startDate = moment(new Date(contractBody.dates.from));
  const endDate = moment(new Date(contractBody.dates.to));

  contractData.buyer = res.locals.user._id;
  contractData.dates.from = startDate.toDate();
  contractData.dates.to = endDate.toDate();
  contractData.coins = contractBody.coins;

  if ( _.isEmpty(contractData.coins) || _.includes(contractData.coins, null) ) {
    return throwError('InvalidParametersError')(res);
  }

  if (
    startDate.isBefore(now, 'day') ||
    endDate.isBefore(now, 'day') ||
    endDate.isSameOrBefore(startDate, 'day')
  ) {
    return throwError('ContractInvalidDatesError')(res);
  }

  var coinIDs = _.map(contractData.coins, function(coinRef) { return coinRef.coin; });

  Coin
    .find()
    .where('_id')
    .in(coinIDs)
    .exec(function(err, coins) {

      var throwDateError = false;
      var throwQuantityError = false;

      if (err || ! coins || _.isEmpty(coins)) {
        console.log(err);
        return throwError('InvalidParametersError')(res);
      } else {

        _.forEach(coins, function(coin) {

          var coinStartDate = moment(new Date(coin.availability.from));
          var coinEndDate = moment(new Date(coin.availability.to));

          if (
            startDate.isBefore(coinStartDate, 'day')
            || endDate.isAfter(coinEndDate, 'day')
          ) {
            throwDateError = true;
          }

          var contractCoin = _.find(contractData.coins, function(o) {
            return o.coin === coin._id.toString();
          });

          if ( contractCoin.quantity > coin.quantity ) {
            throwQuantityError = true;
          }

        });
      }

      if ( throwDateError ) {
        console.log('Invalid dates given: outside availability range');
        return throwError('ContractDateNotAvailableError')(res);
      }
      else if ( throwQuantityError ) {
        console.log('Invalid quantity given: exceeds availability');
        return throwError('ContractQuantityExceedsAvailabilityError')(res);
      }
      else {
        const contract = new Contract(contractData);
        const duration = moment.duration(endDate.diff(startDate));
        const numDays = duration.asDays();

        contract
          .save((err) => {

            if (err) {
              console.log({ error: err });
              return throwError('RequestFailedError')(res);
            }

            Contract
              .findById(contract._id)
              .populate('coins.coin')
              .populate('buyer')
              .exec(function(err, data) {

                calculateCostForContract(data, createTransaction);

                quilt.call().then(function(client) {

                  var LEGEND, _cipherdict, key, encrypted;
                  var __error__;

                  console.log(contractData);

                  try {

                    LEGEND = cipher.init();
                    _cipherdict = cipher.gen(LEGEND, LEGEND.length);
                    key = _cipherdict.k;
                    encrypted = cipher.encrypt(key, key);

                  } catch(e) {

                    __error__ = e;

                    console.log({ error: e });

                  } finally {
                    if (__error__) {
                      return throwError('RequestFailedError')(res);
                    }
                  }

                  var ticket = _.extend(req, {
                    _contract : contract,
                    _cipher   : [encrypted, key].join('-')
                  });

                  return quilt.createTicket(req, res);

                }, function(err) {
                  console.log({ error: err  });
                }).catch(function(err) {
                  console.log({ error: err  });
                });
              });
          });
      }
    });
};

/**
 * @api {put} /api/v1/contracts/:id Update Contract
 * @apiName updateContract
 * @apiGroup Contracts
 *
 * @apiParam {String} id Contract id
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
exports.updateContract = (req, res, next) => {
  var user = res.locals.user;
  var updateParams = req.body;

  Contract
    .findById(req.params.id)
    .populate('coins.token')
    .exec((err, contract) => {
      if (err) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      var errorHandler = function() {
        return throwError('RequestFailedError')(res);
      };

      var successHandler = function(offer) {
        if ( ! modifyContractPermissionCheck(res.locals.user, contract, offer.buyer) ) {
          return throwError('RolePermissionError')(res);
        } else {
          calculateCostForContract(updateContract(contract, updateParams),
                                      updateTransaction);
          return sendResponse({contract: contract})(res);
        }
      };

      return lookupOfferForContract(contract)(errorHandler, successHandler);
    });
};

/**
 * @api {post} /api/v1/contracts/:id/action Dispatch Action
 * @apiName dispatchContractAction
 * @apiGroup Contracts
 *
 * @apiParam {String} id Contract id
 * @apiParam {Object} action Contract Action
 * @apiParam {String} action.key Action Key
 * @apiParam {Object} action.params Action Parameters
 *
 * @apiDescription Documentation for specific actions will
 * assume it is wrapped in an action object with the
 * appropriate key, only values for action.params will be
 * documented for a given action.
 *
 * @apiSuccess {Object} contract Contract object
 * @apiError error Error object
 */
exports.dispatchContractAction = (req, res, next) => {
  const actionKey = req.body.action.key;
  const actionParams = req.body.action.params || {};
  const contractActions = getActionHandler('contract', actionKey);

  const stripeActions = [ 'cancel', 'submitPayment', 'completePayment' ];

  if ( _.includes(stripeActions, actionKey) ) {
    actionParams.stripe = req.app.locals.stripe;
  }

  Contract
    .findById(req.params.id)
    .populate('buyer')
    .populate('coins.token')
    .exec((err, contract) => {
      if (err) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      var errorHandler = function() {
        return throwError('RequestFailedError')(res);
      };

      var successHandler = function(offer) {
        if ( ! modifyContractPermissionCheck(res.locals.user, contract, offer.buyer) ) {
          return throwError('RolePermissionError')(res);
        } else {
          const actionHandler = contractActions(contract);
          return dispatchAction(actionHandler, actionParams)(res);
        }
      };

      return lookupOfferForContract(contract)(errorHandler, successHandler);
    });
};

/**
 * @api {delete} /api/v1/contracts/:id Delete Contract
 * @apiName deleteContract
 * @apiGroup Contracts
 *
 * @apiParam {String} id Contract id
 *
 * @apiError error Error object
 */
exports.deleteContract = (req, res, next) => {

  Contract
    .findById(req.params.id)
    .exec((err, contract) => {
      if (err) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      if (!deleteContractPermissionCheck(res.locals.user, contract)) {
        return throwError('RolePermissionError')(res);
      } else {
        contract.remove((err) => {
          if (err) {
            console.log(err);
            return throwError('RequestFailedError')(res);
          }
          return sendResponse()(res);
        });
      }
    });
};
