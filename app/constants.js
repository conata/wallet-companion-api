/**
 * @fileOverview ./app/constants.js
 * @description
 * Elements of Refinement[0].
 *
 * The member role distinction is withdrawn from the application such that the
 * behavior of animating IDs in their transactions are inferred from Contract
 * and Offer relations, expressed and measured in labor tokens describing
 * Service and Resource discovery relations of non-local exchange interest
 * agents, royalty agents, liability agents, profit agents.
 *
 * So yes, they are distributed chatbots based on hashwords driving blockchain
 * interaction at a distance.
 *
 * ___
 * [0]: https://www.w3.org/TR/odrl-model/#x2-5-4-refinement-property-with-an-action
 */
var countrynames = require('countrynames');

/**
 * Roles
 */
const ROLE_MEMBER = 'buyer';
const ROLE_ADMIN = 'admin';

/**
 * User gender options
 */

const GENDER_MALE = 'Male';
const GENDER_FEMALE = 'Female';
const GENDER_OTHER = 'Other';

/**
 * Reservation Status
 */

const CONTRACT_PENDING = 'Pending';
const CONTRACT_APPROVED = 'Approved';
const CONTRACT_ACTIVE = 'Active';
const CONTRACT_CANCELLED = 'Cancelled';
const CONTRACT_COMPLETED = 'Completed';

/**
 * Transaction Status
 */
const TRANSACTION_PENDING = 'TransactionPending';
const TRANSACTION_PROCESSING = 'TransactionProcessing';
const TRANSACTION_COMPLETED = 'TransactionCompleted';
const TRANSACTION_CANCELLED = 'TransactionCancelled';
const TRANSACTION_REFUNDED = 'TransactionRefunded';
const TRANSACTION_FAILED = 'TransactionFailed';

/**
 * Country Data
 */

const COUNTRY_CODES = countrynames.getAllCodes();
const COUNTRY_NAMES = countrynames.getAllNames();

/**
 * Exported Constants
 */

const EXPORT_CONSTANTS = {
  ROLE_MEMBER: ROLE_MEMBER,
  ROLE_ADMIN: ROLE_ADMIN,
  CONTRACT_PENDING: CONTRACT_PENDING,
  CONTRACT_APPROVED: CONTRACT_APPROVED,
  CONTRACT_ACTIVE: CONTRACT_ACTIVE,
  CONTRACT_CANCELLED: CONTRACT_CANCELLED,
  CONTRACT_COMPLETED: CONTRACT_COMPLETED,
  TRANSACTION_PENDING: TRANSACTION_PENDING,
  TRANSACTION_PROCESSING: TRANSACTION_PROCESSING,
  TRANSACTION_COMPLETED: TRANSACTION_COMPLETED,
  TRANSACTION_FAILED: TRANSACTION_FAILED,
  GENDER_MALE: GENDER_MALE,
  GENDER_FEMALE: GENDER_FEMALE,
  GENDER_OTHER: GENDER_OTHER,
  COUNTRY_CODES:  COUNTRY_CODES,
  COUNTRY_NAMES:  COUNTRY_NAMES
};

module.exports = {
  ROLE_MEMBER,
  ROLE_ADMIN,
  CONTRACT_PENDING,
  CONTRACT_APPROVED,
  CONTRACT_ACTIVE,
  CONTRACT_CANCELLED,
  CONTRACT_COMPLETED,
  TRANSACTION_PENDING,
  TRANSACTION_PROCESSING,
  TRANSACTION_COMPLETED,
  TRANSACTION_FAILED,
  GENDER_MALE,
  GENDER_FEMALE,
  GENDER_OTHER,
  COUNTRY_CODES,
  EXPORT_CONSTANTS,
};
