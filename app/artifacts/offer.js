/* jshint asi: true */
'use strict';

const _ = require('lodash');
const moment = require('moment');
const Random = require("random-js");

const Coin = require('../models/Coin');
const Offer = require('../models/Offer');

const utils = require('../lib/utils');
const cipher = require('../lib/utils/cipher');

const throwError = utils.handlers.throwError;
const sendResponse = utils.handlers.sendResponse;
const getActionHandler = utils.handlers.getActionHandler;
const dispatchAction = utils.handlers.dispatchAction;

const filterCoins = utils.filters.filterCoins;
const filterOffers = utils.filters.filterOffers;
const extractFilterParams = utils.filters.extractFilterParams;

const createOfferPermissionCheck = utils.permissions.createOfferPermissionCheck;
const modifyOfferPermissionCheck = utils.permissions.modifyOfferPermissionCheck;
const deleteOfferPermissionCheck = utils.permissions.deleteOfferPermissionCheck;

/**
 * @apiDefine OfferObjectResponse
 *
 * @apiSuccess {Object} offer Offer
 * --
 * @apiSuccess {Object} offer.buyer User Object for offer buyer
 * --
 * @apiSuccess {Object[]} offer.contracts Contract objects for this offer
 * --
 * @apiSuccess {Object[]} offer.coins Array of coins
 * @apiSuccess {String} offer.coins.tokenType Type of token available
 * @apiSuccess {String} offer.coins.coinType Type of coin token available
 * @apiSuccess {String} offer.coins.payType Type of payment available
 * @apiSuccess {Number} offer.coins.quantity Number of coins available
 * @apiSuccess {Number} offer.coins.price Price for a single coin
 * @apiSuccess {Object} offer.coins.availability Dates this coin will be availalble
 * @apiSuccess {Date} offer.coins.availability.from Start date this coin is availabile
 * @apiSuccess {Date} offer.coins.availability.to End date this coin is availabile
 * --
 * @apiSuccess {String[]} offer.assets Assets available at this location
 * --
 * @apiSuccess {Object} offer.profile Profile for this offer listing
 * @apiSuccess {String} offer.profile.title Title for this offer listing
 * @apiSuccess {String} offer.profile.subtitle Subtitle for this offer listing
 * @apiSuccess {String} offer.profile.description Description for this offer listing
 * --
 * @apiSuccess {Object[]} offer.profile.contacts List of contacts for this offer (ETH, BTC, etc. addresses)
 * @apiSuccess {String} offer.profile.contacts.name Name for the contact
 * @apiSuccess {String} offer.profile.contacts.phone Phone number for the contact
 * @apiSuccess {String} offer.profile.contacts.email Email address for the contact
 * @apiSuccess {String} offer.profile.contacts.<address> Email address for the contact
 * @apiSuccess {Boolean} offer.profile.contacts.isPrimary True if this is the primary contact
 * @apiSuccess {String} offer.profile.contacts.email Email address for the contact
 * --
 * @apiSuccess {String} offer.profile.offerType Collectibles, Computer Equipment, ETH, BTC, XMR, COT offer
 * --
 * @apiSuccess {Object} offer.profile.location Object containing location information
 * @apiSuccess {String} offer.profile.location.address Street address for the offer
 * @apiSuccess {String} offer.profile.location.unit Unit for the offer
 * @apiSuccess {String} offer.profile.location.city City where the offer is located
 * @apiSuccess {String} offer.profile.location.province Province where the offer is located
 * @apiSuccess {String} offer.profile.location.state State where the offer is located
 * @apiSuccess {String} offer.profile.location.postalCode Postal code for the offer
 * @apiSuccess {String} offer.profile.location.country Country were the offer is located
 */


/**
 * @api {get} /api/v1/offers List Offers
 * @apiName listOffers
 * @apiGroup Offers
 *
 * @apiSuccess {Object[]} offers Array of Offer objects
 * @apiError error Error object
 */
let listOffers = (req, res, next) => {

  var page = req.query.page || 1;

  if ( ! _.isInteger(page) ) {
    page = _.toInteger(page);
  }

  var limit = req.query.limit || 10;

  if ( ! _.isInteger(limit) ) {
    limit = _.toInteger(limit);
  }

  var offerQuery = {};

  if (
    process.env['NODE_ENV'] === 'production' &&
    ! res.locals.user.isAdmin()
  ) {
    offerQuery['isPublished'] = true;
  }

  Coin
    .find(offerQuery)
    .populate('offer')
    .exec(function(err, coins) {
      if ( err ) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      var offers = _.uniq(
        _.map(coins, function(coin) {
          return coin.offer._id;
        })
      );

      Offer
        .paginate({}, {
          page: page,
          limit: limit,
          populate: 'profile.photos.image',
          sort: {
            'createdAt': -1
          }
        })
        .then(function(result) {

          var offers = result.docs;

          var offerResults =_.map(offers, function(offerData) {
            var offer = offerData.toJSON();

            // Don't leak contact information on unauthenticated requests
            if ( _.isUndefined(res.locals.user) ) {
              delete(offer.buyer);
              delete(offer.profile.contacts);
            }

            var matchingCoins = _.filter(coins, function(coin) {
              return offer._id.equals(coin.offer._id);
            });

            offer.coins = _.map(matchingCoins, function(coinData) {
              var coin = coinData.toJSON();
              var offerId = coin.offer._id;
              delete(coin.offer);
              coin.offer = offerId;
              return coin;
            });

            return offer;
          });

          return sendResponse({ 'offers' : offerResults })(res);
        });
    });
};


/**
 * @api {post} /api/v1/offers Search Offers
 * @apiName searchOffers
 * @apiGroup Offers
 *
 * @apiSuccess {Object[]} offers Array of Offer objects
 * @apiError error Error object
 */
let searchOffers = (req, res, next) => {

  const coinFilterKeys = [
    'tokenType',
    'coinType',
    'payType',
    'price',
    'availability'
  ];

  const offerFilterKeys = [
    'offerType',
    'assets',
    'location',
    'buyer'
  ];

  var extractParams = extractFilterParams(req.body);

  var coinQuery      = filterCoins(extractParams('coinFilter', coinFilterKeys));
  var offerQuery = filterOffers(extractParams('offerFilter', offerFilterKeys));

  if ( process.env['NODE_ENV'] === 'production' && ! _.isNil(res.locals.user) ) {
    if ( ! res.locals.user.isAdmin() ) {
      offerQuery['isPublished'] = true;
    }
  }

  Coin
    .find(coinQuery)
    .populate('offer')
    .exec(function(err, coins) {
      if ( err ) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      var offers = _.uniq(
        _.map(coins, function(coin) {
          return coin.offer._id;
        })
      );

      offerQuery._id = {
        $in: offers
      };

      Offer
        .find(offerQuery)
        .sort({ 'createdAt': -1 })
        .populate('profile.photos.image')
        .exec(function(err, offers) {

          if ( err ) {
            return throwError('RequestFailedError')(res);
          }

          var filterResults =_.map(offers, function(offerData) {
            var offer = offerData.toJSON();

            // Don't leak contact information on unauthenticated requests
            if ( _.isUndefined(res.locals.user) ) {
              delete(offer.buyer);
              delete(offer.profile.contacts);
            }

            var matchingCoins = _.filter(coins, function(coin) {
              return offer._id.equals(coin.offer._id);
            });

            offer.coins = _.map(matchingCoins, function(coinData) {
              var coin = coinData.toJSON();
              var offerId = coin.offer._id;
              delete(coin.offer);
              coin.offer = offerId;
              return coin;
            });

            return offer;
          });

          return sendResponse(filterResults)(res);
        });
    });
};

/**
 * @api {get} /api/v1/offers/:id Get Offer
 * @apiName getOffer
 * @apiGroup Offers
 *
 * @apiParam {String} id Offer id
 *
 * @apiUse OfferObjectResponse
 * @apiError error Error object
 */
let getOffer = (req, res, next) => {

  Offer
    .findById(req.params.id)
    .populate('buyer')
    .populate('profile.photos.image')
    .exec((err, offerData) => {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      if ( _.isNil(offerData) ) {
        console.log('OfferData is nil');
        return throwError('RequestFailedError')(res);
      } else {

        var offer = offerData.toJSON();

        // Don't leak contact information on unauthenticated requests
        if ( _.isUndefined(res.locals.user) ) {
          delete(offer.buyer);
          delete(offer.profile.contacts);
        }

        return sendResponse({offer: offer})(res);
      }
    });
};

/**
 * @api {post} /api/v1/offers Create Offer
 * @apiName createOffer
 * @apiGroup Offers
 *
 * @apiParam {stub} stub Stub for documentation
 *
 * @apiUse OfferObjectResponse
 * @apiError error Error object
 */
let createOffer = (req, res, next) => {

  var user = res.locals.user;
  var offerBody = req.body;

  if (!createOfferPermissionCheck(user)) {
    return throwError('RolePermissionError')(res);
  }

  var offerData = {};
  var offerCoins = [];

  const coinKeys = [
    'price',
    'quantity',
    'tokenType',
    'coinType',
    'payType',
    'description',
    'availability'
  ];

  if (_.has(offerBody, 'coins')) {
    offerCoins = offerBody.coins;
    offerData = _.omit(offerBody, ['coins']);

    _.forEach(offerCoins, function(coinData) {
      _.forEach(coinKeys, function(key) {
        if ( ! _.has(coinData, key) ) {
          return throwError('InvalidParametersError');
        }
      });
    });
  } else {
    offerData = offerBody;
  }

  const offer = new Offer(offerData);

  offer.buyer = user._id;

  offer.save((err) => {

    if (err) {

      console.log(err);

      return throwError('RequestFailedError')(res);

    } else {

      if ( ! _.isNil(offerCoins) && ! _.isEmpty(offerCoins)) {

        for(var i=0; i < offerCoins.length; i++) {

          var coinParams = offerCoins[i];

          if ( ! _.isNil(coinParams) && _.isPlainObject(coinParams) ) {

            var coinData = {
              offer        : offer._id,
              price        : coinParams.price,
              quantity     : coinParams.quantity,
              tokenType    : coinParams.tokenType,
              coinType     : coinParams.coinType,
              payType      : coinParams.payType,
              description  : coinParams.description,
              availability : {
                from     : new Date(coinParams.availability.from),
                to       : new Date(coinParams.availability.to),
                checkIn  : coinParams.availability.checkIn,
                checkOut : coinParams.availability.checkOut
              }
            };

            const coin = new Coin(coinData);

            coin.save((err) => {
              if (err) {
                console.log(err);
                return throwError('RequestFailedError')(res);
              }
            });
          }
        }
      }
    }

    var LEGEND = cipher.init();
    var staticLegend = cipher.LEGEND;
    var random = new Random(Random.engines.mt19937().autoSeed());
    var length = random.integer(1, Math.floor(Math.random() * 100));
    var list = [];

    for (var p = 0; p < length; ++p) {
      var _cipherdict = cipher.gen(LEGEND, LEGEND.length);
      var key = _cipherdict.k;
      var encrypted = cipher.encrypt(offer.profile.title, key);
      list.push({
        key: key,
        enc: encrypted
      });
    }

    return sendResponse({
      offer: offer,
      cipher: {
        legend : LEGEND,
        keys   : list
      }
    })(res);
  });

};

/**
 * @api {put} /api/v1/offers/:id Update Offer
 * @apiName updateOffer
 * @apiGroup Offers
 *
 * @apiParam {String} id Offer id
 * @apiParam {Coins[]} coins Array of Coin objects to add
 * @apiParam {Object} profile Profile information to update
 * @apiParam {String} offerType Type of offer
 *
 * @apiError error Error object
 */
let updateOffer = (req, res, next) => {

  var user = res.locals.user;

  const updateKeys = [ 'offerType', 'location',
                       'title', 'subtitle', 'description' ];

  var updateQuery = {};

  _.forEach(updateKeys, function(key) {
    if ( _.has(req.body, key) ) {
      if ( _.isPlainObject(req.body[key]) ) {
        _.forEach(_.keys(req.body[key]), function(k) {
          var attributePath = _.join([ 'profile', key, k ], '.');
          updateQuery[attributePath] = req.body[key][k];
        });
      } else {
        var attributePath = _.join([ 'profile', key], '.');
        updateQuery[attributePath] = req.body[key];
      }
    }
  });

  Offer
    .findById(req.params.id)
    .populate('buyer')
    .exec((err, offer) => {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      if (!modifyOfferPermissionCheck(user, offer)) {
        return throwError('RolePermissionError')(res);
      }

      Offer
        .findByIdAndUpdate(req.params.id, { $set: updateQuery }, { new: true }, (err, updatedOffer) => {
          if (err) {
            console.log(err);
            return throwError('RequestFailedError')(res);
          }
          return sendResponse({offer: updatedOffer})(res);
        });
    });
};

/**
 * @api {post} /api/v1/offers/:id/action Dispatch Action
 * @apiName dispatchOfferAction
 * @apiGroup Offers
 *
 * @apiParam {String} id Offer id
 * @apiParam {Object} action Offer Action
 * @apiParam {String} action.key Action Key
 * @apiParam {Object} action.params Action Parameters
 *
 * @apiDescription Documentation for specific actions will
 * assume it is wrapped in an action object with the
 * appropriate key, only values for action.params will be
 * documented for a given action.
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
let dispatchOfferAction = (req, res, next) => {

  const actionKey = req.body.action.key;
  const actionParams = req.body.action.params || {};

  const offerActions = getActionHandler('offer', actionKey);

  if ( actionKey === 'requestInformation' ) {
    actionParams['user'] = res.locals.user;
  }

  Offer
    .findById(req.params.id)
    .populate('buyer')
    .populate('profile.photos.image')
    .exec((err, offer) => {
      if (err) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      if ( actionKey !== 'requestInformation' && ! modifyOfferPermissionCheck(res.locals.user, offer) ) {
        return throwError('RolePermissionError')(res);
      }

      const actionHandler = offerActions(offer);

      return dispatchAction(actionHandler, actionParams)(res);
    });
};

/**
 * @api {delete} /api/v1/offers/:id Delete Offer
 * @apiName deleteOffer
 * @apiGroup Offers
 *
 * @apiParam {String} id Offer id
 *
 * @apiError error Error object
 */
let deleteOffer = (req, res, next) => {

  Offer.findById(req.params.id, (err, offer) => {
    if (err) {
      return throwError('RequestFailedError')(res);
    }

    if (!deleteOfferPermissionCheck(res.locals.user, offer)) {
      return throwError('RolePermissionError')(res);
    }

    offer.remove((err) => {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      return sendResponse()(res);
    });
  });
};

/**
 * @api {get} /api/v1/offers/:id/coins List Coins
 * @apiName listCoinsForOffer
 * @apiGroup Offers
 *
 * @apiParam {String} id Offer id
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
let listCoinsForOffer = (req, res, next) => {

  Coin
    .find({ offer: req.params.id })
    .exec((err, coins) => {
      if (err) {
        console.log(err);
        return throwError('RequestFailedError')(res);
      }

      return sendResponse({coins: coins})(res);
  });
};

/**
 * @api {post} /api/v1/offers/:id/coins Create Coin
 * @apiName createCoinForOffer
 * @apiGroup Offers
 *
 * @apiParam {String} id Offer id
 *
 * @apiSuccess {Object} offer Offer object
 * @apiError error Error object
 */
let createCoinForOffer = (req, res, next) => {

  const coinData = {};

  coinData.offer = req.params.id;
  coinData.price = req.body.price;
  coinData.quantity = req.body.quantity;
  coinData.tokenType = req.body.tokenType;
  coinData.coinType = req.body.coinType;
  coinData.payType = req.body.payType;
  coinData.description = req.body.description;
  coinData.availability = {
    from: new Date(req.body.availability.from),
    to: new Date(req.body.availability.to),
    checkIn: req.body.availability.checkIn,
    checkOut: req.body.availability.checkOut
  };

  Offer
    .findById(coinData.offer)
    .populate('buyer')
    .exec((err, offer) => {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      if ( ! modifyOfferPermissionCheck(res.locals.user, offer)) {
        return throwError('RolePermissionError')(res);
      }

      const coin = new Coin(coinData);

      coin.save((err) => {
        if (err) {
          return throwError('RequestFailedError')(res);
        }
        return sendResponse({coin: coinData})(res);
      });
    });
};

/**
 * @api {put} /api/v1/offers/:offerId/coins/coinId Update Coin
 * @apiName updateCoinForOffer
 * @apiGroup Offers
 *
 * @apiParam {String} offerId Offer ID
 * @apiParam {String} coinId Coin ID
 *
 * @apiSuccess {Object} coin Coin object
 * @apiError error Error object
 */
let updateCoinForOffer = (req, res, next) => {

  const updateKeys = [
    'price',
    'quantity',
    'tokenType',
    'coinType',
    'payType',
    'description',
    'availability'
  ];

  var updateQuery = {};

  Offer
    .findById(req.params.offerID)
    .populate('buyer')
    .exec((err, offer) => {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      if ( ! modifyOfferPermissionCheck(res.locals.user, offer)) {
        return throwError('RolePermissionError')(res);
      }

      _.forEach(updateKeys, function(key) {
        if ( _.has(req.body, key) ) {
          if ( _.isPlainObject(req.body[key]) ) {
            _.forEach(_.keys(req.body[key]), function(k) {
              var attributePath = _.join([ key, k ], '.');
              updateQuery[attributePath] = req.body[key][k];
            });
          } else {
            updateQuery[key] = req.body[key];
          }
        }
      });

      var configureQuery = {
        $set: updateQuery
      };

      var methodSetting = {
        new: true
      };

      Coin
        .findByIdAndUpdate(req.params.coinID, configureQuery, methodSetting, (err, coin) => {
          if (err) {
            return throwError('RequestFailedError')(res);
          }

          return sendResponse({ coin: coin })(res);
        });
    });
};

/**
 * @api {get} /api/v1/offers/:offerId/coins/coinId Update Coin
 * @apiName getCoinForOffer
 * @apiGroup Offers
 *
 * @apiParam {String} offerId Offer ID
 * @apiParam {String} coinId Coin ID
 *
 * @apiSuccess {Object} coin Coin object
 * @apiError error Error object
 */
let getCoinForOffer = (req, res, next) => {

  Coin
    .findById(req.params.coinID)
    .populate('offer')
    .exec((err, coin) => {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      return sendResponse({ coin: coin })(res);
    });
};


/**
 * @api {delete} /api/v1/offers/:offerID/coins/:coinID Delete Coin
 * @apiName deleteCoinForOffer
 * @apiGroup Offers
 *
 * @apiParam {String} id Offer id
 *
 * @apiError error Error object
 */
let deleteCoinForOffer = (req, res, next) => {

  Coin
    .findById(req.params.coinID)
    .populate('offer')
    .exec((err, coin) => {
      if (err) {
        return throwError('RequestFailedError')(res);
      }

      if ( ! modifyOfferPermissionCheck(res.locals.user, coin.offer)) {
        return throwError('RolePermissionError')(res);
      }

      coin.remove((err) => {
        if (err) {
          return throwError('RequestFailedError')(res);
        }

        return sendResponse()(res);
      });
    });
};

exports.listOffers = listOffers;
exports.deleteCoinForOffer = deleteCoinForOffer;
exports.getCoinForOffer = getCoinForOffer;
exports.updateCoinForOffer = updateCoinForOffer;
exports.searchOffers = searchOffers;
exports.getOffer = getOffer;
exports.createCoinForOffer = createCoinForOffer;
exports.listCoinsForOffer = listCoinsForOffer;
exports.deleteOffer = deleteOffer;
exports.dispatchOfferAction = dispatchOfferAction;
exports.updateOffer = updateOffer;
exports.createOffer = createOffer;

