const Organization = require('../models/Organization');

const throwError = require('../utils').throwError;
const sendResponse = require('../utils').sendResponse;

const getActionHandler = require('../utils').getActionHandler;
const dispatchAction = require('../utils').dispatchAction;

/**
 * @api {get} /api/v1/organizations List Organizations
 * @apiName listOrganizations
 * @apiGroup Organizations
 *
 * @apiSuccess {Object[]} organizations Array of Organization objects
 * @apiError error Error object
 */
exports.listOrganizations = (req, res, next) => {
  Organization.find({}, function (err, organizations) {
    if (err) {
      console.log(err);
      return throwError('RequestFailedError')(res);
    }
    return sendResponse({organizations: organizations})(res);
  });
};

/**
 * @api {get} /api/v1/organizations/:id Get Organization
 * @apiName getOrganization
 * @apiGroup Organizations
 *
 * @apiParam {String} id Organization id
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
exports.getOrganization = (req, res, next) => {
  return throwError('ResourceNotImplementedError')(res);
};

/**
 * @api {post} /api/v1/organizations Create Organization
 * @apiName createOrganization
 * @apiGroup Organizations
 *
 * @apiParam {Object} stub Stub for documentation
 *
 * @apiSuccess organization Organization object
 * @apiError error Error object
 */
exports.createOrganization = (req, res, next) => {
  return throwError('ResourceNotImplementedError')(res);
};


/**
 * @api {put} /api/v1/organizations/:id Update Organization
 * @apiName updateOrganization
 * @apiGroup Organizations
 *
 * @apiParam {String} id Organization id
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
exports.updateOrganization = (req, res, next) => {
  return throwError('ResourceNotImplementedError')(res);
};

/**
 * @api {post} /api/v1/organizations/:id/action Dispatch Action
 * @apiName dispatchOrganizationAction
 * @apiGroup Organizations
 *
 * @apiParam {String} id Organization id
 * @apiParam {Object} action Organization Action
 * @apiParam {String} action.key Action Key
 * @apiParam {Object} action.params Action Parameters
 *
 * @apiDescription Documentation for specific actions will
 * assume it is wrapped in an action object with the
 * appropriate key, only values for action.params will be
 * documented for a given action.
 *
 * @apiSuccess {Object} organization Organization object
 * @apiError error Error object
 */
exports.dispatchOrganizationAction = (req, res, next) => {
  const actionKey = req.body.action.key;
  const actionParams = req.body.action.params;
  const organizationActions = getActionHandler('organization', actionKey);

  Organization.findById(req.params.id, (err, organization) => {
    if (err) {
      console.log(err);
      return throwError('RequestFailedError')(res);
    }

    const actionHandler = organizationActions(organization);

    return dispatchAction(actionHandler, actionParams)(res);
  });
};

/**
 * @api {delete} /api/v1/organizations/:id Delete Organization
 * @apiName deleteOrganization
 * @apiGroup Organizations
 *
 * @apiParam {String} id Organization id
 *
 * @apiError error Error object
 */
exports.deleteOrganization = (req, res, next) => {
  return throwError('ResourceNotImplementedError')(res);
};
