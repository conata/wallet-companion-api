/**
 * @fileOverview ./app/controllers/app.js
 * @name TW.controllers.app
 * @description
 * @TODO Core microservice capabilities that which be moved over to a semaphore.
 */
const _ = require('lodash')
const country = require('countryjs')
const utils = require('../lib/utils')

const throwError = utils.handlers.throwError
const sendResponse = utils.handlers.sendResponse
const getActionHandler = utils.handlers.getActionHandler
const dispatchAction = utils.handlers.dispatchAction

const getCountryName = utils.constants.getCountryName
const getConstant = utils.constants.getConstant

const sendEmail = utils.email.sendEmail

/**
 * Set route for create user and authentication when under maintenance
 */
function maintenanceMode(req, res, next) {
  return throwError('MaintenanceModeError')(res)
}


/**
 * GET /app/config/stripe
 */
function getStripeKey(req, res, next) {

  var key

  try {
    key = process.env.STRIPE_PUBLISHABLEKEY
  } catch(e) {
    key = {
      messgae: 'No Stripe PublishableKey found.',
      error: e
    }
  }

  if (key.hasOwnOffer('error')) {
    return function() { console.log(key.message) }
  } else {
    return sendResponse({ key: key })(res)
  }

}


/**
 * GET /app/constants
 */
function getConstants(req, res, next) {
  const appConstants = getConstant()
  return sendResponse({ constants: appConstants })(res)
}


/**
 * GET /app/constants/countries
 */
function getCountryCodes(req, res, next) {

  var countryMap = {}

  _.forEach(utils.constants.getCountryCodes(), function(countryCode) {
    var countryName = getCountryName(countryCode)
    countryMap[countryName] = countryCode
  })

  return sendResponse({ countries: countryMap })(res)
}


/**
 * GET /app/constants/countries/:countryCode
 */
function getCountryInfo(req, res, next) {

  const countryCode = req.params.countryCode

  const countryCodeList = utils.constants.getCountryCodes()

  console.log(countryCode)
  console.log(countryCodeList)

  if ( ! _.includes(countryCodeList, countryCode)) {
    return throwError('InvalidParametersError')(res)
  }

  var countryInfo = country.info(countryCode)

  if ( ! countryInfo ) {
    return throwError('RequestFailedError')(res)
  }

  return sendResponse({ countryInfo: countryInfo })(res)
}


/**
 * GET /app/constants/countries/:countryCode/states
 */
function getStatesForCountry(req, res, next) {

  const countryCode = req.params.countryCode

  const countryCodeList = utils.constants.getCountryCodes()

  if ( ! _.includes(countryCodeList, countryCode )) {
    return throwError('InvalidParametersError')(res)
  }

  var countryStates = country.states(countryCode)

  if ( ! countryStates ) {
    return throwError('RequestFailedError')(res)
  }

  return sendResponse({ states: countryStates })(res)
}


/**
 * POST /app/support
 */
function createSupportRequest(req, res, next) {
  const currentUser = res.locals.user
  const supportEmail = process.env['WALLETCOMPANION_SUPPORT_EMAIL_SENDTO']

  if ( ! currentUser ) {
    return throwError('RolePermissionError')
  }

  const supportRequest = {
    subject: '[Support Request] ' + req.body.subject,
    body: req.body.message
  }

  const emailParams = {
    sendTo: supportEmail,
    replyTo: currentUser.email,
    subject: supportRequest.subject,
    body: supportRequest.body,
  }

  sendEmail(emailParams)

  return sendResponse()(res)
}



/**
 * EXPORTS
 */

module.exports = {
    createSupportRequest : createSupportRequest,
    getStatesForCountry  : getStatesForCountry,
    maintenanceMode      : maintenanceMode,
    getCountryInfo       : getCountryInfo,
    getStripeKey         : getStripeKey,
    getConstants         : getConstants,
    getCountryCodes      : getCountryCodes
}

