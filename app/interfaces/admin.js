const _ = require('lodash');

const User = require('../models/User');

const utils = require('../lib/utils');

const throwError = utils.handlers.throwError;
const sendResponse = utils.handlers.sendResponse;
const getActionHandler = utils.handlers.getActionHandler;
const dispatchAction = utils.handlers.dispatchAction;

const ROLE_ADMIN = utils.constants.getConstant('ROLE_ADMIN');

exports.setup = (req, res, next) => {

  var stripe = req.app.locals.stripe;

  createAdminUser();

  return sendResponse()(res);
};

function createAdminUser() {
  const adminUser = {
    email: 'admin@backpackercollege.com',
    password: 'a19a2d704ee1c36ad335e1b9722b2d39661780d3',
    profile: {
      name: {
        first: 'Wallet Companion',
        last: 'Admin'
      },
      location: {
        city: 'Toronto',
        state: 'ON',
        postalCode: 'P2P',
        country: 'CA'
      },
      phone: '416-555-5555'
    },
    role: ROLE_ADMIN
  };

  let admin = new User(adminUser);

  admin.setProfileVerified();
  admin.setProfileApproved();
  admin.setProfileComplete();

  admin.save();

  return admin;
}

function deleteAllStripeCustomers(stripe) {
  stripe.customers
    .list( { limit: 100 } )
    .then(function(customers) {
      _.forEach(customers.data, function(customer) {
        console.log('Deleting customer: ' + customer.id);
        stripe.customers
          .del(customer.id)
          .then(function(response) {
            console.log(response);
          });
      });
    });
}
