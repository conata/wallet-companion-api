const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');

const ObjectId =  mongoose.Schema.Types.ObjectId;

const ROLE_MEMBER = require('../constants').ROLE_MEMBER; // Buyer
const ROLE_ADMIN = require('../constants').ROLE_ADMIN; // Administrator

const GENDER_MALE = require('../constants').GENDER_MALE;
const GENDER_FEMALE = require('../constants').GENDER_FEMALE;
const GENDER_OTHER = require('../constants').GENDER_OTHER;

const COUNTRY_CODES = require('../constants').COUNTRY_CODES;

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    select: false,
    required: true
  },
  passwordResetToken: {
    type: String,
    select: false
  },
  passwordResetExpires: {
    type: Date,
    select: false
  },
  verificationToken: {
    type: String,
    select: false
  },
  verificationExpires: {
    type: Date,
    select: false
  },
  role: {
    type: String,
    enum: [ROLE_MEMBER, ROLE_ADMIN],
    default: ROLE_MEMBER
  },
  profile: {
    name: {
      first: {
        type: String,
        required: true
      },
      last: {
        type: String,
        required: true
      },
    },
    gender: {
      type: String,
      enum: [GENDER_MALE, GENDER_FEMALE, GENDER_OTHER],
    },
    location: {
      city: {
        type: String,
        required: true
      },
      state: {
        type: String,
        required: true
      },
      postalCode: {
        type: String,
        required: true
      },
      country: {
        type: String,
        enum: COUNTRY_CODES,
        required: true
      },
    },
    phone: {
      type: String,
      required: true
    },
    avatar: {
      type: ObjectId,
      ref: 'Asset',
    }
  },
  verification: {
    isComplete: {
      type: Boolean,
      default: false
    },
    isVerified: {
      type: Boolean,
      default: false
    },
    isApproved: {
      type: Boolean,
      default: true
    },
    photoID: {
      type: String,
      select: false
    }
  },
  stripe: {
    customerId: {
      type: String,
    }
  }
}, { timestamps: true, id: true });

/**
 * Plugins
 */

userSchema.plugin(paginate);

/**
 * Middleware
 */

userSchema.pre('save', function save(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) { return next(err); }
      user.password = hash;
      next();
    });
  });
});

/**
 * Methods
 */

userSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

userSchema.methods.hasRole = function hasRole(role) {
  if ( this.role === role ) {
    return true;
  }
  return false;
};

userSchema.methods.isBuyer = function isBuyer() {
  return this.hasRole(ROLE_MEMBER);
};

userSchema.methods.isOfferBuyer = function isOfferBuyer() {
  return this.hasRole(ROLE_MEMBER);
};

userSchema.methods.isAdmin = function isAdmin() {
  return this.hasRole(ROLE_ADMIN);
};

userSchema.methods.setAdmin = function setAdmin() {
  this.role = ROLE_ADMIN;
};

userSchema.methods.setProfileVerified = function setProfileVerified() {
  this.verification.isVerified = true;
};

userSchema.methods.setProfileApproved = function setProfileApproved() {
  this.verification.isApproved = true;
};

userSchema.methods.setProfileComplete = function setProfileComplete() {
  this.verification.isComplete = true;
};

userSchema.methods.isVerified = function isVerified() {
  return this.verification.isVerified;
};

userSchema.methods.isComplete = function isComplete() {
  return this.verification.isComplete;
};

userSchema.methods.isApproved = function isApproved() {
  return this.verification.isApproved;
};

/**
 * Statics
 */

userSchema.statics.findUserByEmail = function(email, cb) {
  User.find({ email: email }, cb);
};

/**
 * Virtuals
 */

userSchema.virtual('links').get(function() {

  const selfURI = '/api/v1/users/' + this._id;

  const links = [
    {
      rel: 'self',
      href: selfURI
    },
    {
      rel: 'setProfilePhoto',
      href: selfURI + '/action'
    },
    {
      rel: 'setProfileVerified',
      href: selfURI + '/action'
    },
    {
      rel: 'setProfileComplete',
      href: selfURI + '/action'
    },
    {
      rel: 'addPaymentMethod',
      href: selfURI + '/action'
    },
    {
      rel: 'getPaymentMethods',
      href: selfURI + '/action'
    },
    {
      rel: 'editPaymentMethod',
      href: selfURI + '/action'
    },
    {
      rel: 'removePaymentMethod',
      href: selfURI + '/action'
    },
  ];

  return links;
});

userSchema.set('toObject', { virtuals: true });
userSchema.set('toJSON',   { virtuals: true });

/**
 * Exports
 */

const User = mongoose.model('User', userSchema);

module.exports = User;
