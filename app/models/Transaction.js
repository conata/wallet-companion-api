const mongoose = require('mongoose');

const ObjectId =  mongoose.Schema.Types.ObjectId;

const TRANSACTION_PENDING = require('../constants').TRANSACTION_PENDING;
const TRANSACTION_PROCESSING = require('../constants').TRANSACTION_PROCESSING;
const TRANSACTION_COMPLETED = require('../constants').TRANSACTION_COMPLETED;
const TRANSACTION_CANCELLED = require('../constants').TRANSACTION_CANCELLED;
const TRANSACTION_REFUNDED = require('../constants').TRANSACTION_REFUNDED;
const TRANSACTION_FAILED = require('../constants').TRANSACTION_FAILED;

const transactionSchema = new mongoose.Schema({
  status: {
    type: String,
    enum: [
      TRANSACTION_PENDING,
      TRANSACTION_PROCESSING,
      TRANSACTION_COMPLETED,
      TRANSACTION_CANCELLED,
      TRANSACTION_REFUNDED,
      TRANSACTION_FAILED
    ],
    default: TRANSACTION_PENDING
  },
  prefix: {
    type: String
  },
  type: {
    type: String
  },
  hash: {
    type: String
  },
  asset: {
    type: String
  },
  quantity: {
    type: String
  },
  timegate: {
    type: String
  },
  contract: {
    type: ObjectId,
    ref: 'Contract',
    required: true
  },
  amountDue: {
    type: Number,
    required: true
  },
  amountPending: {
    type: Number,
    default: 0.00
  },
  amountPaid: {
    type: Number,
    default: 0.00
  },
  amountRefunded: {
    type: Number,
    default: 0.00
  },
  amountHeldForDeposit: {
    type: Number,
    default: 0.00
  },
  paymentSource: {
    type: String
  },
  stripeCharge: {
    type: String
  },
  stripeRefund: {
    type: String
  },
  subtotal: {
    type: Number
  },
  serviceFee: {
    type: Number
  },
  cleaningFee: {
    type: Number
  },
  securityDeposit: {
    type: Number
  },
  taxes : {
    type: Number
  },
  salesTax: {
    type: Number
  },
  occupncyTax: {
    type: Number
  }
}, {timestamps: true, id: false});

/**
 * Methods
 */

transactionSchema.methods.setServiceFee = function(amount) {
  this.serviceFee = amount;
};

transactionSchema.methods.setCleaningFee = function(amount) {
  this.cleaningFee = amount;
};

transactionSchema.methods.setSecurityDeposit = function(amount) {
  this.securityDepsoit = amount;
  this.amountHeldForDeposit = amount;
};

transactionSchema.methods.setSalesTax = function(amount) {
  this.salesTax = amount;
  this.taxes += amount;
};

transactionSchema.methods.setOccupancyTax = function(amount) {
  this.occupancyTax = amount;
  this.taxes += amount;
};

transactionSchema.methods.setSubtotal = function(amount) {
  this.subtotal = amount;
};

transactionSchema.methods.setAmountDue = function(amount) {
  this.amountDue = amount;
};

transactionSchema.methods.setAmountPending = function(amount) {
  this.amountPending = amount;
};

transactionSchema.methods.setAmountPaid = function(amount) {
  this.amountPaid = amount;
};

transactionSchema.methods.setAmountRefunded = function(amount) {
  this.amountRefunded = amount;
};

transactionSchema.methods.setAmountHeldForDeposit = function(amount) {
  this.amountHeldForDeposit = amount;
};

transactionSchema.methods.setPaymentSource = function(sourceToken) {
  this.paymentSource = sourceToken;
};

transactionSchema.methods.setStripeCharge = function(chargeID) {
  this.stripeCharge = chargeID;
};

transactionSchema.methods.setStripeRefund = function(refundID) {
  this.stripeRefund = refundID;
};

transactionSchema.methods.setStatusProcessing = function() {
  if ( this.isStatusPending() || this.isStatusFailed() ) {
    this.status = TRANSACTION_PROCESSING;
    return true;
  }
  return false;
};

transactionSchema.methods.setStatusCompleted = function() {
  if ( this.isStatusProcessing() ) {
    this.status = TRANSACTION_COMPLETED;
    return true;
  }
  return false;
};

transactionSchema.methods.setStatusFailed = function() {
  if ( this.isStatusProcessing() ) {
    this.status = TRANSACTION_FAILED;
    return true;
  }
  return false;
};

transactionSchema.methods.setStatusRefunded = function() {
  if ( this.isStatusProcessing() || this.isStatusCompleted() ) {
    this.status = TRANSACTION_REFUNDED;
    return true;
  }
  return false;
};

transactionSchema.methods.setStatusCancelled = function() {
  if ( this.isStatusPending() || this.isStatusProcessing() ) {
    this.status = TRANSACTION_CANCELLED;
    return true;
  }
  return false;
};

transactionSchema.methods.isStatusPending = function() {
  if ( this.status === TRANSACTION_PENDING ) {
    return true;
  }
  return false;
};

transactionSchema.methods.isStatusProcessing = function() {
  if ( this.status === TRANSACTION_PROCESSING ) {
    return true;
  }
  return false;
};

transactionSchema.methods.isStatusCompleted = function() {
  if ( this.status === TRANSACTION_COMPLETED ) {
    return true;
  }
  return false;
};

transactionSchema.methods.isStatusFailed = function() {
  if ( this.status === TRANSACTION_FAILED ) {
    return true;
  }
  return false;
};

transactionSchema.methods.getAmountDue = function() {
  return this.amountDue;
};

transactionSchema.methods.getAmountPending = function() {
  return this.amountPending;
};

transactionSchema.methods.getAmountPaid = function() {
  return this.amountPaid;
};

transactionSchema.methods.getAmountRefunded = function() {
  return this.amountRefunded;
};

transactionSchema.methods.getAmountHeldForDeposit = function() {
  return this.amountHeldForDeposit;
};

transactionSchema.methods.getPaymentSource = function() {
  return this.paymentSource;
};

transactionSchema.methods.getStripeCharge = function() {
  return this.stripeCharge;
};

transactionSchema.methods.getStripeRefund = function() {
  return this.stripeRefund;
};

/**
 * Statics
 */

transactionSchema.statics.findTransactionForContract = function(contractID, cb) {
  Transaction.findOne({ 'contract' : contractID}, cb);
};

/**
 * Exports
 */

const Transaction = mongoose.model('Transaction', transactionSchema);

module.exports = Transaction;
