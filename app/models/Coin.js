const mongoose = require('mongoose');

const ObjectId =  mongoose.Schema.Types.ObjectId;

const coinSchema = new mongoose.Schema({
  // @TODO was privacy setting
  tokenType: {
    type: String,
    enum: [ 'erc20', 'erc721', 'erc223', 'counterparty'],
    default: 'erc20'
  },
  // @TODO was room type
  coinType: {
    type: String,
    enum: [ 'scrypt', 'sha256' ],
    default: 'sha256'
  },
  // @TODO was bathtype
  payType: {
    type: String,
    enum: [ 'card', 'bank', 'network', 'cash', 'manual' ],
    default: 'network'
  },
  offer: {
    type: ObjectId,
    ref: 'Offer',
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  availability:
  {
    from: {
      type: Date,
      default: Date.now
    },
    to: {
      type: Date,
    },
    checkIn: {
      type: String,
      required: true
    },
    checkOut: {
      type: String,
      required: true
    }
  },
  description: {
    type: String,
    required: true
  }
}, {timestamps: true, id: false});

/**
 * Statics
 */

coinSchema.statics.findCoinsForOffer = function(offerID, cb) {
  Coin.find({ 'offer': offerID }, cb);
};

/**
 * Virtuals
 */

coinSchema.virtual('links').get(function() {

  const selfURI = '/api/v1/offers/' + this.offer._id + '/coins/' + this._id;

  const links = [
    {
      rel: 'self',
      href: selfURI
    }
  ];

  return links;
});

coinSchema.set('toObject', { virtuals: true });
coinSchema.set('toJSON',   { virtuals: true });

/**
 * Exports
 */

const Coin = mongoose.model('Coin', coinSchema);

module.exports = Coin;
