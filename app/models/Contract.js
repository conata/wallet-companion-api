const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');

const ObjectId =  mongoose.Schema.Types.ObjectId;

const CONTRACT_PENDING = require('../constants').CONTRACT_PENDING;
const CONTRACT_APPROVED = require('../constants').CONTRACT_APPROVED;
const CONTRACT_ACTIVE = require('../constants').CONTRACT_ACTIVE;
const CONTRACT_CANCELLED = require('../constants').CONTRACT_CANCELLED;
const CONTRACT_COMPLETED = require('../constants').CONTRACT_COMPLETED;

// Contracts are types of Messages; front matter/end Missives are
// coordinations to the correct Message/Contract.

const contractSchema = new mongoose.Schema({
  status: {
    type: String,
    enum: [
      CONTRACT_PENDING,
      CONTRACT_APPROVED,
      CONTRACT_ACTIVE,
      CONTRACT_CANCELLED,
      CONTRACT_COMPLETED
    ],
    default: CONTRACT_PENDING
  },
  buyer: {
    type: ObjectId,
    ref: 'User',
    required: true
  },
  timegate: {
    type: String,
    required: false
  },
  dates: {
    from: {
      type: Date,
      required: true
    },
    to:{
      type: Date,
      required: true
    }
  },
  coins: [{
    _id: false,
    coin: {
      type: ObjectId,
      ref: 'Coin',
      required: true
    },
    quantity: {
      type: Number,
      default: 1,
      required: true
    }
  }]
}, { timestamps: true, id: false });

/**
 * Plugins
 */

contractSchema.plugin(paginate);

/**
 * Middleware
 */

contractSchema.pre('remove', function(next) {
  // Flag transactions as cancelled/refunded
  next();
});

/**
 * Methods
 */

contractSchema.methods.setStatusApproved = function() {

  if ( this.isStatusPending() ) {
    this.status = CONTRACT_APPROVED;
    return true;
  }
  return false;
};

contractSchema.methods.setStatusActive = function() {

  if ( this.isStatusApproved() ) {
    this.status = CONTRACT_ACTIVE;
    return true;
  }
  return false;
};

contractSchema.methods.setStatusComplete = function() {

  if ( this.isStatusActive() ) {
    this.status = CONTRACT_COMPLETED;
    return true;
  }
  return false;
};

contractSchema.methods.setStatusCancelled = function() {

  if ( ! this.isStatusComplete() ) {
    this.status = CONTRACT_CANCELLED;
    return true;
  }

  return false;
};

contractSchema.methods.isStatusPending = function() {
  if ( this.status === CONTRACT_PENDING ) {
    return true;
  }
  return false;
};

contractSchema.methods.isStatusApproved = function() {
  if ( this.status === CONTRACT_APPROVED ) {
    return true;
  }
  return false;
};

contractSchema.methods.isStatusActive = function() {
  if ( this.status === CONTRACT_ACTIVE ) {
    return true;
  }
  return false;
};

contractSchema.methods.isStatusComplete = function() {
  if ( this.status === CONTRACT_COMPLETED ) {
    return true;
  }
  return false;
};

contractSchema.methods.isStatusCancelled = function() {
  if ( this.status === CONTRACT_CANCELLED ) {
    return true;
  }
  return false;
};

/**
 * Statics
 */

contractSchema.statics.findAllContractsForOffer = function(offerID, cb) {
  Contract.find({offer : offerID}, cb);
};

contractSchema.statics.findAllContractsForBuyer = function(userID, cb) {
  Contract.find({buyer : userID}, cb);
};

/**
 * Virtuals
 */

contractSchema.virtual('links').get(function() {

  const selfURI = '/api/v1/contracts/' + this._id;

  const links = [
    {
      rel: 'self',
      href: selfURI
    },
    {
      rel: 'cancel',
      href: selfURI + '/action'
    },
    {
      rel: 'addCoin',
      href: selfURI + '/action'
    },
    {
      rel: 'changeDates',
      href: selfURI + '/action'
    },
    {
      rel: 'getInvoice',
      href: selfURI + '/action'
    },
    {
      rel: 'submitPayment',
      href: selfURI + '/action'
    },
    {
      rel: 'completePayment',
      href: selfURI + '/action'
    }
  ];

  return links;
});


contractSchema.set('toObject', { virtuals: true });
contractSchema.set('toJSON',   { virtuals: true });

/**
 * Exports
 */

const Contract = mongoose.model('Contract', contractSchema);

module.exports = Contract;
