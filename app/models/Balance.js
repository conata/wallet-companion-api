const mongoose = require('mongoose');

const ObjectId =  mongoose.Schema.Types.ObjectId;

const balanceSchema = new mongoose.Schema({

    prefix: {
        type: String
    },

    type: {
        type: String
    },

    asset: {
        type: String
    },

    asset_longname: {
        type: String
    },

    display_name: {
        type: String
    },

    quantity: {
        type: String
    },

    estimated_value: {
        type: String
    }

}, { timestamps: true, id: false })


/**
 * Exports
 */

const Balance = mongoose.model('Balance', balanceSchema);

module.exports = Balance;
