/* jshint asi: true */
const _ = require('lodash');
const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');

const ObjectId =  mongoose.Schema.Types.ObjectId;

const Coin = require('./Coin');

const COUNTRY_CODES = require('../constants').COUNTRY_CODES;

// @TODO For offers: Policies and Assets work together for tracking to handling tax redemption and monitoring.
// @TODO Tax declarations: https://cointracking.info/
// @TODO https://coinspectator.com/news/101359/bitcoin-tax-guide-trading-gains-and-losses-lifo-fifo-offsetting-lots
// @TODO "assets" should be associated with tax codes dynamically and made
// options to the user; policies ultimately will govern these transactions;
// @TODO https://github.com/codeforamerica/naics-api
// @TODO http://api.naics.us/v0/q?year=2012&code=519120
const offerSchema = new mongoose.Schema({
  buyer: {
    type: ObjectId,
    ref: 'User',
    required: true
  },
  policy: {
    type: ObjectId,
    ref: 'Policy',
  },
  isPublished: {
    type: Boolean,
    default: true
  },
  isPlaceholder: {
    type: Boolean,
    default: true
  },
  assets: [{
    type: String,
    enum: [
      // Cryptocurrency-specific
      // #virtualforcing "Nobody Is Illegal"
      'fresh-mint',
      'multisig',
      // Equities ("Ownership" which is flattened to labor price calculations over "service" words.)
      // Equity is a form of ownership in the firm and equity holders are
      // known as the ‘owners’ of the firm, its momentum correlation with "service" words.
      // #digitalforcing "Accept or Reject"
      'tourism',
      'duty-free',
      'travel',
      // Service-like (Utilities/Commodities) for material instruments to be used to conform
      // Offers of (Token-bearing) Coins.
      // #digitalcheating "Tweedledee and Tweedledum"
      'sic',
      'naics',
      // Tax-like (Securities) financial instruments to be used to conform
      // Offers of (Token-bearing) Coins (OTC), like stocks, futures, forwards,
      // swaps, bank notes, etc.
      // #virtualcheating "There Is No Alternative"
      'vat',
      'fifo',
      'lifo',
      'hifo',
      'lofo'
    ]
  }],
  geometry: {
    loc: {
      type: {
        type: String,
        default: 'Point'
      },
      coordinates: [],
    }
  },
  profile: {
    contacts: [
      {
        _id: false,
        isPrimary: {
          type: Boolean,
          default: true
        },
        name: {
          type: String,
          required: true
        },
        phone: {
          type: String,
          required: true
        },
        email: {
          type: String,
          required: true
        },
        address: {
          type: String,
          required: false
        }
      }
    ],
    offerType: {
      type: String,
      enum: [
        'collectibles',
        'computer-equipment',
        'eth',
        'btc',
        'xmr',
        'cot'
      ],
      required: true
    },
    location: {
      address: {
        type: String,
        required: true
      },
      unit: {
        type: String,
      },
      city: {
        type: String,
        required: true
      },
      state: {
        type: String,
        required: true
      },
      postalCode: {
        type: String,
        required: true
      },
      country: {
        type: String,
        enum: COUNTRY_CODES,
        required: true
      }
    },
    title: {
      type: String,
      required: true
    },
    subtitle: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    photos: [{
      _id: false,
      image: {
        type: ObjectId,
        ref: 'Asset',
        required: true
      },
      isDefault: {
        type: Boolean,
        default: false
      }
    }]
  }
}, {timestamps: true, id: false});

/**
 * Plugins
 */

offerSchema.plugin(paginate);

/**
 * Middleware
 */

offerSchema.pre('remove', function(next) {
  Coin.findCoinsForOffer(this._id, function(err, coins) {
    if (err) {
      console.log(err);
    }
    _.forEach(coins, function(coin) {
      console.log(coin);
      coin.remove();
    });
    next();
  });
});

/**
 * Methods
 */

offerSchema.methods.setPublished = function setPublished() {
  this.isPublished = true;
};

offerSchema.methods.setHidden = function setHidden() {
  this.isPublished = false;
};

/**
 * Statics
 */

offerSchema.statics.findOfferByBuyer = function(userID, cb) {
   Offer.find({ buyer: userID }, cb);
};

offerSchema.statics.findOfferByContract = function (contract, cb) {
  Offer
    .findOne({ _id: contract.coins[0].coin.offer })
    .populate('policy')
    .exec(function(err, offer) {
      if (err) {
        cb(err);
      }
      else {
        cb(null, offer);
      }
    });
};


/**
 * Virtuals
 */

offerSchema.virtual('geometry.position').get(function () {

  var geo = {
    longitude: 0,
    latitude: 0
  };

  try {
    geo = {
      longitude: this.geometry.loc.coordinates[0],
      latitude: this.geometry.loc.coordinates[1]
    };
  } catch(e) {
    console.log(e);
  }

  return geo;
});

offerSchema.virtual('links').get(function() {

  const selfURI = '/api/v1/offers/' + this._id;

  const links = [
    {
      rel: 'self',
      href: selfURI
    },
    {
      rel: 'coins',
      href: selfURI + '/coins'
    },
    {
      rel: 'addCoin',
      href: selfURI + '/action'
    },
    {
      rel: 'addPhoto',
      href: selfURI + '/action'
    },
    {
      rel: 'addAsset',
      href: selfURI + '/action'
    },
    {
      rel: 'setPublished',
      href: selfURI + '/action'
    },
    {
      rel: 'setHidden',
      href: selfURI + '/action'
    },
    {
      rel: 'setPolicy',
      href: selfURI + '/action'
    }
  ];

  return links;
});


offerSchema.set('toObject', { virtuals: true });
offerSchema.set('toJSON',   { virtuals: true });

/**
 * Indexes
 */

offerSchema.index({ 'geometry.loc': '2dsphere' });

/**
 * Exports
 */

const Offer = mongoose.model('Offer', offerSchema);

module.exports = Offer;
