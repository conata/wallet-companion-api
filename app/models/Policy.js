const mongoose = require('mongoose');

const ObjectId =  mongoose.Schema.Types.ObjectId;

// Buyer (Buyer of Contracts; not Buyer in Store; at the Store level, these are sellers)
// Property > Bed > (bed|room)
// Offer > Coin > (token)
//
// Buyer
// Reservation > Bed > (bed|room)
// [Offer/Message/Contract] > Coin > (token) (coin and token are used
// interchangeably in cases where the user is buying a "token-representation
// at full parity with the coin" such that the coin becomes its own asset
// which may not require SIC/NAICS codes but rather new regulatory definitions
// that concern:
// 1. "zero material design constraint"
// 2. "instrinsic"
// 3. "software-based"
// 4. "protocological matter (based on logical/transaction time)"
//
// 1-4 are problems because "cryptocurrencies" are not being developed as models
// within "planned economy" based on labor prices[0]. We are solving a real, concrete
// problem by pre-processing "contract offer messages" to buyers with "policies" that
// inspect potential offers for certain relevant tax/trade/etc codes, since consumer markets
// are presently based on these, and neither the inefficiencies of the market (because it is
// hierarchical and privileged based) nor the favoritism, monopoly power, or collusion of
// government intervention changes the fact that the codes can be described, expanded, and dynamically
// configured based on PKI, which can be rhizomically structured (untold
// hierarchies must be "flatten" by propertization over conditional statements
// about the currency trades from a policy; AND HENCE NOT political determination);
// with this understanding we can develop "letter of credit" systems between cities,
// such that acropolis, decapolis, pentapolis, etc. can be governed by abstract fees
// which need not necessarily include taxes but rather "momentous services" as
// "service words" which subtend and support the capabilities of stateless web applications
// constituted by cached clients written to the bone of capital (pre-redistribution)
// in order to give users and central managers (authorities need not be
// necessarily one kind of entity, but rather an entity which can be the
// bearer of a private key). Generally most assets in the world are centralized, such
// that inroads must be made between "market" and "coercive" centralization.
//
// However, through policy, not politics, we can propertize assets in such a way that
// political power, and therefore power-relations, are "flattened" without "ownership"
// where "means" need to be seized. Nothing is hidden: not even prices, regardless of who
// can see them: it's a matter of provenance of the names collated at the time of
// propertization. "Ownership" is stipulative on the grounds of presence, and the participatory
// relationship between names, their value, and their causal introduction to
// the system. The presence of peers engenders the co-referentiality of a name such that the
// use of the name, if coherent, established integrity, re-inforces the weight of parameters
// toward immutability, and then ultimately governance without ownersip.
//
// Ownership, therefore, is an emergent property of the use (utility) of names (address), whereas
// policies enable the coordination of industry groups over time within a
// system of "borderless" (two properties of common consensus protocols when
// dictates of an digital insufficiency then therefore virtualize
// "borderlessness" from "permissionlessness + domainlessness", for instance).
// We might also induce other properties, with the relevant parameters, such
// as "frictionlessness" or "namelessness" as "versionlessness - timelessness
// (or universion without time; as opposed to being universional in time)";
// (or algorithmically bordered based on COUNTRY_CODES) and letter of credit
// expansion as industry codes evolve over "organic compositions" and variable capital.
//
// [0]: "We have suggested that consumer goods ought to be marked with their
// actual labor content, but for the purposes of determining target prices—in
// order to apply the consumer goods algorithm13—there may be some merit in
// this alternative."  Economic planning, computers and labor values.  W. Paul
// Cockshott and Allin Cottrell∗ January, 1999

// "Fee" is generic enough to include tax codes (SIC, VAT properties, etc.),
// and tokens can be given rules to correlate with their type such that loans
// and another properties of transfers can be programmed at a policy level in
// order to coordinate faithful interactions between buyers and sellers.

// Look for "assets" on a created "Offer" for a "Coin's" "token", the tax code
// ultimately determines the "meaning" of the currency from a regulatory standpoint.
//
// Of course, "assets" exist as digital artifacts too, like images and video.
// Currencies COULD also be programmed around these digital assets, versus
// "virtual" ones like land banks, which might have all sorts of new policies.
//
// It makes sense to keep the "cleaningFee" and other kinds of policy
// subspecifications because some currencies COULD be "pegged" to tourism or
// hotel reservations. So a buyer could pay a certain rate toward occupancy
// tax or a security deposit when buying coins from the person, in case they
// want to use those tokens to ACTUALLY realize whatever asset they virtualized.
const policySchema = new mongoose.Schema({
  buyer: {
    type: ObjectId,
    ref: 'User',
    required: true
  },
  name: {
    type: String,
    default: 'Untitled Policy'
  },
  description: {
    type: String,
    default: ''
  },
  tokenPolicy: {
    checkIn: {
      type: String,
      default: '11:00'
    },
    checkOut: {
      type: String,
      default: '15:00'
    }
  },
  feePolicy: {
    serviceFee: {
      enabled: {
        type: Boolean,
        default: false
      },
      rate: {
        type: Number,
        default: 0.00
      }
    },
    salesTax: {
      enabled: {
        type: Boolean,
        default: false
      },
      rate: {
        type: Number,
        default: 0.00
      }
    },
    occupancyTax: {
      enabled: {
        type: Boolean,
        default: false
      },
      rate: {
        type: Number,
        default: 0.00
      }
    },
    cleaningFee: {
      enabled: {
        type: Boolean,
        default: false
      },
      rate: {
        type: Number,
        default: 0.00
      }
    },
    securityDeposit: {
      enabled: {
        type: Boolean,
        default: false
      },
      amount: {
        type: Number,
        default: 0.00
      }
    },
    earlyCheckIn: {
      enabled: {
        type: Boolean,
        default: false
      },
      amount: {
        type: Number,
        default: 0.00
      }
    },
    lateCheckOut: {
      enabled: {
        type: Boolean,
        default: false
      },
      amount: {
        type: Number,
        default: 0.00
      }
    }
  }
}, {timestamps: true, id: false});

/**
 * Methods
 */

policySchema.methods.getCheckInTime = function() {
  return this.tokenPolicy.checkIn;
};

policySchema.methods.setCheckInTime = function(timeString) {
  this.tokenPolicy.checkIn = timeString;
};

policySchema.methods.getCheckOutTime = function() {
  return this.tokenPolicy.checkOut;
};

policySchema.methods.setCheckOutTime = function(timeString) {
  this.tokenPolicy.checkOut = timeString;
};

policySchema.methods.hasServiceFee = function() {
  return this.feePolicy.serviceFee.enabled;
};

policySchema.methods.enableServiceFee = function() {
  this.feePolicy.serviceFee.enabled = true;
};

policySchema.methods.disableServiceFee = function() {
  this.feePolicy.serviceFee.enabled = false;
};

policySchema.methods.getServiceFee = function() {
  if ( ! this.hasServiceFee() ) {
    return 0.0;
  }

  return this.feePolicy.serviceFee.rate;
};

policySchema.methods.setServiceFee = function(rate) {
  this.feePolicy.serviceFee.rate = rate;
};

policySchema.methods.enableSalesTax = function() {
  this.feePolicy.salesTax.enabled = true;
};

policySchema.methods.disableSalesTax = function() {
  this.feePolicy.salesTax.enabled = false;
};

policySchema.methods.hasSalesTax = function() {
  return this.feePolicy.salesTax.enabled;
};

policySchema.methods.getSalesTaxRate = function() {
  if ( ! this.hasSalesTax() ) {
    return 0.0;
  }

  return this.feePolicy.salesTax.rate;
};

policySchema.methods.setSalesTaxRate = function(rate) {
  this.feePolicy.salesTax.rate = rate;
};

policySchema.methods.enableOccupancyTax = function() {
  this.feePolicy.occupancyTax.enabled = true;
};

policySchema.methods.disableOccupancyTax = function() {
  this.feePolicy.occupancyTax.enabled = false;
};

policySchema.methods.hasOccupancyTax = function() {
  return this.feePolicy.occupancyTax.enabled;
};

policySchema.methods.getOccupancyTaxRate = function() {
  if ( ! this.hasOccupancyTax() ) {
    return 0.0;
  }

  return this.feePolicy.occupancyTax.rate;
};

policySchema.methods.setOccupancyTaxRate = function(rate) {
  this.feePolicy.occupancyTax.rate = rate;
};


policySchema.methods.hasTaxes = function() {
  if ( this.feePolicy.hasSalesTax() || this.feePolicy.hasOccupancyTax() ) {
    return true;
  }

  return false;
};

policySchema.methods.getTaxRate = function() {
  if ( ! this.hasTaxes() ) {
    return 0.0;
  }

  return this.getOccupancyTaxRate() + this.getSalesTaxRate();
};


policySchema.methods.hasCleaningFee = function() {
  return this.feePolicy.cleaningFee.enabled;
};

policySchema.methods.enableCleaningFee = function() {
  this.feePolicy.cleaningFee.enabled = true;
};

policySchema.methods.disableCleaningFee = function() {
  this.feePolicy.cleaningFee.enabled = false;
};

policySchema.methods.getCleaningFee = function() {
  if ( ! this.hasCleaningFee() ) {
    return 0.0;
  }

  return this.feePolicy.cleaningFee.rate;
};

policySchema.methods.setCleaningFee = function(rate) {
  this.feePolicy.cleaningFee.rate = rate;
};

policySchema.methods.hasSecurityDeposit = function() {
  return this.feePolicy.securityDeposit.enabled;
};

policySchema.methods.enableSecurityDeposit = function() {
  this.feePolicy.securityDeposit.enabled = true;
};

policySchema.methods.disableSecurityDeposit = function() {
  this.feePolicy.securityDeposit.enabled = false;
};

policySchema.methods.getSecurityDeposit = function() {
  if ( ! this.hasSecurityDeposit() ) {
    return 0.0;
  }

  return this.feePolicy.securityDeposit.amount;
};

policySchema.methods.setSecurityDeposit = function(amount) {
  this.feePolicy.securityDeposit.amount = amount;
};

/**
 * Virtuals
 */

policySchema.virtual('links').get(function() {

  const selfURI = '/api/v1/polices/' + this._id;

  const links = [
    {
      rel: 'self',
      href: selfURI
    }
  ];
});

policySchema.set('toObject', { virtuals: true });
policySchema.set('toJSON',   { virtuals: true });

/**
 * Exports
 */

const Policy = mongoose.model('Policy', policySchema);

module.exports = Policy;
