const mongoose = require('mongoose');
const paginate = require('mongoose-paginate');

const ObjectId =  mongoose.Schema.Types.ObjectId;

const assetSchema = new mongoose.Schema({
  buyer: {
    type: ObjectId,
    ref: 'User',
    required: true,
  },
  type: {
    type: String,
    enum: [ 'offerImage', 'userImage' ],
    required: true,
  },
  url: {
    type: String,
    required: true
  }
}, { timestamps: true, id: false });

/**
 * Plugins
 */

assetSchema.plugin(paginate);

/**
 * Exports
 */

const Asset = mongoose.model('Asset', assetSchema);

module.exports = Asset;
