/**
 * @schema https://schema.org/Text
 */
const mongoose = require('mongoose')

const ObjectId =  mongoose.Schema.Types.ObjectId

const addressSchema = new mongoose.Schema({

    prefix: {
        type: String
    },

    index: {
        type: Number
    },

    network: {
        type: Number
    },

    address: {
        type: String
    },

    label: {
        type: String
    },

}, { timestamps: true, id: false })


/**
 * Exports
 */

const Address = mongoose.model('Balance', addressSchema)

module.exports = Address
