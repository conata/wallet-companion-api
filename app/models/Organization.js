const mongoose = require('mongoose');

const ObjectId =  mongoose.Schema.Types.ObjectId;

const organizationSchema = new mongoose.Schema({
  name: String,
  offers: [
    {
      type: ObjectId,
      ref: 'Offer'
    }
  ],
  buyers: [
    {
      type: ObjectId,
      ref: 'User'
    }
  ]
});

/**
 * Exports
 */

const Organization = mongoose.model('Organization', organizationSchema);

module.exports = Organization;
