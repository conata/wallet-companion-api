.PHONY: all install test build run tarball

all: run

install:
	npm install

test:
	npm test

build:
	node ./bin/reportComplexity.js

run:
	cd app && nodemon server.js

tarball: wallet-companion-api.tar.gz

wallet-companion-api.tar.gz:
	tar -czvf wallet-companion-api.tar.gz \
	--exclude=app/env.test \
	--exclude=app/.env.develop \
	--exclude=app/.env.production \
	--exclude=app/public/docs/ \
	README.md package.json gulpfile.js .jshintrc \
	app/ scripts/ test/
