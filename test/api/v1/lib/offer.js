let _ = require('lodash');
let moment = require('moment');

let User = require('../../../../app/models/User');
let Offer = require('../../../../app/models/Offer');
let Coin = require('../../../../app/models/Coin');

var exports = module.exports = {};

/**
 * Private Functions
 */

/**
 * _generateOfferData
 *
 * @param params
 * @returns {undefined}
 */
function _generateOfferData(params) {

  let offerData = {
    profile: params.profile,
    geometry: params.geometry,
    assets: params.assets,
    coins: params.coins
  };

  return offerData;
}

/**
 * _createOffer
 *
 * @param offerParams
 * @returns {undefined}
 */
function _createOffer(offerParams) {

  let offerData = {};
  let offerCoins = [];

  if (_.has(offerData, 'coins')) {
    offerCoins = offerParams.coins;
    offerData = _.omit(offerParams, ['coins']);
  } else {
    offerData = offerParams;
  }

  let offer = new Offer(offerData);
  offer.save();

  if ( ! _.isEmpty(offerCoins) ) {
    _.forEach(offerCoins, function(coinData) {
      coinData.offer = offer._id;
      let coin = _createCoin(coinData);
    });
  }

  return offer;
}

/**
 * _createCoin
 *
 * @param coinData
 * @returns {undefined}
 */
function _createCoin(coinData) {
  let coin = new Coin(coinData);
  coin.save();
  return coin;
}


/**
 * _removeOffer
 *
 * @param offerID
 * @returns {undefined}
 */
function _removeOffer(offerID) {
  Offer.findById(offerID).remove();
  return offerID;
}

/**
 * _removeCoin
 *
 * @param coinID
 * @returns {undefined}
 */
function _removeCoin(coinID) {
  Coin.findById(coinID).remove();
  return coinID;
}

/**
 * Public Functions
 */

exports.generateOfferData = function(params) {

  if ( ! params ) {
    params = {};
  }

  var defaultStartDate = moment().subtract(7, 'd');
  var defaultEndDate = moment(defaultStartDate).add(14, 'd');

  var defaultCoin = {
    price: 50,
    quantity: 100,
    tokenType: 'erc20',
    coinType: 'sha256',
    payType: 'network',
    description: 'Test coin',
    availability: {
      from: defaultStartDate.format('MM-DD-YYYY'),
      to: defaultEndDate.format('MM-DD-YYYY'),
      checkIn: '15:00',
      checkOut: '11:00'
    }
  };

  var offerParams = {
    owner    : params.owner || null,
    profile  : params.profile || {},
    coins    : params.coins || [ defaultCoin ],
    geometry : params.geometry || {}
  };

  return _generateOfferData(offerParams);
};

exports.createOffer = function(params) {
  return _createOffer(params);
};

exports.removeOffer = function(offerID) {
  return _removeOffer(offerID);
};

exports.removeAllOffers = function() {
  Offer.remove({}, (err) => {});
  Coin.remove({}, (err) => {});
};
