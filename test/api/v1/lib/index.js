const _ = require('lodash')

let commonLib = require('./common')
let userLib = require('./user')
let offerLib = require('./offer')
let contractLib = require('./contract')

var exports = module.exports = {}

/**
 * Private Functions
 */

function _init() {
  const testData = commonLib.loadTestData()

  const testUsers = _.map(testData.users, function(testUser) {
    return userLib.createUser(testUser)
  })

  const defaultSeller     = _.find(testUsers, { role : 'Seller' })
  const defaultBuyer = _.find(testUsers, { role: 'Buyer '})

  const testOffers = _.map(testData.offers, function(testOffer) {
    testOffer.seller = defaultSeller._id
    return offerLib.createOffer(testOffer)
  })

  return { users: testUsers, offers: testOffers }
}

function _purge() {
  userLib.removeAllUsers()
  offerLib.removeAllOffers()
  contractLib.removeAllContracts()
}

/**
 * Public Functions
 */

exports.init = function() { return _init() }

exports.purge = function() { return _purge() }

exports.common = {
  auth                : commonLib.authRequest,
  authWithEmail       : commonLib.authWithEmail,
  authWithRole        : commonLib.authWithRole,
  getTestUserByRole   : commonLib.getTestUserByRole,
  generateUniqueEmail : commonLib.generateUniqueEmail
}

exports.user = {
  generate : userLib.generateUserData,
  create   : userLib.createUser,
  remove   : userLib.removeUser,
  purge    : userLib.removeAllUsers,
}

exports.offer = {
  generate : offerLib.generateOfferData,
  create   : offerLib.createOffer,
  remove   : offerLib.removeOffer,
  purge    : offerLib.removeAllOffers
}

exports.contract = {
  generate : contractLib.generateContractData,
  create   : contractLib.createContract,
  remove   : contractLib.removeContract,
  purge    : contractLib.removeAllContracts
}

