let User = require('../../../../app/models/User');

var exports = module.exports = {};

/**
 * Private Functions
 */

function _generateUserData(params) {

  let userData = {
    email: params.email,
    password: params.password,
    role: params.role,
    profile: {
      name: {
        first: 'Mocha',
        last: 'Test'
      },
      gender: 'Other',
      location: {
        city: 'Toronto',
        state: 'ON',
        postalCode: 'M2M',
        country: 'CA'
      },
      phone: '555-5555'
    }
  };

  return userData;
}

function _createUser(userData) {
  let user = new User(userData);
  user.save();
  return user;
}

function _removeUser(userID) {
  User.findById(userID).remove();
  return userID;
}

/**
 * Public Functions
 */

exports.generateUserData = function(params) {

  if ( ! params ) {
    params = {};
  }

  var userParams = {
    email: params.email || 'mocha@kpd-i.com',
    password: params.password || 'test1234',
    role: params.role || 'Member'
  };

  return _generateUserData(userParams);
};

exports.createUser = function(params) {
  return _createUser(params);
};

exports.removeUser = function(userID) {
  return _removeUser(userID);
};

exports.removeAllUsers = function() {
  User.remove({}, (err) => {
    if (err) {
      console.log(err);
    }
  });
};
