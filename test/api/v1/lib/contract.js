let _ = require('lodash');
let moment = require('moment');

let User = require('../../../../app/models/User');
let Contract = require('../../../../app/models/Contract');

var exports = module.exports = {};

/**
 * Private Functions
 */

function _generateContractData(params) {

  let contractData = {
    coins: params.coins,
    dates: params.dates
  };

  return contractData;
}

function _createContract(contractParams) {

  let startDate = new Date(contractParams.dates.from);
  let endDate = new Date(contractParams.dates.to);

  let contractData = {
    coins: contractParams.coins,
    dates: {
      from: startDate,
      to: endDate
    }
  };

  let contract = new Contract(contractData);
  contract.save();
  return contract;
}

function _removeContract(contractID) {
  Contract.findById(contractID).remove();
  return contractID;
}

/**
 * Public Functions
 */

exports.generateContractData = function(params) {

  if ( ! params ) {
    params = {};
  }

  var defaultStartDate = moment();
  var defaultEndDate = moment(defaultStartDate).add(7, 'd');

  var startDate = null;

  if ( _.has(params, 'dates.from') ) {
    startDate = moment(new Date(params.dates.from));
  } else {
    startDate = defaultStartDate;
  }

  var endDate = null;

  if ( _.has(params, 'dates.to') ) {
    endDate = moment(new Date(params.dates.to));
  } else {
    endDate = defaultEndDate;
  }

  var contractParams = {
    coins: params.coins || [],
    dates: {
      from: startDate.format('MM-DD-YYYY'),
      to: endDate.format('MM-DD-YYYY')
    }
  };

  return _generateContractData(contractParams);
};

exports.createContract = function(params) {
  return _createContract(params);
};

exports.removeContract = function(contractID) {
  return _removeContract(contractID);
};

exports.removeAllContracts = function() {
  Contract.remove({}, (err) => {});
};
