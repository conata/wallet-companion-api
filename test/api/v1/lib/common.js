const _ = require('lodash');
const uuidv4 = require('uuid/v4');

let testUsers      = require('./data/users');
let testOffers = require('./data/offers');

var exports = module.exports = {};

var users = [];
var offers = [];

function _loadTestUsers() {
  users = _.map(testUsers, function(userData) {
    return userData.user;
  });

  return users;
}

function _loadTestOffers() {

  offers =_.map(testOffers, function(offerData) {

    var offer = offerData.offer;

    _.reverse(offer.geometry.loc.coordinates);

    _.forEach(offer.coins, function(coinData) {
      coinData.description = 'Excellent coin for only $' +
        coinData.price + ' at ' + offerData.offer.profile.title;
    });

    return offer;
  });

  return offers;
}

exports.getTestUserByRole = function(role) {
  var user = _.find(users, { role: role });
  return user;
};

exports.authRequest = function(agent) {
  return function(credentials) {
    return agent.post('/api/v1/auth').send(credentials);
  };
};

exports.authWithEmail = function(email) {
  var user = _.find(users, { email: email });
  var credentials = { email: email, password: user.password };

  return function(agent) {
    return agent.post('/api/v1/auth').send(credentials);
  };
};

exports.authWithRole = function(role) {
  var user = _.find(users, { role: role });
  var credentials = { email: user.email, password: user.password };

  return function(agent) {
    return agent.post('/api/v1/auth').send(credentials);
  };
};

exports.loadTestData = function() {
  const testData = {
    users: _loadTestUsers(),
    offers: _loadTestOffers()
  };
  return testData;
};

exports.generateUniqueEmail = function(prefix, domain) {

  var emailString = prefix || 'mochatest.';

  emailString += uuidv4();

  if ( domain ) {
    emailString += ['@', domain].join('');
  } else {
    emailString += '@bpc-dev.local';
  }

  return emailString;
};
