process.env.NODE_ENV = 'test'

let chai = require('chai')
let should = chai.should()
let chaiHttp = require('chai-http')
let mongoose = require('mongoose')

let testLib = require('./lib/index')
let server = require('../../../app/server')

chai.use(chaiHttp)

var agent = chai.request.agent(server)

var testData = {}

describe('Offer', () => {

  before((done) => {
    testData = testLib.init()
    done()
  })

  after((done) => {
    testLib.purge()
    done()
  })

  describe('/GET offers', () => {
    it('it should be able to GET offers', (done) => {

      testLib.common.authWithRole('Buyer')(agent)
        .then(function(res) {
          return agent
            .get('/api/v1/offers')
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200)
              res.body.offers.should.be.an('array')
              done()
            })
            .catch(function(err) {
              done(err)
            })
        })
        .catch(function(err) {
          done(err)
        })
    })

    it([
        'it should not GET offer contact information on unauthenticated',
        'requests'
    ].join(' '), (done) => {
      agent
        .get('/api/v1/offers')
        .then(function(res) {
          res.should.have.status(200)
          res.body.offers.should.be.an('array')
          done()
        })
        .catch(function(err) {
          done(err)
        })
    })
  })

  describe('POST /offers', () => {

    it('it should not POST a offer with invalid parameters', (done) => {

      let offerData = testLib.offer.generate()

      testLib.common.authWithRole('Seller')(agent)
        .then(function(res) {
          return agent
            .post('/api/v1/offers')
            .send(offerData)
            .then(function(res) {
              res.should.have.status(400)
              res.body.should.be.a('object')
              res.body.should.have.offer('error')
              done()
            })
            .catch(function(err) {
              done()
            })
        })
        .catch(function(err) {
          done()
        })
    })
  })

  describe('GET /offers/:id', () => {

    it('it should GET a offer by the given id', (done) => {

      var testOffer = testData.offers[0]

      testLib.common.authWithRole('Buyer')(agent)
        .then(function(res) {
          return agent
            .get('/api/v1/offers/' + testOffer._id)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200)
              res.body.should.be.a('object')
              res.body.offer.should.have.offer('profile')
              done()
            })
            .catch(function(err) {
              done(err)
            })
        })
        .catch(function(err) {
          done(err)
        })
    })
  })


  describe('PUT /offers/:id', () => {
    it('it should UPDATE a offer given the id', (done) => {

      var testOffer = testData.offers[0]

      testLib.common.authWithRole('Admin')(agent)
        .then(function(res) {
          return agent
            .put('/api/v1/offers/' + testOffer._id)
            .withCredentials()
            .send({ profile: { location: { city: 'London' } } } )
            .then(function(res) {
              res.should.have.status(200)
              done()
            })
            .catch(function(err) {
              done(err)
            })
        })
        .catch(function(err) {
          done(err)
        })
    })
  })

  describe('DELETE /offers/:id', () => {
    it('it should DELETE a offer given the id', (done) => {

      var testOffer = testData.offers[0]

      testLib.common.authWithRole('Admin')(agent)
        .then(function(res) {
          return agent
            .delete('/api/v1/offers/' + testOffer._id)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200)
              done()
            })
            .catch(function(err) {
              done(err)
            })
        })
        .catch(function(err) {
          done(err)
        })
    })
  })
})
