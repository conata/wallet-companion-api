process.env.NODE_ENV = 'test';

let chai = require('chai');
let should = chai.should();
let chaiHttp = require('chai-http');
let mongoose = require('mongoose');

let testLib = require('./lib/index');
let server = require('../../../app/server');

chai.use(chaiHttp);

var agent = chai.request.agent(server);

var testData = {};

describe('User', () => {

  before((done) => {
    testData = testLib.init();
    done();
  });

  after((done) => {
    testLib.purge();
    done();
  });

  describe('/GET users', () => {
    it('it should not be able to GET users', (done) => {
      chai.request(server)
        .get('/api/v1/users')
        .then(function(res) {
          res.should.have.status(401);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          done();
        })
        .catch(function(err) {
          done();
        });
    });
  });

  describe('POST /users', () => {

    it('it should not POST a user with invalid parameters', (done) => {

      let userData = testLib.user.generate();

      userData.email = 'bademail';
      userData.confirmPassword = userData.password;

      chai.request(server)
        .post('/api/v1/users')
        .send(userData)
        .then(function(res) {
          res.should.have.status(400);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          done();
        })
        .catch(function(err) {
          done();
        });
    });

    it('it should POST a user ', (done) => {

      let userData = testLib.user.generate({ email: testLib.common.generateUniqueEmail() });

      userData.confirmPassword = userData.password;

      agent
        .post('/api/v1/users')
        .send(userData)
        .then(function(res) {
          res.should.have.status(200);
          done();
        })
        .catch(function(err) {
          done(err);
        });
    });
  });

  describe('GET /users/:id', () => {
    it('it should GET a user by the given id', (done) => {

      let userData = testLib.user.generate({ email: 'getme@test.com' });
      let testUser = testLib.user.create(userData);

      testLib.common.authWithRole('Admin')(agent)
        .then(function(res) {

          res.should.have.status(200);

          return agent
            .get('/api/v1/users/' + testUser._id)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.user.should.have.property('email');
              res.body.user.should.have.property('role');
              res.body.user.should.have.property('profile');
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });
  });


  describe('PUT /users/:id', () => {
    it('it should UPDATE a user given the id', (done) => {

      let userData = testLib.user.generate({ email: 'putme@test.com' });
      let testUser = testLib.user.create(userData);

      testLib.common.authWithRole('Admin')(agent)
        .then(function(res) {

          res.should.have.status(200);

          agent
            .put('/api/v1/users/' + testUser._id)
            .withCredentials()
            .send({ profile: { location: { city: 'London' } } } )
            .then(function(res) {
              res.should.have.status(200);
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });
  });

  describe('DELETE /users/:id', () => {
    it('it should DELETE a user given the id', (done) => {

      let userData = testLib.user.generate({ email: 'deleteme@test.com' });
      let testUser = testLib.user.create(userData);

      testLib.common.authWithRole('Admin')(agent)
        .then(function(res) {

          res.should.have.status(200);

          agent
            .delete('/api/v1/users/' + testUser._id)
            .withCredentials()
            .then(function(res) {
              done();
              res.should.have.status(200);
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });
  });
});
