/**
 * @fileOverview ./test/api/v1/contract.js
 * @description
 * Test Suite for potentially CoRE discoverable resources planted by web
 * applications to hybridizations of native applications.
 */
process.env.NODE_ENV = 'test';

let chai = require('chai');
let should = chai.should();
let chaiHttp = require('chai-http');
let mongoose = require('mongoose');

let testLib = require('./lib/index');
let server = require('../../../app/server');

chai.use(chaiHttp);

var agent = chai.request.agent(server);

var testData = {};

var contractBuyer = {};
var contractAttacker = {};

var contractOffer = {};

var contractCoinId = null;

var contractId = null;

var buyerEmail = testLib.common.generateUniqueEmail();
var attackerEmail = testLib.common.generateUniqueEmail();

var credentials = { email: buyerEmail, password: 'test1234'};
var attackerCredentials = { email: attackerEmail, password: 'test1234' };

describe('Contract', Contract);

function Contract() {

  /*
   * INIT
   *
   * @description
   * Hypermedia Controls, Media format interaction event sources, etc.
   **/

  before((done) => {
    testData = testLib.init();
    done();
  });

  after((done) => {
    testLib.purge();
    done();
  });

  /*
   * MODEL
   *
   * @description
   * Or merely top-level functions, wrapper functions, first-major functions,
   * module controllers, moduleSpecs, etc.
   **/

  describe('/POST users', post$users);

  describe('/POST users/:id/action', post$usersIdAction);

  describe('/POST offers', post$offers);

  describe('/GET /offers/:id/coins', get$offersIdCoins);

  describe('/POST /contracts', post$contracts);

  describe('/POST /contracts/:id/action', post$contractsIdAction);

  ////////////////////

  /*
   * DETAILS
   *
   * @description
   **/

  /**
   * @name post$users
   */
  function post$users() {

    it('it should create a buyer for testing contracts', (done) => {

      let userData = testLib.user.generate({ email: buyerEmail });
      userData.confirmPassword = userData.password;

      agent
        .post('/api/v1/users')
        .send(userData)
        .then(function(res) {
          contractBuyer = res.body.user;
          res.should.have.status(200);
          done();
        })
        .catch(function(err) {
          done(err);
        });
    });

    it('it should create an attacker for testing contracts', (done) => {

      let userData = testLib.user.generate({ email: attackerEmail });
      userData.confirmPassword = userData.password;

      agent
        .post('/api/v1/users')
        .send(userData)
        .then(function(res) {
          contractAttacker = res.body.user;
          res.should.have.status(200);
          done();
        })
        .catch(function(err) {
          done(err);
        });
    });
  }

  /**
   * @name post$usersIdAction
   */
  function post$usersIdAction() {

    it('it should add a dummy payment method', (done) => {

      var dummyPaymentAction = {
        action: {
          key: 'addDummyPaymentMethod'
        }
      };

      testLib.common.auth(agent)(credentials)
        .then(function(res) {
          return agent
            .post('/api/v1/users/' + contractBuyer._id + '/action')
            .send(dummyPaymentAction)
            .then(function(res) {
              res.should.have.status(200);
              res.body.should.be.a('object');
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });
  });

  function post$offers() {

    it('it should create a offer', (done) => {

      var offer = testData.offers[0];

      var offerData = testLib.offer.generate({
        seller: offer.seller,
        profile: offer.profile,
        geometry: offer.geometry
      });

      testLib.common.authWithRole('Seller')(agent)
        .then(function(res) {
          return agent
            .post('/api/v1/offers/')
            .send(offerData)
            .then(function(res) {
              res.should.have.status(200);
              contractOffer = res.body.offer;
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });
  }

  function get$offersIdCoins{

    it('it should get coins for the offer', (done) => {
      testLib.common.authWithRole('Seller')(agent)
        .then(function(res) {
          return agent
            .get('/api/v1/offers/' + contractOffer._id + '/coins')
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200);
              var coins = res.body.coins;
              var contractCoin = coins[0];
              contractCoinId = contractCoin._id;
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });
  }

  function post$contracts() {

    it('it should create a contract', (done) => {

      var contractData = testLib.contract.generate({
        coins: [{ coin: contractCoinId, quantity: 1 }]
      });

      testLib.common.auth(agent)(credentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts')
            .send(contractData)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200);
              contractId = res.body.contract._id;
              res.body.should.be.a('object');
              res.body.contract.status.should.be.eql('Pending');
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });

    it('it should not create a contract with invalid dates', (done) => {

      var contractData = testLib.contract.generate(
        {
          dates: {
            from: '01-25-2020',
            to: '01-20-2020'
          },
          coins: [{ coin: contractCoinId, quantity: 1 }]
        });

      testLib.common.auth(agent)(credentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts')
            .send(contractData)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(400);
              res.body.should.be.a('object');
              done();
            })
            .catch(function(err) {
              done();
            });
        })
        .catch(function(err) {
          done(err);
        });
    });
  }

  function post$contractsIdAction() {

    it('it should create a transaction for the invoice', (done) => {

      var getInvoiceAction = {
        action: {
          key: 'getInvoice'
        }
      };

      testLib.common.auth(agent)(credentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts/' + contractId + '/action')
            .send(getInvoiceAction)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.status.should.be.equal('TransactionPending');
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });

    it('it should not allow the attacker to submit payment for the contract', (done) => {

      var submitPaymentAction = {
        action: {
          key: 'submitPayment'
        }
      };

      testLib.common.auth(agent)(attackerCredentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts/' + contractId + '/action')
            .send(submitPaymentAction)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(401);
              res.body.should.be.a('object');
              res.body.error.type.should.be.equal('RolePermissionError');
              done();
            })
            .catch(function(err) {
              done();
            });
        })
        .catch(function(err) {
          done(err);
        });
    });

    it('it should not allow the attacker to cancel the contract', (done) => {

      var submitPaymentAction = {
        action: {
          key: 'cancel'
        }
      };

      testLib.common.auth(agent)(attackerCredentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts/' + contractId + '/action')
            .send(submitPaymentAction)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(401);
              res.body.should.be.a('object');
              res.body.error.type.should.be.equal('RolePermissionError');
              done();
            })
            .catch(function(err) {
              done();
            });
        })
        .catch(function(err) {
          done(err);
        });
    });

    it('it should not allow the attacker to view the contract invoice', (done) => {

      var submitPaymentAction = {
        action: {
          key: 'getInvoice'
        }
      };

      testLib.common.auth(agent)(attackerCredentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts/' + contractId + '/action')
            .send(submitPaymentAction)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(401);
              res.body.should.be.a('object');
              res.body.error.type.should.be.equal('RolePermissionError');
              done();
            })
            .catch(function(err) {
              done();
            });
        })
        .catch(function(err) {
          done(err);
        });
    });

    it('it should submit a payment for the contract', (done) => {

      var submitPaymentAction = {
        action: {
          key: 'submitPayment'
        }
      };

      testLib.common.auth(agent)(credentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts/' + contractId + '/action')
            .send(submitPaymentAction)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.status.should.be.equal('Approved');
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });

    it('it should update the transaction to "processing" status', (done) => {

      var getInvoiceAction = {
        action: {
          key: 'getInvoice'
        }
      };

      testLib.common.auth(agent)(credentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts/' + contractId + '/action')
            .send(getInvoiceAction)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.status.should.be.equal('TransactionProcessing');
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });

    it('it should complete payment for the contract', (done) => {

      var completePaymentAction = {
        action: {
          key: 'completePayment'
        }
      };

      testLib.common.auth(agent)(credentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts/' + contractId + '/action')
            .send(completePaymentAction)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.status.should.be.equal('Active');
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });


    it('it should update the transaction to "completed" status', (done) => {

      var getInvoiceAction = {
        action: {
          key: 'getInvoice'
        }
      };

      testLib.common.auth(agent)(credentials)
        .then(function(res) {
          return agent
            .post('/api/v1/contracts/' + contractId + '/action')
            .send(getInvoiceAction)
            .withCredentials()
            .then(function(res) {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.status.should.be.equal('TransactionCompleted');
              done();
            })
            .catch(function(err) {
              done(err);
            });
        })
        .catch(function(err) {
          done(err);
        });
    });
  }
}

