WALLET COMPANION API
-------

See the wiki for more detailed reference on data models and API implementation notes.

Usage
=====

Make sure the `.env` file is configured appropriately.

For contracts (MRC interactions):

    node ./node_modules/.bin/testrpc

Install `geth`:

    brew install ethereum
    brew tap ethereum/ethereum

Run the RPC server without CORS restrictions:

    geth --syncmode="fast" --cache=4096 --ws --wsport 8546 --wsorigins "*" --datadir=/path/to/dir

Or:

    geth --syncmode="fast" --cache=4096 --ws --wsport 8546 --wsorigins "*" --datadir=/path/to/dir --genesis=~/genesis_block.json

Install MongoDB. For databases (API interactions):

    sudo mongod --fork --logpath ./log/mongodb.log

For development (MRC interactions):

    $ npm install -g nodemon
    $ npm install
    $ cd app && nodemon server.js

Deployment
==========

To deploy, make sure `heroku` is installed and configured, then run:

...TODO

Transaction Flow
================

The following steps assume that you are authenticated as a traveller and have
added a payment source.

Step 1: Create a contract
--------------------

Send a `POST` request to `/api/v1/contracts` with the following payload.

```
{
  "coins": [
        {
            "coin": "5936117f943ad07adc62f4c0",
            "quantity": 1
        }
    ],
  "dates": { "from": "06/08/2017", "to": "06/31/2017" }
}
```

Response should be `200 OK`, with no body.

Then find the ID of the contract by sending a `GET` request to `/api/v1/contract`.

Step 2: Check the invoice
-----------------

Send a `POST` request to `/api/v1/contracts/:contractID/actions` with the following payload.

```
{
  "action": {
    "key": "getInvoice"
  }
}
```

The response should look like this:

```
{
  "_id": "5936bf82c4a693960d907991",
  "updatedAt": "2017-06-06T14:43:14.401Z",
  "createdAt": "2017-06-06T14:43:14.401Z",
  "contract": "5936bf82c4a693960d907990",
  "amountDue": 3680,
  "__v": 0,
  "amountRefunded": 0,
  "amountPaid": 0,
  "amountPending": 0,
  "status": "TransactionPending"
}
```

(Note: You can jump straight to step 5 from any of the following steps, to test how cancellations work.)

Step 3: Submit the payment
--------------------------

Send a `POST` request to `/api/v1/contracts/:contractID/actions` with the payload:

```
{
  "action": {
    "key": "submitPayment"
  }
}
```

Then check the invoice again (repeat step 2), which should now look like:

```
{
  "_id": "5936bf82c4a693960d907991",
  "updatedAt": "2017-06-06T14:49:54.632Z",
  "createdAt": "2017-06-06T14:43:14.401Z",
  "contract": "5936bf82c4a693960d907990",
  "amountDue": 3680,
  "__v": 0,
  "paymentSource": "card_1ARYexGq97o9O9GGtPabBJym",
  "amountRefunded": 0,
  "amountPaid": 0,
  "amountPending": 3680,
  "status": "TransactionProcessing"
}
```

Step 4: Complete the payment
----------------------------

Send a `POST` request to `/api/v1/contracts/:contractID/actions` with the payload:

```
{
  "action": {
    "key": "completePayment"
  }
}
```

Once again, check the invoice, which should now look like:

```
{
  "_id": "5936bf82c4a693960d907991",
  "updatedAt": "2017-06-06T14:56:00.965Z",
  "createdAt": "2017-06-06T14:43:14.401Z",
  "contract": "5936bf82c4a693960d907990",
  "amountDue": 3680,
  "__v": 0,
  "paymentSource": "card_1ARYexGq97o9O9GGtPabBJym",
  "stripeCharge": "ch_1ARj6DGq97o9O9GGLxaJk0wO",
  "amountRefunded": 0,
  "amountPaid": 3680,
  "amountPending": 0,
  "status": "TransactionCompleted"
}
```

Step 5: Issue a refund (Optional)
---------------------------------

Send a `POST` request to `/api/v1/contracts/:contractID/actions` with the payload:

```
{
  "action": {
    "key": "cancel"
  }
}
```

The invoice should look like this, if you followed all the steps above:

```
{
  "_id": "5936bf82c4a693960d907991",
  "updatedAt": "2017-06-06T15:00:25.325Z",
  "createdAt": "2017-06-06T14:43:14.401Z",
  "contract": "5936bf82c4a693960d907990",
  "amountDue": 3680,
  "__v": 0,
  "paymentSource": "card_1ARYexGq97o9O9GGtPabBJym",
  "stripeCharge": "ch_1ARj6DGq97o9O9GGLxaJk0wO",
  "stripeRefund": "re_1ARjATGq97o9O9GGqsSc7gJd",
  "amountRefunded": 3680,
  "amountPaid": 3680,
  "amountPending": 0,
  "status": "TransactionRefunded"
}
```

Or, if you jumped from step 3 or step 4, it should look something like this:

```
{
  "_id": "5936bf82c4a693960d907991",
  "updatedAt": "2017-06-06T15:00:25.325Z",
  "createdAt": "2017-06-06T14:43:14.401Z",
  "contract": "5936bf82c4a693960d907990",
  "amountDue": 3680,
  "__v": 0,
  "paymentSource": "card_1ARYexGq97o9O9GGtPabBJym",
  "amountRefunded": 0,
  "amountPaid": 0,
  "amountPending": 0,
  "status": "TransactionCancelled"
}
```

Degradable Contract Model functions
=====

    Attribution
    CommericalUse
    DerivativeWorks
    Distribution
    Notice
    Reproduction
    ShareAlike
    Sharing
    SourceCode

    acceptTracking
    adHocShare
    aggregate
    annotate
    anonymize
    append
    appendTo
    archive
    attachPolicy
    attachSource
    attribute
    commercialize
    compensate
    concurrentUse

    copy
    delete
    derive
    digitize
    display
    distribute
    ensureExclusivity
    execute

    export
    extract
    extractChar
    extractPage
    extractWord
    give
    grantUse

    include
    index
    inform
    install
    lease
    lend
    license
    modify
    move

    nextPolicy
    obtainConsent
    pay
    play
    present
    preview
    print
    read

    reproduce
    reviewPolicy
    secondaryUse
    sell
    share
    shareAlike
    stream

    synchronize
    textToSpeech
    transfer
    transform
    translate
    uninstall
    use

    watermark
    write
    writeTo
    identify
    own
    name
    differentiate

    represent
    describe
    depict
    illustrate
    model
    simulate

    synthesize
    replicate
    relate

    connect
    associate
    combine
    merge
    integrate
    unify

    organize
    classify
    index
    locate
    address
    position

    route
    maintain
    store
    retrieve
    find
    share
    improve
    sequence
    instruct
    operate
    manipulate
    control

for information events in regards to Assets[0]. Will likely experimentally test in [0xBitcoin][0xBTC].


[0]: https://www.w3.org/TR/odrl-vocab/#actionConcepts
[0xBTC]: https://github.com/0xbitcoin/white-paper/blob/master/README.md

